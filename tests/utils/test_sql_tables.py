# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exodam_utils import ExodamSqlTables, Table

EXPECT_COLS = ["refereed", "data"]


def test__Tables_class_with_given_insert_columns():
    res = Table("toto", ["titi", "tutu"])

    with soft_assertions():
        assert_that(res.name).is_equal_to("toto")
        assert_that(res.insert_columns).is_equal_to(["titi", "tutu"])


def test__Tables_class_without_given_insert_columns():
    res = Table("titi")

    with soft_assertions():
        assert_that(res.name).is_equal_to("titi")
        assert_that(res.insert_columns).is_equal_to(["refereed", "data"])


@pytest.mark.parametrize(
    "table, expct_name, expct_cols",
    [
        param(
            ExodamSqlTables.exodam_skel,
            "exodam.exodam_skel",
            EXPECT_COLS,
            id="table exodam_skel",
        ),
        param(
            ExodamSqlTables.exodam_types,
            "exodam.exodam_types",
            EXPECT_COLS,
            id="table exodam_types",
        ),
        param(
            ExodamSqlTables.exodam_known_keys,
            "exodam.exodam_known_keys",
            EXPECT_COLS,
            id="table exodam_known_keys",
        ),
        param(
            ExodamSqlTables.exodam_vupnu_keys,
            "exodam.exodam_vupnu_keys",
            EXPECT_COLS,
            id="table exodam_vupnu_keys",
        ),
        param(
            ExodamSqlTables.exodam_cerberus,
            "exodam.exodam_cerberus",
            ["refereed"],
            id="table exodam_cerberus",
        ),
        param(
            ExodamSqlTables.exodam_cerberus2data,
            "exodam.exodam_cerberus2data",
            ["id_cerberus", "name", "cerberus_type", "id_cerberus_data"],
            id="table exodam_cerberus2data",
        ),
        param(
            ExodamSqlTables.exodam_cerberus_data,
            "exodam.exodam_cerberus_data",
            ["data"],
            id="table exodam_cerberus_data",
        ),
        param(
            ExodamSqlTables.exodict,
            "exodam.exodict",
            ["refereed"],
            id="table exodict",
        ),
        param(
            ExodamSqlTables.exodict2data,
            "exodam.exodict2data",
            ["id_exodict", "name", "id_data"],
            id="table exodict2data",
        ),
        param(
            ExodamSqlTables.exodict_data,
            "exodam.exodict_data",
            ["data"],
            id="table exodict_data",
        ),
        param(
            ExodamSqlTables.exodict_editorial,
            "exodam.exodict_editorial",
            ["refereed"],
            id="table exodict_editorial",
        ),
        param(
            ExodamSqlTables.exodict_editorial2data,
            "exodam.exodict_editorial2data",
            ["id_exodict", "name", "id_data"],
            id="table exodict_editorial2data",
        ),
        param(
            ExodamSqlTables.exodict_editorial_data,
            "exodam.exodict_editorial_data",
            ["data"],
            id="table exodict_editorial_data",
        ),
        param(
            ExodamSqlTables.exodam_editorial_skel,
            "exodam.exodam_editorial_skel",
            EXPECT_COLS,
            id="table exodam_editorial_skel",
        ),
        param(
            ExodamSqlTables.exodam_editorial_known_keys,
            "exodam.exodam_editorial_known_keys",
            EXPECT_COLS,
            id="table exodam_editorial_known_keys",
        ),
        #
        param(
            ExodamSqlTables.exodam_editorial_cerberus,
            "exodam.exodam_editorial_cerberus",
            ["refereed"],
            id="table exodam_editorial_cerberus",
        ),
        param(
            ExodamSqlTables.exodam_editorial_cerberus2data,
            "exodam.exodam_editorial_cerberus2data",
            ["id_cerberus", "name", "cerberus_type", "id_cerberus_data"],
            id="table exodam_editorial_cerberus2data",
        ),
        param(
            ExodamSqlTables.exodam_editorial_cerberus_data,
            "exodam.exodam_editorial_cerberus_data",
            ["data"],
            id="table exodam_editorial_cerberus_data",
        ),
    ],
)
def test_ExodamSqlTables(table, expct_name, expct_cols):
    with soft_assertions():
        assert_that(table.name).is_equal_to(expct_name)
        assert_that(table.insert_columns).is_equal_to(expct_cols)
