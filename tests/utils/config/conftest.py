# Pytest imports
import pytest

# Standard imports
from os import environ as setenv
from os import getenv

# Third party imports
from dotenv import load_dotenv

# First party imports
from exodam_utils import ExodamDbConnectionInfo


def _get_old_env_vars() -> dict[str, str]:
    load_dotenv()

    return {
        "old_user": getenv("LOCAL_DB_USER", default="toto"),
        "old_password": getenv("LOCAL_DB_PASSWORD", default="toto_strong_password"),
        "old_host": getenv("LOCAL_DB_HOST", default="toto_db_host"),
        "old_port": getenv("LOCAL_DB_PORT", default="toto_db_port"),
        "old_db_name": getenv("LOCAL_DB_DATABASE_NAME", default="toto_db_name"),
    }


def _set_env_vars_with_old(old_vars: dict[str, str]) -> None:
    setenv["LOCAL_DB_USER"] = old_vars["old_user"]
    setenv["LOCAL_DB_PASSWORD"] = old_vars["old_password"]
    setenv["LOCAL_DB_HOST"] = old_vars["old_host"]
    setenv["LOCAL_DB_PORT"] = old_vars["old_port"]
    setenv["LOCAL_DB_DATABASE_NAME"] = old_vars["old_db_name"]


@pytest.fixture(scope="function")
def dummy_environment_vars():
    old_env_vars = _get_old_env_vars()

    setenv["LOCAL_DB_USER"] = "test_user"
    setenv["LOCAL_DB_PASSWORD"] = "dummy"
    setenv["LOCAL_DB_HOST"] = "test_localhost"
    setenv["LOCAL_DB_PORT"] = "5555"
    setenv["LOCAL_DB_DATABASE_NAME"] = "test_exodam"

    yield

    _set_env_vars_with_old(old_env_vars)


@pytest.fixture(scope="function")
def empty_some_environment_vars():
    old_env_vars = _get_old_env_vars()

    setenv["LOCAL_DB_PORT"] = ""
    setenv["LOCAL_DB_USER"] = ""

    yield

    _set_env_vars_with_old(old_env_vars)


@pytest.fixture(scope="function")
def ExodamDbConnectionInfo_lru_cache_clear():
    yield
    ExodamDbConnectionInfo.cache_clear()
