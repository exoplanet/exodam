# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exodam_utils import ExodamDbConnectionInfo, MissingEnvironmentVariableError
from exodam_utils.config import DbConnectionInfo


def test_DbConnectionInfo_success(
    dummy_environment_vars,
    ExodamDbConnectionInfo_lru_cache_clear,
):
    assert DbConnectionInfo(
        "LOCAL_DB_USER",
        "LOCAL_DB_PASSWORD",
        "LOCAL_DB_HOST",
        "LOCAL_DB_PORT",
        "LOCAL_DB_DATABASE_NAME",
    )


def test_ExodamDbConnectionInfo(
    dummy_environment_vars,
    ExodamDbConnectionInfo_lru_cache_clear,
):
    conn_info = ExodamDbConnectionInfo()
    with soft_assertions():
        assert_that(conn_info.user).is_equal_to("test_user")
        assert_that(conn_info.password).is_equal_to("dummy")
        assert_that(conn_info.host).is_equal_to("test_localhost")
        assert_that(conn_info.port).is_equal_to("5555")
        assert_that(conn_info.db_name).is_equal_to("test_exodam")


def test_DbConnectionInfo_missing_env_var(
    empty_some_environment_vars,
    ExodamDbConnectionInfo_lru_cache_clear,
):
    with pytest.raises(MissingEnvironmentVariableError):
        assert DbConnectionInfo(
            "LOCAL_DB_USER",
            "LOCAL_DB_PASSWORD",
            "LOCAL_DB_HOST",
            "LOCAL_DB_PORT",
            "LOCAL_DB_DATABASE_NAME",
        )
