# Pytest imports
import pytest
from pytest import param

# Standard imports
from pathlib import Path
from random import randint, shuffle

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exodict import (
    EditorialGenerateParams,
    ExodictEditorialName,
    ExodictScientificName,
    MandatoryFieldError,
    ScientificGenerateParams,
)

FILE_PATH = Path(__file__)


def _random_upper_lower_str(given_str: str) -> str:
    """
    Randomize upper and lower characters of a string.

    Args:
        given_str: Given string.

    Returns:
        Given string in lowercase with some random uupper case character.
    """
    manipulated_str = given_str.lower()
    nb_upper_char = randint(2, len(manipulated_str) - 1)
    range_given_str = list(range(0, len(manipulated_str)))
    shuffle(range_given_str)
    idx_char_to_upper = range_given_str[0:nb_upper_char]

    res = list(manipulated_str)
    for idx in idx_char_to_upper:
        res[idx] = res[idx].upper()

    return "".join(res)


@pytest.mark.parametrize(
    "exodict_type, given_str, expected_res",
    [
        # SCIENTIFIC
        param(
            ExodictScientificName,
            "exo_type",
            ExodictScientificName.EXO_TYPE,
            id="exo type resource",
        ),
        param(
            ExodictScientificName,
            "identification",
            ExodictScientificName.IDENTIFICATION,
            id="identification resource",
        ),
        param(
            ExodictScientificName,
            "internal",
            ExodictScientificName.INTERNAL,
            id="internal resource",
        ),
        param(
            ExodictScientificName,
            "parameters",
            ExodictScientificName.PARAMETERS,
            id="parameters resource",
        ),
        param(
            ExodictScientificName,
            "reference",
            ExodictScientificName.REFERENCE,
            id="reference resource",
        ),
        param(
            ExodictScientificName,
            "relationship",
            ExodictScientificName.RELATIONSHIP,
            id="relationship resource",
        ),
        # EDITORIAL
        param(
            ExodictEditorialName,
            "news",
            ExodictEditorialName.NEWS,
            id="news resource",
        ),
    ],
)
@pytest.mark.parametrize(
    "funct_to_apply",
    [
        param(str.lower, id="str.lower"),
        param(str.upper, id="str.upper"),
        param(str.title, id="str.title"),
        param(_random_upper_lower_str, id="random upper and lower"),
    ],
)
def test_exodict_name_from_str(exodict_type, funct_to_apply, given_str, expected_res):
    with soft_assertions():
        assert_that(exodict_type.from_str(funct_to_apply(given_str))).is_equal_to(
            expected_res
        )


@pytest.mark.parametrize(
    "given_str, expected_error",
    [
        param(None, MandatoryFieldError, id="MandatoryFieldError"),
        param(12, TypeError, id="TypeError"),
        param("toto", ValueError, id="ValueError"),
    ],
)
def test_exodict_name_from_str_error(given_str, expected_error):
    with pytest.raises(expected_error):
        ExodictScientificName.from_str(given_str)


def test_len_python_enum_is_equal_to_sql_enum__exodict_scientific(
    db_connection_with_only_schema,
):
    cursor = db_connection_with_only_schema.cursor()
    cursor.execute(
        "SELECT COUNT(*) FROM unnest(enum_range(NULL::exodam.exodict_data_name))"
    )
    count_from_sql = cursor.fetchall()[0][0]

    assert_that(count_from_sql).is_equal_to(len(ExodictScientificName))


def test_len_python_enum_is_equal_to_sql_enum__exodict_editorial(
    db_connection_with_only_schema,
):
    cursor = db_connection_with_only_schema.cursor()
    cursor.execute(
        "SELECT COUNT(*) FROM "
        "unnest(enum_range(NULL::exodam.exodict_editorial_data_name))"
    )
    count_from_sql = cursor.fetchall()[0][0]

    assert_that(count_from_sql).is_equal_to(len(ExodictEditorialName))


@pytest.mark.parametrize(
    "g_exo_type",
    [
        param(FILE_PATH, id="given exotype"),
        param(None, id="no given exotype"),
    ],
)
@pytest.mark.parametrize(
    "g_identification",
    [
        param(FILE_PATH, id="given identification"),
        param(None, id="no given identification"),
    ],
)
@pytest.mark.parametrize(
    "g_internal",
    [
        param(FILE_PATH, id="given internal"),
        param(None, id="no given internal"),
    ],
)
@pytest.mark.parametrize(
    "g_parameters",
    [
        param(FILE_PATH, id="given parameters"),
        param(None, id="no given parameters"),
    ],
)
@pytest.mark.parametrize(
    "g_reference",
    [
        param(FILE_PATH, id="given reference"),
        param(None, id="no given reference"),
    ],
)
@pytest.mark.parametrize(
    "g_relationship",
    [
        param(FILE_PATH, id="given relationship"),
        param(None, id="no given relationship"),
    ],
)
def test_ScientificGenerateParams(
    g_exo_type,
    g_identification,
    g_internal,
    g_parameters,
    g_reference,
    g_relationship,
):
    res = ScientificGenerateParams(
        exo_type=g_exo_type,
        identification=g_identification,
        internal=g_internal,
        parameters=g_parameters,
        reference=g_reference,
        relationship=g_relationship,
    )
    with soft_assertions():
        assert_that(res.exo_type).is_equal_to(g_exo_type)
        assert_that(res.identification).is_equal_to(g_identification)
        assert_that(res.internal).is_equal_to(g_internal)
        assert_that(res.parameters).is_equal_to(g_parameters)
        assert_that(res.reference).is_equal_to(g_reference)
        assert_that(res.relationship).is_equal_to(g_relationship)
        assert_that(res.as_dict()).is_equal_to(
            {
                "exo_type": g_exo_type,
                "identification": g_identification,
                "internal": g_internal,
                "parameters": g_parameters,
                "reference": g_reference,
                "relationship": g_relationship,
            }
        )


@pytest.mark.parametrize(
    "g_news",
    [
        param(FILE_PATH, id="given news"),
        param(None, id="no given news"),
    ],
)
def test_EditorialGenerateParams(
    g_news,
):
    res = EditorialGenerateParams(
        news=g_news,
    )
    with soft_assertions():
        assert_that(res.news).is_equal_to(g_news)
        assert_that(res.as_dict()).is_equal_to({"news": g_news})
