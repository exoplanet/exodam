# Pytest imports
import pytest
from pytest import param

# Standard imports
from pathlib import Path
from typing import Any

# Third party imports
from assertpy import assert_that, soft_assertions
from psycopg import Connection
from py_linq_sql import SQLEnumerable

# First party imports
from exodam_utils import (
    BASE_DIR,
    ExodamSqlTables,
    get_refereed_id,
    load_json,
    load_yaml,
)
from exodict import (
    EditorialGenerateParams,
    ExodictEditorialName,
    ExodictType,
    generate_and_insert_exodict,
    generate_exodict_and_write_in_file,
    merge_exodict,
)
from exodict.generate.generate_exodict import _generate_exodict

TEST_FILES_PATH = BASE_DIR / "tests/test_files/generate_exodict/"

EXODICT_EXPECTED_PATH = TEST_FILES_PATH / "exodict/expected/editorial"

NEW_EXODICT_PATH = TEST_FILES_PATH / "new_exodict/editorial"
NEW_EXODICT_DATA_PATH = NEW_EXODICT_PATH / "data"
NEW_EXODICT_EXPECTED_PATH = NEW_EXODICT_PATH / "expected"

EXPECTED_NAMES = [
    "news",
]


def _get_count(conn: Connection, table: str) -> int:
    """
    Get count of elements in a table.

    Args:
        conn: Connection to the database.
        table: table for the request.

    Returns:
        Number of element in the given table.
    """
    sqle_count = SQLEnumerable(conn, table).select().count().execute()
    return sqle_count[0].count


def _get_expected_from_files(resources: list[Path]) -> dict[str, Any]:
    result = {}
    for path in resources:
        result.update(load_json(path))
    return result


NEWS_TEST_PARAMS = [
    param(
        load_yaml(NEW_EXODICT_DATA_PATH / "news.yml"),
        NEW_EXODICT_EXPECTED_PATH / "news.json",
        id="New news",
    ),
    param(
        None,
        EXODICT_EXPECTED_PATH / "news.json",
        id="No new news",
    ),
]


# g_<name> for given, e_<name> for expected
@pytest.mark.parametrize("g_news, e_news", NEWS_TEST_PARAMS)
def test_generate_editorial_exodict_succeeds(
    db_connection_with_data,
    g_news,
    e_news,
):
    generated_exodict = _generate_exodict(
        db_connection_with_data,
        EditorialGenerateParams(news=g_news).as_dict(),
        ExodictType.EDITORIAL,
    )

    merged_generated_exodict = merge_exodict(generated_exodict)

    expected = _get_expected_from_files(
        [
            e_news,
        ]
    )

    with soft_assertions():
        # Check exodict sections
        assert_that(merged_generated_exodict).contains_key("news")

        # Check parts by parts
        assert_that(generated_exodict["news"]).is_equal_to(load_json(e_news))

        # Check whole exodict
        assert_that(merged_generated_exodict).is_equal_to(expected)


# g_<name> for given, e_<name> for expected
@pytest.mark.parametrize("g_news, e_news", NEWS_TEST_PARAMS)
def test_generate_and_write_in_file_editorial_exodict(
    db_connection_with_data,
    tmp_path,
    g_news,
    e_news,
):
    dir_path = tmp_path / "test_generated_exodict"
    dir_path.mkdir()
    export_path = dir_path / "export_exodict.exodict.json"

    generate_exodict_and_write_in_file(
        export_path,
        EditorialGenerateParams(news=g_news),
        db_connection_with_data,
    )

    generated_exodict = load_json(export_path)
    expected = _get_expected_from_files(
        [
            e_news,
        ]
    )
    # assert_that(generated_exodict).is_equal_to(expected)
    generated_exodict == expected


# g_<name> for given, e_<name> for expected
@pytest.mark.parametrize("g_news, e_news", NEWS_TEST_PARAMS)
def test_generate_and_insert_exodict_success(
    db_connection_with_data_for_modif,
    g_news,
    e_news,
):
    conn = db_connection_with_data_for_modif
    exodict_table = ExodamSqlTables.exodict_editorial.name
    exodict_data_table = ExodamSqlTables.exodict_editorial_data.name
    exodict2data_table = ExodamSqlTables.exodict_editorial2data.name

    count_before_exodict = _get_count(conn, exodict_table)
    count_before_data = _get_count(conn, exodict_data_table)
    count_before_2data = _get_count(conn, exodict2data_table)

    generate_and_insert_exodict(
        EditorialGenerateParams(news=g_news),
        ExodictType.EDITORIAL,
        conn,
    )

    count_after_exodict = _get_count(conn, exodict_table)
    count_after_data = _get_count(conn, exodict_data_table)
    count_after_2data = _get_count(conn, exodict2data_table)

    check_exodict = (
        SQLEnumerable(conn, exodict_table)
        .select()
        .order_by_descending(lambda x: x.id)
        .execute()
    )

    exodict_id = get_refereed_id(conn, ExodamSqlTables.exodict_editorial)
    obj_to_check_exodict = check_exodict.to_list()[0]
    other_exodict = check_exodict.to_list()[1:]
    all_other_exodict_refereed_value = [res.refereed for res in other_exodict]

    check_2data = (
        SQLEnumerable(conn, exodict2data_table)
        .select(
            lambda x: {
                "data_id": x.id_data,
                "name": x.name,
            }
        )
        .where(lambda x: x.id_exodict == exodict_id)
        .execute()
    )

    check_2data_list = check_2data.to_list()
    data_ids_and_name = [
        {
            "id": data.data_id,
            "name": data.name,
        }
        for data in check_2data_list
    ]
    names = [data.name for data in check_2data_list]

    check_data = {}
    for row in data_ids_and_name:
        res = (
            (
                SQLEnumerable(conn, exodict_data_table)
                .select(lambda x: x.data)
                .where(lambda x: x.id == row["id"])
                .take(1)
                .execute()
            )
            .to_list()[0]
            .data
        )
        check_data[row["name"]] = res

    expected = {
        "news": e_news,
    }

    with soft_assertions():
        # Count
        assert_that(count_after_exodict).is_equal_to(count_before_exodict + 1)
        assert_that(count_after_data).is_equal_to(
            count_before_data + len(ExodictEditorialName)
        )
        assert_that(count_after_2data).is_equal_to(
            count_before_2data + len(ExodictEditorialName)
        )

        # # refereed
        assert_that(obj_to_check_exodict.refereed).is_true()
        assert_that(all_other_exodict_refereed_value).contains_only(False)

        # len of check_2data
        assert_that(len(check_2data_list)).is_equal_to(len(ExodictEditorialName))

        # exodict names
        assert_that(names).contains_only(*EXPECTED_NAMES)

        # data
        for name, data in check_data.items():
            assert_that(data).is_equal_to(load_json(expected[name]))
