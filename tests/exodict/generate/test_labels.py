# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType, LabelError, check_labels
from exodict.generate.check_labels import _is_valid_label
from exodict.generate.generate_exodict import _generate_exodict

TEST_PATH = BASE_DIR / "tests/test_files/generate_exodict/"


def test_check_labels_succeeds(db_connection_with_data):
    generated_exodict = _generate_exodict(
        db_connection_with_data,
        {
            "exo_type": None,
            "identification": None,
            "internal": None,
            "parameters": None,
            "reference": None,
            "relationship": None,
        },
        ExodictType.SCIENTIFIC,
    )
    try:
        check_labels(generated_exodict)
    except LabelError as err:
        pytest.fail(f"Tests fail because: {err}")


# The label error is in internal -> display_status
def test_check_labels_exception_label_error(
    db_connection_with_data,
):
    with pytest.raises(LabelError):
        _generate_exodict(
            db_connection_with_data,
            {
                "exo_type": None,
                "identification": None,
                "internal": load_yaml(TEST_PATH / "internal_label_error.yml"),
                "parameters": None,
                "reference": None,
                "relationship": None,
            },
            ExodictType.SCIENTIFIC,
        )


@pytest.mark.parametrize(
    "long_label, is_long_valid",
    [
        param("it is a pretty long label", True, id="valid long label"),
        param(12, False, id="long not str"),
        param(None, False, id="long is None"),
    ],
)
@pytest.mark.parametrize(
    "short_label, is_short_valid",
    [
        param("teff.", True, id="valid short label without mathml"),
        param(
            "<math><msub><mi>T</mi><mi>conj.</mi></msub></math>",
            True,
            id="valid short label with long mathml",
        ),
        param("<math>toto</math>", True, id="valid short label with short mathml"),
        param(
            "too long short labels aaaaaaaaaaaaaaaaaaaaa",
            False,
            id="too long short label",
        ),
        param(12, False, id="short not str"),
        param(None, True, id="short is None"),
    ],
)
@pytest.mark.parametrize(
    "ascii_label, is_ascii_valid",
    [
        param("it_is_an_ascii_label", True, id="valid ascii label"),
        param(12, False, id="ascii not str"),
        param(None, False, id="ascii is None"),
        param("éaa", False, id="ascii not ascii"),
        param("9a-abc", False, id="ascii not valid header"),
    ],
)
def test_is_valid_label_result(
    long_label,
    short_label,
    ascii_label,
    is_long_valid,
    is_short_valid,
    is_ascii_valid,
):
    labels = {"long": long_label, "short": short_label, "full_ascii": ascii_label}
    assert_that(_is_valid_label("toto", labels)[0]).is_equal_to(
        all([is_ascii_valid, is_long_valid, is_short_valid])
    )


@pytest.mark.parametrize(
    "label, key, expected_msg",
    [
        param("sh. label", "short", "", id="all valid"),
        param(None, "long", "toto: The 'long' label is mandatory", id="long is None"),
        param(
            12, "long", "toto: The 'long' label must be a string", id="long is not str"
        ),
        param(
            None,
            "full_ascii",
            "toto: The 'full_ascii' label is mandatory",
            id="ascii is None",
        ),
        param(
            12,
            "full_ascii",
            "toto: The 'full_ascii' label must be a string",
            id="ascii is not str",
        ),
        param(
            "éaa",
            "full_ascii",
            "toto: The 'full_ascii' label must be write in ASCII",
            id="ascii is not ascii",
        ),
        param(
            "9a-abc",
            "full_ascii",
            "toto: The 'full_ascii' label must be a valid csv header (begin with "
            "underscore or all letter and contains only letter, underscore and digits)",
            id="ascii is not valid header",
        ),
        param(
            12,
            "short",
            "toto: The 'short' label must be a string",
            id="short is too long",
        ),
        param(
            "short label",
            "short",
            "toto: The 'short' label must be shorter of the 'long' label",
            id="short is not str",
        ),
    ],
)
def test_is_valid_label_return_message(label, key, expected_msg):
    labels = {"short": "sh. label", "long": "long label", "full_ascii": "ascii_label"}
    labels[key] = label
    msg = _is_valid_label("toto", labels)[1]

    assert_that(msg).is_equal_to(expected_msg)
