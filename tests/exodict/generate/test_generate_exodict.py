# Pytest imports
import pytest

# First party imports
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType, MissingMandatoryKeyError
from exodict.generate.generate_exodict import _generate_exodict

TEST_FILES_PATH = BASE_DIR / "tests/test_files/generate_exodict/"


def test_generate_exodict_duplicate_key_error(db_connection_with_data):
    with pytest.raises(KeyError):
        _generate_exodict(
            db_connection_with_data,
            {
                "exo_type": None,
                "identification": load_yaml(TEST_FILES_PATH / "internal_key_error.yml"),
                "internal": load_yaml(TEST_FILES_PATH / "internal_key_error.yml"),
                "parameters": None,
                "reference": None,
                "relationship": None,
            },
            ExodictType.SCIENTIFIC,
        )


# Missing key: identification -> name
def test_generate_exodict_missing_key_error(db_connection_with_data):
    with pytest.raises(MissingMandatoryKeyError):
        _generate_exodict(
            db_connection_with_data,
            {
                "exo_type": None,
                "identification": load_yaml(
                    TEST_FILES_PATH / "identification_missing_name.yml"
                ),
                "internal": load_yaml(TEST_FILES_PATH / "internal_key_error.yml"),
                "parameters": None,
                "reference": None,
                "relationship": None,
            },
            ExodictType.SCIENTIFIC,
        )
