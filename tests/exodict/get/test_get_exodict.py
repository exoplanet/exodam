# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that
from py_linq_sql import PyLinqSQLError

# First party imports
from exodam_utils import BASE_DIR, load_yaml
from exodict import (
    ExodictEditorialName,
    ExodictScientificName,
    ExodictType,
    get_exodict,
    merge_exodict,
)

EXODICT_EXPECT_PATH = BASE_DIR / "tests/test_files/get/exodict"

EXPECTED_DATA_SCIENTIFIC = {
    "exo_type": load_yaml(EXODICT_EXPECT_PATH / "exo_type.yml"),
    "identification": load_yaml(EXODICT_EXPECT_PATH / "identification.yml"),
    "internal": load_yaml(EXODICT_EXPECT_PATH / "internal.yml"),
    "parameters": load_yaml(EXODICT_EXPECT_PATH / "parameters.yml"),
    "reference": load_yaml(EXODICT_EXPECT_PATH / "reference.yml"),
    "relationship": load_yaml(EXODICT_EXPECT_PATH / "relationship.yml"),
}

EXPECTED_DATA_EDITORIAL = {
    "news": load_yaml(EXODICT_EXPECT_PATH / "news.yml"),
}


@pytest.mark.parametrize(
    "name, exodict_type, expected",
    [
        # SCIENTIFIC TYPE
        param(
            ExodictScientificName.EXO_TYPE,
            ExodictType.SCIENTIFIC,
            EXPECTED_DATA_SCIENTIFIC["exo_type"],
            id="Name = exo_type, Type = scientific",
        ),
        param(
            ExodictScientificName.IDENTIFICATION,
            ExodictType.SCIENTIFIC,
            EXPECTED_DATA_SCIENTIFIC["identification"],
            id="Name = identification, Type = scientific",
        ),
        param(
            ExodictScientificName.INTERNAL,
            ExodictType.SCIENTIFIC,
            EXPECTED_DATA_SCIENTIFIC["internal"],
            id="Name = internal, Type = scientific",
        ),
        param(
            ExodictScientificName.PARAMETERS,
            ExodictType.SCIENTIFIC,
            EXPECTED_DATA_SCIENTIFIC["parameters"],
            id="Name = parameters, Type = scientific",
        ),
        param(
            ExodictScientificName.REFERENCE,
            ExodictType.SCIENTIFIC,
            EXPECTED_DATA_SCIENTIFIC["reference"],
            id="Name = reference, Type = scientific",
        ),
        param(
            ExodictScientificName.RELATIONSHIP,
            ExodictType.SCIENTIFIC,
            EXPECTED_DATA_SCIENTIFIC["relationship"],
            id="Name = relationship, Type = scientific",
        ),
        param(
            None,
            ExodictType.SCIENTIFIC,
            merge_exodict(EXPECTED_DATA_SCIENTIFIC),
            id="all resources, Type = scientific",
        ),
        # EDITORIAL TYPE
        param(
            ExodictEditorialName.NEWS,
            ExodictType.EDITORIAL,
            EXPECTED_DATA_EDITORIAL["news"],
            id="Name = news, Type = editorial",
        ),
        param(
            None,
            ExodictType.EDITORIAL,
            merge_exodict(EXPECTED_DATA_EDITORIAL),
            id="all resources, Type = editorial",
        ),
    ],
)
def test_get_exodict_success(db_connection_with_data, name, exodict_type, expected):
    assert_that(
        get_exodict(
            exodict_type,
            db_connection_with_data,
            name,
        )
    ).is_equal_to(expected)


@pytest.mark.parametrize(
    "name_with_bad_type",
    [
        param(1, id="int"),
        param(1.2, id="float"),
        param("toto", id="str"),
        param(True, id="bool"),
        param({"a": 1}, id="dict"),
        param([1, 2], id="list"),
    ],
)
@pytest.mark.parametrize(
    "exodict_type",
    [
        param(ExodictType.SCIENTIFIC, id="scientific type"),
        param(ExodictType.EDITORIAL, id="editorial type"),
    ],
)
def test_get_exodict_raises_ValueError_if_given_a_name_with_bad_type(
    db_connection_with_data, name_with_bad_type, exodict_type
):
    with pytest.raises(ValueError):
        get_exodict(exodict_type, db_connection_with_data, name_with_bad_type)


@pytest.mark.parametrize(
    "sql_failing_func",
    [
        "exodict.get.get_exodict_funcs.conn_manager",
        "exodict.get.get_exodict_funcs.get_refereed_id",
        "exodict.get.get_exodict_funcs.SQLEnumerable",
        "exodict.get.get_exodict_funcs.SQLEnumerable.join",
        "exodict.get.get_exodict_funcs.SQLEnumerable.where",
        "exodict.get.get_exodict_funcs.SQLEnumerable.execute",
    ],
)
@pytest.mark.parametrize(
    "exodict_type",
    [
        param(ExodictType.SCIENTIFIC, id="scientific type"),
        param(ExodictType.EDITORIAL, id="editorial type"),
    ],
)
def test_get_exodict_raises_PyLinqSQLError_if_connexion_fails(
    mocker, db_connection_with_data, sql_failing_func, exodict_type
):
    # Let's fake the call to the underlying func since we are just testing get_exodict
    fake_func = mocker.patch(
        sql_failing_func,
        side_effect=PyLinqSQLError,
    )

    with pytest.raises(PyLinqSQLError):
        get_exodict(exodict_type, db_connection_with_data)

    # Was the underlying function called?
    fake_func.assert_called_once()
