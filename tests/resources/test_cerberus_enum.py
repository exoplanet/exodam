# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from exodam_resources import (
    EditorialCerberusName,
    EditorialCerberusType,
    ScientificCerberusName,
    ScientificCerberusType,
)


@pytest.mark.parametrize(
    "sql_enum, python_enum",
    [
        param(
            "exodam.exodam_cerberus_data_name",
            ScientificCerberusName,
            id="Scientific Cerberus Name",
        ),
        param(
            "exodam.exodam_cerberus_type",
            ScientificCerberusType,
            id="Scientific Cerberus Type",
        ),
        param(
            "exodam.exodam_editorial_cerberus_data_name",
            EditorialCerberusName,
            id="Editorial Cerberus Name",
        ),
        param(
            "exodam.exodam_editorial_cerberus_type",
            EditorialCerberusType,
            id="Editorial Cerberus Type",
        ),
    ],
)
def test_len_python_enum_is_equal_to_sql_enum__cerberus(
    db_connection_with_only_schema,
    sql_enum,
    python_enum,
):
    cursor = db_connection_with_only_schema.cursor()
    cursor.execute(f"SELECT COUNT(*) FROM unnest(enum_range(NULL::{sql_enum}))")
    count_from_sql = cursor.fetchall()[0][0]

    assert_that(count_from_sql).is_equal_to(len(python_enum))
