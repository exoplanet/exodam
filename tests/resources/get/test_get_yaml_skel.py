# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from exodam_resources import get_yml_skel
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType

EXPECTED_DATA = {
    "scienitific": load_yaml(BASE_DIR / "tests/test_files/generate/yaml_skel.yaml"),
    "editorial": load_yaml(
        BASE_DIR / "tests/test_files/generate/yaml_editorial_skel.yml"
    ),
}


@pytest.mark.parametrize(
    "exodict_type, expected",
    [
        param(
            ExodictType.SCIENTIFIC,
            EXPECTED_DATA["scienitific"],
            id="scientific skel",
        ),
        param(
            ExodictType.EDITORIAL,
            EXPECTED_DATA["editorial"],
            id="editorial skel",
        ),
    ],
)
def test_get_yaml_skel(db_connection_with_data, exodict_type, expected):
    assert_that(get_yml_skel(exodict_type, db_connection_with_data)).is_equal_to(
        expected
    )
