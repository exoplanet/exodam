# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from exodam_resources import (
    EditorialCerberusName,
    EditorialCerberusType,
    ScientificCerberusName,
    ScientificCerberusType,
    get_cerberus_content,
)
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType

CERBERUS_EXPECT_PATH = BASE_DIR / "tests/test_files/get/cerberus_files"

EXPECTED_SCIENTIFIC_DATA = {
    "type__objects": {
        "identity": load_yaml(CERBERUS_EXPECT_PATH / "identity.yaml"),
        "internal": load_yaml(CERBERUS_EXPECT_PATH / "internal.yaml"),
        "parameters": load_yaml(CERBERUS_EXPECT_PATH / "parameters.yaml"),
        "references": load_yaml(CERBERUS_EXPECT_PATH / "references.yaml"),
    },
    "type__permissive": {
        "permissive": load_yaml(CERBERUS_EXPECT_PATH / "permissive.yaml")
    },
    "type__base": {
        "units": load_yaml(CERBERUS_EXPECT_PATH / "units.yaml"),
        "relationship": load_yaml(CERBERUS_EXPECT_PATH / "relationship.yaml"),
        "keys": load_yaml(CERBERUS_EXPECT_PATH / "keys.yaml"),
        "objects_type": load_yaml(CERBERUS_EXPECT_PATH / "objects_type.yaml"),
    },
    "all": {
        "identity": load_yaml(CERBERUS_EXPECT_PATH / "identity.yaml"),
        "internal": load_yaml(CERBERUS_EXPECT_PATH / "internal.yaml"),
        "parameters": load_yaml(CERBERUS_EXPECT_PATH / "parameters.yaml"),
        "references": load_yaml(CERBERUS_EXPECT_PATH / "references.yaml"),
        "permissive": load_yaml(CERBERUS_EXPECT_PATH / "permissive.yaml"),
        "keys": load_yaml(CERBERUS_EXPECT_PATH / "keys.yaml"),
        "objects_type": load_yaml(CERBERUS_EXPECT_PATH / "objects_type.yaml"),
        "relationship": load_yaml(CERBERUS_EXPECT_PATH / "relationship.yaml"),
        "units": load_yaml(CERBERUS_EXPECT_PATH / "units.yaml"),
    },
}

EXPECTED_EDITORIAL_DATA = {
    "type__base": {
        "news": load_yaml(CERBERUS_EXPECT_PATH / "news.yaml"),
    },
    "all": {
        "news": load_yaml(CERBERUS_EXPECT_PATH / "news.yaml"),
    },
}


@pytest.mark.parametrize(
    "name_or_type, exodict_type, expected",
    [
        # SCIENTIFIC Name
        param(
            ScientificCerberusName.IDENTITY,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__objects"]["identity"],
            id="Scientific - Name = identity",
        ),
        param(
            ScientificCerberusName.INTERNAL,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__objects"]["internal"],
            id="Scientific - Name = internal",
        ),
        param(
            ScientificCerberusName.PARAMETERS,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__objects"]["parameters"],
            id="Scientific - Name = parameters",
        ),
        param(
            ScientificCerberusName.REFERENCES,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__objects"]["references"],
            id="Scientific - Name = references",
        ),
        param(
            ScientificCerberusName.PERMISSIVE,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__permissive"]["permissive"],
            id="Scientific - Name = permissive",
        ),
        param(
            ScientificCerberusName.KEYS,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__base"]["keys"],
            id="Scientific - Name = keys",
        ),
        param(
            ScientificCerberusName.OBJECTS_TYPE,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__base"]["objects_type"],
            id="Scientific - Name = objects_type",
        ),
        param(
            ScientificCerberusName.RELATIONSHIP,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__base"]["relationship"],
            id="Scientific - Name = relationship",
        ),
        param(
            ScientificCerberusName.UNITS,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__base"]["units"],
            id="Scientific - Name = units",
        ),
        # SCIENTIFIC Type
        param(
            ScientificCerberusType.TYPE__OBJECTS,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__objects"],
            id="Scientific - Type = objects",
        ),
        param(
            ScientificCerberusType.TYPE__PERMISSIVE,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__permissive"],
            id="Scientific - Type = permissive",
        ),
        param(
            ScientificCerberusType.TYPE__BASE,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["type__base"],
            id="Scientific - Type = base",
        ),
        # SCIENTIFIC All
        param(
            None,
            ExodictType.SCIENTIFIC,
            EXPECTED_SCIENTIFIC_DATA["all"],
            id="Scientific - all resources",
        ),
        #
        # EDITORIAL Name
        param(
            EditorialCerberusName.NEWS,
            ExodictType.EDITORIAL,
            EXPECTED_EDITORIAL_DATA["type__base"]["news"],
            id="Editorial - Name = news",
        ),
        # EDITORIAL Type
        param(
            EditorialCerberusType.TYPE__BASE,
            ExodictType.EDITORIAL,
            EXPECTED_EDITORIAL_DATA["type__base"],
            id="Editorial - Type = base",
        ),
        # EDITORIAL All
        param(
            None,
            ExodictType.EDITORIAL,
            EXPECTED_EDITORIAL_DATA["all"],
            id="Editorial - all resources",
        ),
    ],
)
def test_get_cerberus_success(
    db_connection_with_data,
    name_or_type,
    exodict_type,
    expected,
):
    assert_that(
        get_cerberus_content(exodict_type, db_connection_with_data, name_or_type)
    ).is_equal_to(expected)


@pytest.mark.parametrize(
    "name_or_type",
    [
        param(1, id="int"),
        param(1.2, id="float"),
        param("toto", id="str"),
        param(True, id="bool"),
        param({"a": 1}, id="dict"),
        param([1, 2], id="list"),
    ],
)
@pytest.mark.parametrize(
    "exodict_type",
    [
        param(ExodictType.SCIENTIFIC, id="scientific type"),
        param(ExodictType.EDITORIAL, id="editorial type"),
    ],
)
def test_get_cerberus_ValueError(db_connection_with_data, name_or_type, exodict_type):
    with pytest.raises(ValueError):
        get_cerberus_content(exodict_type, db_connection_with_data, name_or_type)
