# Third party imports
from assertpy import assert_that

# First party imports
from exodam_resources import get_vupnu_keys
from exodam_utils import BASE_DIR, load_yaml

EXPECTED_DATA = set(
    load_yaml(BASE_DIR / "tests/test_files/get/vupnu_key.yaml")["vupnu_keys"]
)


def test_get_known_keys(db_connection_with_data):
    assert_that(set(get_vupnu_keys(db_connection_with_data))).is_equal_to(EXPECTED_DATA)
