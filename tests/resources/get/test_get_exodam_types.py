# Third party imports
from assertpy import assert_that

# First party imports
from exodam_resources import get_exodam_types
from exodam_utils import BASE_DIR, load_yaml

EXPECTED_DATA = set(
    load_yaml(BASE_DIR / "tests/test_files/generate/exodam_types.yaml")["exodam_types"]
)


def test_get_exodam_types(db_connection_with_data):
    assert_that(set(get_exodam_types(db_connection_with_data))).is_equal_to(
        EXPECTED_DATA
    )
