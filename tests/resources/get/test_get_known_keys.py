# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from exodam_resources import get_known_keys
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType

EXPECTED_DATA = {
    "scientific": set(
        load_yaml(BASE_DIR / "tests/test_files/generate/known_keys.yaml")["known_keys"]
    ),
    "editorial": set(
        load_yaml(BASE_DIR / "tests/test_files/generate/editorial_known_keys.yaml")[
            "known_keys"
        ]
    ),
}


@pytest.mark.parametrize(
    "exodict_type, expected",
    [
        param(
            ExodictType.SCIENTIFIC,
            EXPECTED_DATA["scientific"],
            id="scientific known keys",
        ),
        param(
            ExodictType.EDITORIAL,
            EXPECTED_DATA["editorial"],
            id="editorial known keys",
        ),
    ],
)
def test_get_known_keys(db_connection_with_data, exodict_type, expected):
    assert_that(set(get_known_keys(exodict_type, db_connection_with_data))).is_equal_to(
        expected
    )
