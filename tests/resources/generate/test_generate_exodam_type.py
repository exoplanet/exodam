# Third party imports
from assertpy import assert_that

# First party imports
from exodam_resources import generate_and_insert_exodam_types, generate_exodam_types
from exodam_utils import BASE_DIR, ExodamSqlTables, load_yaml

# Local imports
from .utils import assert_for_generate_and_insert_function

EXPECTED_DATA = load_yaml(BASE_DIR / "tests/test_files/generate/exodam_types.yaml")


def test_generate_exodam_types(db_connection_with_data_for_modif):
    generated_exodam_types = generate_exodam_types(db_connection_with_data_for_modif)
    assert_that({"exodam_types": generated_exodam_types}).is_equal_to(EXPECTED_DATA)


def test_generate_and_insert_exodam_type(db_connection_with_data_for_modif):
    assert_for_generate_and_insert_function(
        db_connection_with_data_for_modif,
        ExodamSqlTables.exodam_types.name,
        generate_and_insert_exodam_types,
        EXPECTED_DATA,
    )
