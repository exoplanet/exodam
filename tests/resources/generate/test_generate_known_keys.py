# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions
from py_linq_sql import SQLEnumerable

# First party imports
from exodam_resources import generate_and_insert_known_keys, generate_keys
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType

# Local imports
from .utils import get_count

EXPECTED_DATA = {
    "scienitific": load_yaml(BASE_DIR / "tests/test_files/generate/known_keys.yaml"),
    "editorial": load_yaml(
        BASE_DIR / "tests/test_files/generate/editorial_known_keys.yaml"
    ),
}


@pytest.mark.parametrize(
    "exodict_type, expected",
    [
        param(ExodictType.SCIENTIFIC, EXPECTED_DATA["scienitific"], id="scientific"),
        param(ExodictType.EDITORIAL, EXPECTED_DATA["editorial"], id="editorial"),
    ],
)
def test_generate_known_keys(db_connection_with_data_for_modif, exodict_type, expected):
    generated_known_keys = set(
        generate_keys(db_connection_with_data_for_modif, exodict_type)
    )

    assert_that(generated_known_keys).is_equal_to(set(expected["known_keys"]))


@pytest.mark.parametrize(
    "exodict_type, expected",
    [
        param(ExodictType.SCIENTIFIC, EXPECTED_DATA["scienitific"], id="scientific"),
        param(ExodictType.EDITORIAL, EXPECTED_DATA["editorial"], id="editorial"),
    ],
)
def test_generate_and_insert_known_keys(
    db_connection_with_data_for_modif,
    exodict_type,
    expected,
):
    conn = db_connection_with_data_for_modif
    table = exodict_type.value.known_keys.name

    count_before = get_count(conn, table)

    generate_and_insert_known_keys(exodict_type, conn)

    count_after = get_count(conn, table)

    check = (
        SQLEnumerable(conn, table)
        .select()
        .order_by_descending(lambda x: x.id)
        .execute()
    )

    obj_to_check, other = check.to_list()[0], check.to_list()[1:]

    all_other_refereed_value = [res.refereed for res in other]

    with soft_assertions():
        assert_that(obj_to_check.refereed).is_true()
        assert_that(set(obj_to_check.data["known_keys"])).is_equal_to(
            set(expected["known_keys"])
        )
        assert_that(count_after).is_equal_to(count_before + 1)
        assert_that(all_other_refereed_value).contains_only(False)
