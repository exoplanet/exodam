# Third party imports
from assertpy import assert_that

# First party imports
from exodam_resources import generate_and_insert_vupnu_keys, generate_vupnu_keys
from exodam_utils import BASE_DIR, ExodamSqlTables, load_yaml

# Local imports
from .utils import assert_for_generate_and_insert_function

EXPECTED_DATA = load_yaml(BASE_DIR / "tests/test_files/get/vupnu_key.yaml")


def test_generate_known_keys():
    generated_vupnu_keys = generate_vupnu_keys()
    assert_that({"vupnu_keys": generated_vupnu_keys}).is_equal_to(EXPECTED_DATA)


def test_generate_and_insert_vupnu_keys(db_connection_with_data_for_modif):
    assert_for_generate_and_insert_function(
        db_connection_with_data_for_modif,
        ExodamSqlTables.exodam_vupnu_keys.name,
        generate_and_insert_vupnu_keys,
        EXPECTED_DATA,
    )
