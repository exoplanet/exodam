# Standard imports
from typing import Any, Callable

# Third party imports
from assertpy import assert_that, soft_assertions
from psycopg import Connection
from py_linq_sql import SQLEnumerable

# First party imports
from exodam import generate_cerberus_and_insert
from exodam_resources.utils import CerberusName
from exodam_utils import Table, get_refereed_id
from exodict import ExodictType


def get_count(conn: Connection, table: str) -> int:
    """
    Get count of elements in a table.

    Args:
        conn: Connection to the database.
        table: table for the request.

    Returns:
        Number of element in the given table.
    """
    sqle_count = SQLEnumerable(conn, table).select().count().execute()
    return sqle_count[0].count


def assert_for_generate_and_insert_function(
    conn: Connection,
    table: str,
    generate_funct: Callable,
    expected_data: dict[str, Any],
    set_on_data: bool = False,
) -> None:
    """
    Make assertions for a `generate_and_insert` function.

    Args:
        conn: Connection top a db for the insertion.
        table: Table for the insertion.
        generate_funct: Function use to generate and insert in db.
        expected_data: Expected data for object_to_check.data
        set_on_data: Convert data in set for the assert
    """
    count_before = get_count(conn, table)

    generate_funct(conn)

    count_after = get_count(conn, table)

    check = (
        SQLEnumerable(conn, table)
        .select()
        .order_by_descending(lambda x: x.id)
        .execute()
    )

    obj_to_check, other = check.to_list()[0], check.to_list()[1:]

    all_other_refereed_value = [res.refereed for res in other]

    with soft_assertions():
        assert_that(obj_to_check.refereed).is_true()
        if set_on_data:
            assert_that(set(obj_to_check.data)).is_equal_to(set(expected_data))
        else:
            assert_that(obj_to_check.data).is_equal_to(expected_data)
        assert_that(count_after).is_equal_to(count_before + 1)
        assert_that(all_other_refereed_value).contains_only(False)


def make_and_assert_generate_cerberus_and_insert(
    conn: Connection,
    exodict_type: ExodictType,
    cerberus_name: CerberusName,
    cerberus_table: Table,
    cerberus_data_table: Table,
    cerbersu2data_table: Table,
    expected_name: list[str],
    expected_type: list[str],
    expected_data: list[dict[str, Any]],
):
    count_before_cerberus = get_count(conn, cerberus_table.name)
    count_before_data = get_count(conn, cerberus_data_table.name)
    count_before_2data = get_count(conn, cerbersu2data_table.name)

    generate_cerberus_and_insert(exodict_type, conn)

    count_after_cerberus = get_count(conn, cerberus_table.name)
    count_after_data = get_count(conn, cerberus_data_table.name)
    count_after_2data = get_count(conn, cerbersu2data_table.name)

    check_cerberus = (
        SQLEnumerable(conn, cerberus_table.name)
        .select()
        .order_by_descending(lambda x: x.id)
        .execute()
    )

    cerberus_id = get_refereed_id(conn, cerberus_table)
    obj_to_check_cerberus = check_cerberus.to_list()[0]
    other_cerberus = check_cerberus.to_list()[1:]
    all_other_cerberus_refereed_value = [res.refereed for res in other_cerberus]

    check_2data = (
        SQLEnumerable(conn, cerbersu2data_table.name)
        .select(
            lambda x: {
                "data_id": x.id_cerberus_data,
                "name": x.name,
                "type": x.cerberus_type,
            }
        )
        .where(lambda x: x.id_cerberus == cerberus_id)
        .execute()
    )

    check_2data_list = check_2data.to_list()
    data_ids_and_name = [
        {
            "id": data.data_id,
            "name": data.name,
        }
        for data in check_2data_list
    ]
    names = [data.name for data in check_2data_list]
    cerberus_types = [data.type for data in check_2data_list]

    check_data = {}
    for row in data_ids_and_name:
        if row["name"] == "parameters":
            fquery = lambda x: {"data": x.data.parameters}
        else:
            fquery = lambda x: x.data
        res = (
            (
                SQLEnumerable(conn, cerberus_data_table.name)
                .select(fquery)
                .where(lambda x: x.id == row["id"])
                .take(1)
                .execute()
            )
            .to_list()[0]
            .data
        )
        check_data[row["name"]] = res

    with soft_assertions():
        # Count
        assert_that(count_after_cerberus).is_equal_to(count_before_cerberus + 1)
        assert_that(count_after_data).is_equal_to(
            count_before_data + len(cerberus_name)
        )
        assert_that(count_after_2data).is_equal_to(
            count_before_2data + len(cerberus_name)
        )

        # refereed
        assert_that(obj_to_check_cerberus.refereed).is_true()
        assert_that(all_other_cerberus_refereed_value).contains_only(False)

        # len of check_2data
        assert_that(len(check_2data_list)).is_equal_to(len(cerberus_name))

        # cerberus type and names
        assert_that(names).contains_only(*expected_name)
        assert_that(cerberus_types).contains_only(*expected_type)

        # data
        for name, data in check_data.items():
            assert_that(data).is_equal_to(expected_data[name])
