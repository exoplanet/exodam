# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from exodam_resources import EditorialCerberusName, ScientificCerberusName
from exodam_resources.generate.cerberus.generate_editorial_part_of_cerberus import (
    generate_news,
)
from exodam_resources.generate.cerberus.generate_scientific_part_of_cerberus import (
    generate_identity,
    generate_internal,
    generate_keys,
    generate_objects_type,
    generate_parameters,
    generate_permissive,
    generate_references,
    generate_relationship,
    generate_units,
)
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType

# Local imports
from .utils import make_and_assert_generate_cerberus_and_insert

CERBERUS_PATH = BASE_DIR / "tests/test_files/generate/cerberus"

EXPECTED_SCIENTIFIC_NAMES = [
    "identity",
    "internal",
    "parameters",
    "references",
    "permissive",
    "keys",
    "objects_type",
    "relationship",
    "units",
]
EXPECTED_EDITORIAL_NAMES = ["news"]

EXPECTED_SCIENTIFIC_TYPES = ["type__objects", "type__permissive", "type__base"]
EXPECTED_EDITORIAL_TYPES = ["type__base"]

EXPECTED_DATA = {
    "identity": load_yaml(CERBERUS_PATH / "identity.yaml"),
    "internal": load_yaml(CERBERUS_PATH / "internal.yaml"),
    "parameters": load_yaml(CERBERUS_PATH / "parameters.yaml")["parameters"],
    "references": load_yaml(CERBERUS_PATH / "references.yaml"),
    "permissive": load_yaml(CERBERUS_PATH / "permissive.yaml"),
    "keys": load_yaml(CERBERUS_PATH / "keys.yaml"),
    "objects_type": load_yaml(CERBERUS_PATH / "objects_type.yaml"),
    "relationship": load_yaml(CERBERUS_PATH / "relationship.yaml"),
    "units": load_yaml(CERBERUS_PATH / "units.yaml"),
    "news": load_yaml(CERBERUS_PATH / "news.yaml"),
}


@pytest.mark.parametrize(
    "funct, expected_content",
    [
        # SCIENTIFIC
        param(
            generate_identity,
            EXPECTED_DATA["identity"],
            id="Scientific - objects->identity",
        ),
        param(
            generate_internal,
            EXPECTED_DATA["internal"],
            id="Scientific - objects->internal",
        ),
        param(
            generate_keys,
            EXPECTED_DATA["keys"],
            id="Scientific - keys",
        ),
        param(
            generate_objects_type,
            EXPECTED_DATA["objects_type"],
            id="Scientific - objects_type",
        ),
        param(
            generate_parameters,
            EXPECTED_DATA["parameters"],
            id="Scientific - objects -> parameters",
        ),
        param(
            generate_permissive,
            EXPECTED_DATA["permissive"],
            id="Scientific - permissive->permissive",
        ),
        param(
            generate_references,
            EXPECTED_DATA["references"],
            id="Scientific - objects->references",
        ),
        param(
            generate_relationship,
            EXPECTED_DATA["relationship"],
            id="Scientific - relationship",
        ),
        param(
            generate_units,
            EXPECTED_DATA["units"],
            id="Scientific - units",
        ),
        # EDITORIAL
        param(
            generate_news,
            EXPECTED_DATA["news"],
            id="Editorial - news",
        ),
    ],
)
def test_generate_parts(funct, expected_content):
    assert_that(funct()).is_equal_to(expected_content)


@pytest.mark.parametrize(
    "exodict_type, cerberus_name, expect_name, expect_type",
    [
        param(
            ExodictType.SCIENTIFIC,
            ScientificCerberusName,
            EXPECTED_SCIENTIFIC_NAMES,
            EXPECTED_SCIENTIFIC_TYPES,
            id="SCIENTIFIC",
        ),
        param(
            ExodictType.EDITORIAL,
            EditorialCerberusName,
            EXPECTED_EDITORIAL_NAMES,
            EXPECTED_EDITORIAL_TYPES,
            id="EDITORIAL",
        ),
    ],
)
def test_generate_cerberus_and_insert(
    db_connection_with_data_for_modif,
    exodict_type,
    cerberus_name,
    expect_name,
    expect_type,
):
    make_and_assert_generate_cerberus_and_insert(
        db_connection_with_data_for_modif,
        exodict_type,
        cerberus_name,
        exodict_type.value.cerberus,
        exodict_type.value.cerberus_data,
        exodict_type.value.cerberus2data,
        expect_name,
        expect_type,
        EXPECTED_DATA,
    )
