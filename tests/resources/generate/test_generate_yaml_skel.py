# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions
from py_linq_sql import SQLEnumerable

# First party imports
from exodam_resources import generate_and_insert_yml_skel, generate_yml_skel
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType

# Local imports
from .utils import get_count

EXPECTED_DATA = {
    "scienitific": load_yaml(BASE_DIR / "tests/test_files/generate/yaml_skel.yaml"),
    "editorial": load_yaml(
        BASE_DIR / "tests/test_files/generate/yaml_editorial_skel.yml"
    ),
}


@pytest.mark.parametrize(
    "exodict_type, expected",
    [
        param(ExodictType.SCIENTIFIC, EXPECTED_DATA["scienitific"], id="scientific"),
        param(ExodictType.EDITORIAL, EXPECTED_DATA["editorial"], id="editorial"),
    ],
)
def test_generate_yaml_skel(db_connection_with_data_for_modif, exodict_type, expected):
    generated_skel = generate_yml_skel(db_connection_with_data_for_modif, exodict_type)
    assert_that(generated_skel).is_equal_to(expected)


@pytest.mark.parametrize(
    "exodict_type, expected",
    [
        param(ExodictType.SCIENTIFIC, EXPECTED_DATA["scienitific"], id="scientific"),
        param(ExodictType.EDITORIAL, EXPECTED_DATA["editorial"], id="editorial"),
    ],
)
def test_generate_and_insert_yaml_skel(
    db_connection_with_data_for_modif,
    exodict_type,
    expected,
):
    conn = db_connection_with_data_for_modif
    table = exodict_type.value.skel.name

    count_before = get_count(conn, table)

    generate_and_insert_yml_skel(exodict_type, conn)

    count_after = get_count(conn, table)

    check = (
        SQLEnumerable(conn, table)
        .select()
        .order_by_descending(lambda x: x.id)
        .execute()
    )

    obj_to_check, other = check.to_list()[0], check.to_list()[1:]

    all_other_refereed_value = [res.refereed for res in other]

    with soft_assertions():
        assert_that(obj_to_check.refereed).is_true()
        assert_that(obj_to_check.data).is_equal_to(expected)
        assert_that(count_after).is_equal_to(count_before + 1)
        assert_that(all_other_refereed_value).contains_only(False)
