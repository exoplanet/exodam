# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exodam import ExodamContent, finalize_for_api, finalize_for_cli
from exodam.finalize.finalize import _finalize_a_raw_data
from exodam_utils import BASE_DIR, load_json, load_yaml
from exodict import ExodictType

TEST_PATH_SCIENTIFIC = BASE_DIR / "tests/test_files/finalize/scientific/"
TEST_PATH_EDITORIAL = BASE_DIR / "tests/test_files/finalize/editorial/"


@pytest.mark.parametrize(
    "exodict_type, test_path",
    [
        param(ExodictType.SCIENTIFIC, TEST_PATH_SCIENTIFIC, id="scientific exodam"),
        param(ExodictType.EDITORIAL, TEST_PATH_EDITORIAL, id="editorial exodam"),
    ],
)
@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_finalize_raw_data_succeed(exodict_type, test_path, with_colors):
    result = _finalize_a_raw_data(
        load_yaml(test_path / "valid_file.yaml"),
        exodict_type,
        warning_messages=[],
        with_colors=with_colors,
    )
    expected = load_json(test_path / "expected_exodam_file.exodam.json")

    with soft_assertions():
        assert_that(result).is_not_none()
        assert_that(ExodamContent(result["data"])).is_equal_to(
            ExodamContent(expected["data"])
        )
        assert_that(ExodamContent(result["exodict"])).is_equal_to(
            ExodamContent(expected["exodict"])
        )
        assert_that(result["exodict_version"]).is_equal_to(expected["exodict_version"])


@pytest.mark.parametrize(
    "exodict_type, test_path",
    [
        param(ExodictType.SCIENTIFIC, TEST_PATH_SCIENTIFIC, id="scientific exodam"),
        param(ExodictType.EDITORIAL, TEST_PATH_EDITORIAL, id="editorial exodam"),
    ],
)
def test_finalize_for_api_succeed(exodict_type, test_path):
    result = finalize_for_api(
        load_yaml(test_path / "valid_file.yaml"),
        exodict_type,
        warning_messages=[],
    )
    expected = load_json(test_path / "expected_exodam_file.exodam.json")

    with soft_assertions():
        assert_that(result).is_not_none()
        assert_that(ExodamContent(result["data"])).is_equal_to(
            ExodamContent(expected["data"])
        )
        assert_that(ExodamContent(result["exodict"])).is_equal_to(
            ExodamContent(expected["exodict"])
        )
        assert_that(result["exodict_version"]).is_equal_to(expected["exodict_version"])


@pytest.mark.parametrize(
    "exodict_type, test_path",
    [
        param(ExodictType.SCIENTIFIC, TEST_PATH_SCIENTIFIC, id="scientific exodam"),
        param(ExodictType.EDITORIAL, TEST_PATH_EDITORIAL, id="editorial exodam"),
    ],
)
def test_finalize_for_cli_export_succeed(tmp_path, exodict_type, test_path):
    export_path_folder = tmp_path / "test_finalize"
    export_path_folder.mkdir()
    export_path = export_path_folder / "test_finalize.exodam.json"
    result = finalize_for_cli(
        test_path / "valid_file.yaml",
        exodict_type,
        export_path,
        warning_messages=[],
    )

    expected = load_json(test_path / "expected_exodam_file.exodam.json")
    check = load_json(export_path)

    with soft_assertions():
        assert_that(result).is_true()
        assert_that(ExodamContent(check["data"])).is_equal_to(
            ExodamContent(expected["data"])
        )
        assert_that(ExodamContent(check["exodict"])).is_equal_to(
            ExodamContent(expected["exodict"])
        )
        assert_that(check["exodict_version"]).is_equal_to(expected["exodict_version"])


@pytest.mark.parametrize(
    "exodict_type, test_path",
    [
        param(ExodictType.SCIENTIFIC, TEST_PATH_SCIENTIFIC, id="scientific exodam"),
        param(ExodictType.EDITORIAL, TEST_PATH_EDITORIAL, id="editorial exodam"),
    ],
)
@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_finalize_a_raw_data_failed(exodict_type, test_path, with_colors):
    assert_that(
        _finalize_a_raw_data(
            load_yaml(test_path / "none_valid.yaml"),
            exodict_type,
            warning_messages=[],
            with_colors=with_colors,
        )
    ).is_none()


@pytest.mark.parametrize(
    "exodict_type, test_path",
    [
        param(ExodictType.SCIENTIFIC, TEST_PATH_SCIENTIFIC, id="scientific exodam"),
        param(ExodictType.EDITORIAL, TEST_PATH_EDITORIAL, id="editorial exodam"),
    ],
)
def test_finalize_for_api_failed(exodict_type, test_path):
    assert_that(
        finalize_for_api(
            load_yaml(test_path / "none_valid.yaml"),
            exodict_type,
            warning_messages=[],
        )
    ).is_none()


@pytest.mark.parametrize(
    "exodict_type, test_path",
    [
        param(ExodictType.SCIENTIFIC, TEST_PATH_SCIENTIFIC, id="scientific exodam"),
        param(ExodictType.EDITORIAL, TEST_PATH_EDITORIAL, id="editorial exodam"),
    ],
)
def test_finalize_for_cli_export_failed(tmp_path, exodict_type, test_path):
    export_path_folder = tmp_path / "test_finalize"
    export_path_folder.mkdir()
    export_path = export_path_folder / "test_finalize_fail.exodam.json"
    assert_that(
        finalize_for_cli(
            test_path / "none_valid.yaml",
            exodict_type,
            export_path,
            warning_messages=[],
        )
    ).is_false()
