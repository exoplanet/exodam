# Pytest imports
import pytest
from pytest import param

# Standard imports
import random
from pathlib import Path

# Third party imports
from assertpy import assert_that

# First party imports
from exodam import ExodamContent
from exodam_utils import load_yaml

TEST_FILES_PATH = Path("tests/test_files/check")
ALL_GOOD_TEST_FILE_PATH = TEST_FILES_PATH / "all_good.yaml"
TEST_FILES = [
    TEST_FILES_PATH / "all_good.yaml",
    TEST_FILES_PATH / "duplicate_name_in_alt_names.yaml",
    TEST_FILES_PATH / "duplicate_name.yaml",
    TEST_FILES_PATH / "empty_gs.yaml",
    TEST_FILES_PATH / "grav_sys_wrong_regex.yaml",
    TEST_FILES_PATH / "identity_alternate_names_lst_of_not_str.yaml",
    TEST_FILES_PATH / "identity_alternate_names_not_lst.yaml",
    TEST_FILES_PATH / "identity_doi_not_str.yaml",
    TEST_FILES_PATH / "identity_exotype_regex_error.yaml",
    TEST_FILES_PATH / "identity_no_doi.yaml",
    TEST_FILES_PATH / "internal_display_status_not_bool.yaml",
    TEST_FILES_PATH / "internal_missing_opt_key.yaml",
    TEST_FILES_PATH / "internal_object_status_regex_error.yaml",
    TEST_FILES_PATH / "invalid_in_multi_gs.yaml",
    TEST_FILES_PATH / "invalid_name_of_all_obj_in_multi.yaml",
    TEST_FILES_PATH / "invalid_name_of_one_obj_in_multi.yaml",
    TEST_FILES_PATH / "invalid_name_one_obj.yaml",
    TEST_FILES_PATH / "missing_identity.yaml",
    TEST_FILES_PATH / "missing_key.yaml",
    TEST_FILES_PATH / "missing_objects_identity.yaml",
    TEST_FILES_PATH / "missing_objects_internal.yaml",
    TEST_FILES_PATH / "missing_objects_parameters.yaml",
    TEST_FILES_PATH / "missing_objects_references.yaml",
    TEST_FILES_PATH / "missing_units.yaml",
    TEST_FILES_PATH / "multi_gs_valid.yaml",
    TEST_FILES_PATH / "multi_objects.yaml",
    TEST_FILES_PATH / "objects_type_lst_not_str.yaml",
    TEST_FILES_PATH / "objects_type_not_lst.yaml",
    TEST_FILES_PATH / "objects_type_regex_error.yaml",
    TEST_FILES_PATH / "parameters_detection_method_regex_error.yaml",
    TEST_FILES_PATH / "parameters_discovery_method_regex_error.yaml",
    TEST_FILES_PATH / "parameters_mass_meas_method_regex_error.yaml",
    TEST_FILES_PATH / "parameters_molecule_source_regex_error.yaml",
    TEST_FILES_PATH / "parameters_not_float_in_vupnu.yaml",
    TEST_FILES_PATH / "parameters_one_in_all_sub_categories.yaml",
    TEST_FILES_PATH / "parameters_rad_meas_method_regex_error.yaml",
    TEST_FILES_PATH / "parameters_temp_source_regex_error.yaml",
    TEST_FILES_PATH / "parameters_temp_type_regex_error.yaml",
    TEST_FILES_PATH / "permissive.yaml",
    TEST_FILES_PATH / "references_missing_opt_key_in_pub.yaml",
    TEST_FILES_PATH / "references_missing_opt_key.yaml",
    TEST_FILES_PATH / "references_not_publication.yaml",
    TEST_FILES_PATH / "references_only_doi.yaml",
    TEST_FILES_PATH / "references_only_url.yaml",
    TEST_FILES_PATH / "references_publication_doi_and_url_are_null.yaml",
    TEST_FILES_PATH / "references_publication_is_null.yaml",
    TEST_FILES_PATH / "references_publication_not_dict.yaml",
    TEST_FILES_PATH / "references_publication_status_regex_error.yaml",
    TEST_FILES_PATH / "references_publication_type_regex_error.yaml",
    TEST_FILES_PATH / "relationship_null.yaml",
    TEST_FILES_PATH / "unknown_key.yaml",
    TEST_FILES_PATH / "valid_without_doi.yml",
    TEST_FILES_PATH / "wrong_type_first_level.yaml",
]


def _shuffle_exodam(to_shuffle):
    match to_shuffle:
        case dict():
            keys = list(to_shuffle.keys())
            random.shuffle(keys)
            return {key: _shuffle_exodam(to_shuffle[key]) for key in keys}
        case list():
            res = []
            random.shuffle(to_shuffle)
            for elem in to_shuffle:
                if isinstance(elem, (dict, list)):
                    res.append(_shuffle_exodam(elem))
                else:
                    res.append(elem)
            return res
        case _:
            return to_shuffle


@pytest.mark.parametrize(
    "raw_dict, expected",
    [
        param({}, {}, id="empty"),
        param({"b": 2, "a": 1}, {"a": 1, "b": 2}, id="simple shuffled dict"),
        param({"a": 1, "b": 2}, {"a": 1, "b": 2}, id="simple sorted dict"),
        param(
            {"d": 3, "a": {"c": 2, "b": 1}},
            {"a": {"b": 1, "c": 2}, "d": 3},
            id="shuffled dict of dict",
        ),
        param(
            {"a": {"b": 1, "c": 2}, "d": 3},
            {"a": {"b": 1, "c": 2}, "d": 3},
            id="sorted dict of dict",
        ),
        param(
            {"b": 4, "a": [3, 1, 2]},
            {"a": [1, 2, 3], "b": 4},
            id="shuffled dict of list",
        ),
        param(
            {"a": [1, 2, 3], "b": 4},
            {"a": [1, 2, 3], "b": 4},
            id="sorted dict of list",
        ),
        param(
            {"b": 4, "a": [{"ab": 3, "aa": 1}, {"ab": 2, "aa": 1}]},
            {"a": [{"aa": 1, "ab": 2}, {"aa": 1, "ab": 3}], "b": 4},
            id="shuffled dict of list of dict",
        ),
        param(
            {"a": [{"aa": 1, "ab": 2}, {"aa": 1, "ab": 3}], "b": 4},
            {"a": [{"aa": 1, "ab": 2}, {"aa": 1, "ab": 3}], "b": 4},
            id="sorted dict of list of dict ",
        ),
        param(
            {"b": 5, "a": [{"ab": [4, 3], "aa": 1}, {"ab": [3, 2], "aa": 1}]},
            {"a": [{"aa": 1, "ab": [2, 3]}, {"aa": 1, "ab": [3, 4]}], "b": 5},
            id="shuffled dict of list of dict of list",
        ),
        param(
            {"a": [{"aa": 1, "ab": [2, 3]}, {"aa": 1, "ab": [3, 4]}], "b": 5},
            {"a": [{"aa": 1, "ab": [2, 3]}, {"aa": 1, "ab": [3, 4]}], "b": 5},
            id="sorted dict of list of dict of list",
        ),
    ],
)
def test_exodam_content_init(raw_dict, expected):
    assert_that(ExodamContent(raw_dict)).is_equal_to(expected)


@pytest.mark.parametrize(
    "content_one",
    [_shuffle_exodam(load_yaml((ALL_GOOD_TEST_FILE_PATH))) for _ in range(5)],
)
@pytest.mark.parametrize(
    "content_two",
    [_shuffle_exodam(load_yaml((ALL_GOOD_TEST_FILE_PATH))) for _ in range(5)],
)
def test_exodam_content_eq_fast(content_one, content_two):
    assert_that(ExodamContent(content_one)).is_equal_to(ExodamContent(content_two))


@pytest.mark.parametrize(
    "content_one",
    [_shuffle_exodam(load_yaml((ALL_GOOD_TEST_FILE_PATH))) for _ in range(50)],
)
@pytest.mark.parametrize(
    "content_two",
    [_shuffle_exodam(load_yaml((ALL_GOOD_TEST_FILE_PATH))) for _ in range(50)],
)
@pytest.mark.slow
def test_exodam_content_eq_slow(content_one, content_two):
    test_exodam_content_eq_fast(content_one, content_two)


@pytest.mark.parametrize(
    "content_one",
    [load_yaml(content) for content in TEST_FILES[0:5]],
)
@pytest.mark.parametrize(
    "content_two",
    [load_yaml(content) for content in TEST_FILES[6:10]],
)
def test_exodam_content_neq_fast(content_one, content_two):
    assert_that(ExodamContent(content_one)).is_not_equal_to(ExodamContent(content_two))


@pytest.mark.parametrize(
    "content_one",
    [load_yaml(content) for content in TEST_FILES[0:25]],
)
@pytest.mark.parametrize(
    "content_two",
    [load_yaml(content) for content in TEST_FILES[26:53]],
)
@pytest.mark.slow
def test_exodam_content_neq_slow(content_one, content_two):
    test_exodam_content_neq_fast(content_one, content_two)
