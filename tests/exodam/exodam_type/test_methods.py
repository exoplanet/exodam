# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from exodam import ExodamContent
from exodam_utils import BASE_DIR, load_yaml


@pytest.mark.parametrize(
    "from_dict, expected",
    [
        param({"a": 1, "b": 2, "c": 3}, ["a", "b", "c"], id="simple dict"),
        param({"a": 1, "b": {"c": 2}}, ["a", "b", "c"], id="nested dict"),
        param(
            {"a": 1, "b": [2, 3, 4], "c": "5"}, ["a", "b", "c"], id="simple with list"
        ),
        param({"a": 1, "b": {"c": [2, 3, 4]}}, ["a", "b", "c"], id="nested with list"),
        param(
            {"a": 1, "b": [{"c": 2, "d": 3}, {"e": 4, "f": 5}]},
            ["a", "b", "c", "d", "e", "f"],
            id="with list of dict",
        ),
        param({"a": 1, "b": (2, 3, 4), "c": "5"}, ["a", "b", "c"], id="with tuple"),
        param(
            {"a": 1, "b": ({"c": 2, "d": 3}, {"e": 4, "f": 5})},
            ["a", "b", "c", "d", "e", "f"],
            id="with tuple of dict]",
        ),
        param(
            {"a": 1, "b": [2, [3], 4], "c": "5"},
            ["a", "b", "c"],
            id="with list of list",
        ),
        param(
            {"a": 1, "b": [[{"c": 2}]]}, ["a", "b", "c"], id="with list of list of dict"
        ),
        param(
            {"a": 1, "b": (2, (3, 4)), "c": "5"},
            ["a", "b", "c"],
            id="with tuple of tuple",
        ),
        param(
            {"a": 1, "b": {"c": 2}}, ["a", "b", "c"], id="with tuple of tuple of dict"
        ),
        param(
            {"a": 1, "b": (2, [3], 4), "c": "5"},
            ["a", "b", "c"],
            id="with mix of list and tuple 1",
        ),
        param(
            {"a": 1, "b": [{"c": 2}]}, ["a", "b", "c"], id="with list of dict of tuple"
        ),
    ],
)
def test_get_all_key(from_dict, expected):
    assert_that(ExodamContent(from_dict).get_all_keys()).is_equal_to(expected)


def test_get_all_key_real_file(expected_all_good_keys):
    from_dict = load_yaml(BASE_DIR / "tests/test_files/check/all_good.yaml")
    assert_that(ExodamContent(from_dict).get_all_keys()).contains_only(
        *expected_all_good_keys
    )
