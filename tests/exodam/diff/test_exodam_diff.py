# Pytest imports
import pytest
from pytest import param

# Standard imports
from pathlib import Path

# Third party imports
from assertpy import assert_that

# First party imports
from exodam import ExodamContent, diff
from exodam_utils import BASE_DIR, load_yaml

TEST_PATH = BASE_DIR / "tests/test_files/diff/"


@pytest.mark.parametrize(
    "file_one, file_two",
    [
        param("all_good.yaml", "all_good.yaml", id="same file - all good"),
        param(
            "all_good.yaml",
            "all_good_change_order.yaml",
            id="same but change order of key - all good",
        ),
        param(
            "multi_objects.yaml", "multi_objects.yaml", id="same file - multi objects"
        ),
        param(
            "multi_objects.yaml",
            "multi_objects_change_order.yaml",
            id="same but change order of key - multi objects",
        ),
    ],
)
def test_diff_no_diffrence(file_one, file_two):
    content_one = ExodamContent(load_yaml(TEST_PATH / file_one))
    content_two = ExodamContent(load_yaml(TEST_PATH / file_two))
    assert_that(diff(content_one, content_two)).is_equal_to("")


@pytest.mark.parametrize(
    "file_one, file_two",
    [
        param(
            "all_good.yaml",
            "all_good_little_different.yaml",
            id="same file with little difference - all good",
        ),
        param(
            "multi_objects.yaml",
            "multi_objects_little_different.yaml",
            id="same file with little difference - multi objects",
        ),
        param("all_good.yaml", "multi_objects.yaml", id="different files"),
    ],
)
def test_diff_with_diffrence(file_one, file_two):
    content_one = ExodamContent(load_yaml(TEST_PATH / file_one))
    content_two = ExodamContent(load_yaml(TEST_PATH / file_two))
    assert_that(diff(content_one, content_two)).is_not_equal_to("")
