# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that
from dotmap import DotMap

# First party imports
from exodam.utils import get_colors


@pytest.mark.parametrize(
    "with_colors, expected",
    [
        param(
            True,
            DotMap(
                {
                    "green": {"on": "[bold green]", "off": "[/bold green]"},
                    "red": {"on": "[bold red]", "off": "[/bold red]"},
                    "white": {"on": "[bold white]", "off": "[/bold white]"},
                    "white_on_purple": {
                        "on": "[bold white on #c074c0]",
                        "off": "[/bold white on #c074c0]",
                    },
                    "white_on_yellow": {
                        "on": "[bold white on yellow]",
                        "off": "[/bold white on yellow]",
                    },
                    "white_on_orange": {
                        "on": "[bold white on orange3]",
                        "off": "[/bold white on orange3]",
                    },
                    "underline": {
                        "on": "[bold underline]",
                        "off": "[/bold underline]",
                    },
                }
            ),
            id="with colors",
        ),
        param(
            False,
            DotMap(
                {
                    "green": {"on": "", "off": ""},
                    "red": {"on": "", "off": ""},
                    "white": {"on": "", "off": ""},
                    "white_on_purple": {"on": "", "off": ""},
                    "white_on_yellow": {"on": "", "off": ""},
                    "white_on_orange": {"on": "", "off": ""},
                    "underline": {"on": "", "off": ""},
                }
            ),
            id="without colors",
        ),
    ],
)
def test_get_colors(with_colors, expected):
    assert_that(get_colors(with_colors)).is_equal_to(expected)
