# Pytest imports
import pytest

# Third party imports
from assertpy import assert_that

# First party imports
from exodam import generate_default_csv_file, generate_default_yaml_file
from exodam_utils import BASE_DIR, load_yaml

EXPECTED_YAML = load_yaml(BASE_DIR / "tests/test_files/generate/yaml_skel.yaml")


def test_yaml_skel(tmp_path, db_connection_with_data):
    tmp_file_path = tmp_path / "test_yaml_skel.yaml"
    generate_default_yaml_file(tmp_file_path, db_connection_with_data)

    result = load_yaml(tmp_file_path)

    assert_that(result).is_equal_to(EXPECTED_YAML)


def test_csv_skel():
    with pytest.raises(NotImplementedError):
        generate_default_csv_file()
