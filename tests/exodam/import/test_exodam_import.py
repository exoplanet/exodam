# Pytest imports
import pytest

# Standard imports
from typing import List

# Third party imports
from assertpy import assert_that, soft_assertions
from flatten_dict import flatten
from flatten_dict.reducers import make_reducer

# First party imports
from exodam import (
    GeneralNotFoundError,
    NoObjectsError,
    csv_to_yaml,
    dict_to_yaml,
    get_general_and_objects,
    get_gs,
    get_system_in_dict,
    read_csv,
)
from exodam_utils import BASE_DIR

TEST_PATH = BASE_DIR / "tests/test_files/import/"


@pytest.mark.skip(reason="revive whem import will be revive")
def test_import_read_csv_successed():
    assert_that(read_csv(f"{TEST_PATH}/valid_general.csv")).is_equal_to(
        [["DOI"], ["blabla12345blabla"]]
    )


@pytest.mark.skip(reason="revive whem import will be revive")
def test_import_read_csv_filenotfound():
    assert_that(read_csv(f"{TEST_PATH}gs.csv")).is_none()


@pytest.mark.skip(reason="revive whem import will be revive")
@pytest.mark.parametrize(
    "input_file, expected",
    [
        ("valid_general.csv", [{"DOI": "blabla12345blabla"}]),
        ("valid_one_object.csv", [{"name": "toto", "exo_type": "star"}]),
        (
            "valid_multi_object.csv",
            [
                {"name": "toto", "exo_type": "star"},
                {"name": "titi", "exo_type": "star"},
            ],
        ),
    ],
)
def test_import_get_general_objects_successed(input_file: str, expected: List[dict]):
    assert_that(get_general_and_objects(f"{TEST_PATH}{input_file}")).is_equal_to(
        expected
    )


@pytest.mark.skip(reason="revive whem import will be revive")
def test_import_get_general_objects_filenotfound():
    assert_that(get_general_and_objects(f"{TEST_PATH}star.csv")).is_none()


@pytest.mark.skip(reason="revive whem import will be revive")
@pytest.mark.parametrize(
    "input_file, expected",
    [
        ("valid_one_gs.csv", [{"sg1": ["beta pic", "beta pic toto", "beta pic ter"]}]),
        (
            "valid_multi_gs.csv",
            [
                {"sg1": ["beta pic", "beta pic toto", "beta pic ter"]},
                {"sg2": ["beta pic", "beta pic toto", "beta pic ter"]},
            ],
        ),
    ],
)
def test_import_get_gs_successed(input_file: str, expected: List[dict]):
    assert_that(get_gs(f"{TEST_PATH}{input_file}")).is_equal_to(expected)


@pytest.mark.skip(reason="revive whem import will be revive")
def test_import_get_gs_filenotfound():
    assert_that(get_gs(f"{TEST_PATH}gs.csv")).is_none()


@pytest.mark.skip(reason="revive whem import will be revive")
def test_import_get_system_one_type_successed():
    result = get_system_in_dict(f"{TEST_PATH}oneTypeSystem")
    with soft_assertions():
        assert_that(result).contains_key("DOI", "objects", "gs")

        # DOI
        assert_that(result["DOI"]).is_equal_to("blabla12345blabla")

        # Objects
        flatten_object = flatten(
            result["objects"][0], reducer=make_reducer(delimiter="__")
        )
        assert_that(flatten_object).contains_entry(
            {"name": "beta pic"},
            {"exo_type": "star"},
            {"position__distance__value": 19.3},
            {"position__distance__uncert": 0.2},
            {"position__distance__unit": "pc"},
            {"position__ra__value": 20837.0},
            {"position__dec__value": -183839.0},
            {"physical__age__value": 0.4},
            {"general__status": "Candidate"},
            {"general__discovered_date": "2011 11 04"},
            {"general__publication_status": "W"},
        )

        # Gs
        assert_that(result["gs"]).is_equal_to(
            [
                {"sg1": ["beta pic", "beta pic toto", "beta pic ter"]},
                {"sg2": ["beta pic", "beta pic toto", "beta pic ter"]},
            ]
        )


@pytest.mark.skip(reason="revive whem import will be revive")
def test_import_get_system_multi_type_successed():
    result = get_system_in_dict(f"{TEST_PATH}multiTypeSystem")
    with soft_assertions():
        assert_that(result).contains_key("DOI", "objects", "gs")

        # DOI
        assert_that(result["DOI"]).is_equal_to("blabla12345blabla")

        # Objects
        flatten_object = flatten(
            result["objects"][0], reducer=make_reducer(delimiter="__")
        )
        assert_that(flatten_object).contains_entry(
            {"name": "beta pic"},
            {"exo_type": "star"},
            {"position__distance__value": 19.3},
            {"position__distance__uncert": 0.2},
            {"position__distance__unit": "pc"},
            {"position__ra__value": 20837.0},
            {"position__dec__value": -183839.0},
            {"physical__age__value": 0.4},
            {"general__status": "Candidate"},
            {"general__discovered_date": "2011 11 04"},
            {"general__publication_status": "W"},
        )

        flatten_object = flatten(
            result["objects"][1], reducer=make_reducer(delimiter="__")
        )
        assert_that(flatten_object).contains_entry(
            {"name": "beta pic b"},
            {"exo_type": "planet"},
            {"general__status": "Candidate"},
            {"general__discovered_date": "2011 11 04"},
            {"general__publication_status": "W"},
        )

        # Gs
        assert_that(result["gs"]).is_equal_to(
            [
                {"sg1": ["beta pic", "beta pic toto", "beta pic ter"]},
                {"sg2": ["beta pic", "beta pic toto", "beta pic ter"]},
            ]
        )


@pytest.mark.skip(reason="revive whem import will be revive")
@pytest.mark.parametrize(
    "input_dir, exception",
    [
        ("generalNotFound", GeneralNotFoundError),
        ("noObjects", NoObjectsError),
    ],
)
def test_import_get_file_not_found(input_dir, exception):
    with pytest.raises(exception):
        get_system_in_dict(f"{TEST_PATH}{input_dir}")


@pytest.mark.skip(reason="revive whem import will be revive")
def test_import_dict_to_yaml(tmp_path):
    dict_to_write = get_system_in_dict(f"{TEST_PATH}multiTypeSystem")
    try:
        dict_to_yaml(dict_to_write, file_name=f"{tmp_path}/import_yaml")
    except (ValueError, FileNotFoundError) as e:
        pytest.fail(f"Error : {e}")


@pytest.mark.skip(reason="revive whem import will be revive")
def test_import_csv_to_yaml(tmp_path):
    try:
        csv_to_yaml(f"{TEST_PATH}multiTypeSystem", file_name=f"{tmp_path}/test_import")
    except (ValueError, FileNotFoundError) as e:
        pytest.fail(f"Error : {e}")
