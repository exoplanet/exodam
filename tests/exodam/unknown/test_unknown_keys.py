# Pytest imports
import pytest
from pytest import param

# Standard imports
import re

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exodam import ExodamContent, check_and_rename_unknowns
from exodam.check.detect_unknown import UNKNOWN_VALUES_PREFIX
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType

TEST_PATH = BASE_DIR / "tests/test_files/unknown"
TEST_PATH_SCIENTIFIC = TEST_PATH / "scientific"
TEST_PATH_EDITORIAL = TEST_PATH / "editorial"

# Regxe to match all keys prefixed by the unknown prefix
REGEX_UNKNOWN = rf"^{UNKNOWN_VALUES_PREFIX}.*$"


@pytest.mark.parametrize(
    "input_file, exodict_type, expected_w_msg, expected_unknown_keys",
    [
        # Scientific
        param(
            TEST_PATH_SCIENTIFIC / "valid.yaml",
            ExodictType.SCIENTIFIC,
            "",
            [],
            id="No unknown",
        ),
        param(
            TEST_PATH_SCIENTIFIC / "one.yaml",
            ExodictType.SCIENTIFIC,
            "unknown keys detected",
            ["Ichthyomorphosis"],
            id="one easy unknown",
        ),
        param(
            TEST_PATH_SCIENTIFIC / "many.yaml",
            ExodictType.SCIENTIFIC,
            "unknown keys detected",
            ["Equilibrium", "Tundra", "Darba", "Shatterstorm"],
            id="many unknown",
        ),
        param(
            TEST_PATH_SCIENTIFIC / "in_multi_obj.yaml",
            ExodictType.SCIENTIFIC,
            "unknown keys detected",
            ["Inspirit", "Despondency"],
            id="unknown in multi objects",
        ),
        # Editorial
        param(
            TEST_PATH_EDITORIAL / "valid.yaml",
            ExodictType.EDITORIAL,
            "",
            [],
            id="No unknown",
        ),
        param(
            TEST_PATH_EDITORIAL / "one.yaml",
            ExodictType.EDITORIAL,
            "unknown keys detected",
            ["Ichthyomorphosis"],
            id="one easy unknown",
        ),
        param(
            TEST_PATH_EDITORIAL / "many.yaml",
            ExodictType.EDITORIAL,
            "unknown keys detected",
            ["Equilibrium", "Tundra", "Darba", "Shatterstorm"],
            id="many unknown",
        ),
        param(
            TEST_PATH_EDITORIAL / "in_multi_obj.yaml",
            ExodictType.EDITORIAL,
            "unknown keys detected",
            ["Inspirit", "Despondency"],
            id="unknown in multi objects",
        ),
    ],
)
def test_check_and_rename_unknowns(
    input_file,
    exodict_type,
    expected_w_msg,
    expected_unknown_keys,
):
    from_dict = load_yaml(input_file)
    w_msg = []
    res = check_and_rename_unknowns(
        ExodamContent(from_dict),
        exodict_type,
        w_msg,
    )

    check = [key for key in res.get_all_keys() if re.match(REGEX_UNKNOWN, key)]
    expected_check = [f"{UNKNOWN_VALUES_PREFIX}{key}" for key in expected_unknown_keys]

    with soft_assertions():
        assert_that("".join(w_msg)).contains(expected_w_msg)
        assert_that(sorted(check)).is_equal_to(sorted(expected_check))
