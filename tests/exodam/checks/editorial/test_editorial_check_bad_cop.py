# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exodam import ExodamContent, dog_house
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType

TEST_PATH_VALID = BASE_DIR / "tests/test_files/check/editorial/valid"
TEST_PATH_NOT_VALID = BASE_DIR / "tests/test_files/check/editorial/not_valid"


@pytest.mark.parametrize(
    "file",
    [
        param("all_good.yml", id="all good"),
        param("missing_opt_image.yml", id="missing optional image"),
        param("missing_opt_links.yml", id="missing optional links"),
        param("multi_news.yml", id="multi news"),
    ],
)
@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_bad_cop_succeeds(file, with_colors):
    exodam_content = ExodamContent(load_yaml(TEST_PATH_VALID / file))
    w_msg = []
    result = dog_house(
        exodam_content,
        ExodictType.EDITORIAL,
        False,
        w_msg,
        color=with_colors,
    )

    with soft_assertions():
        assert_that(result).is_true()
        assert_that("".join(w_msg)).contains("Exodam news syntax is good")


@pytest.mark.parametrize(
    "file",
    [
        param("body_wrong_type.yml", id="body wrong type"),
        param("missing_title.yml", id="missing mandatory title"),
        param("news_type_wrong_regex.yml", id="news_type wrong regex"),
        param("links_wrong_type.yml", id="links wrong type"),
        param("missing_web_status.yml", id="missing mandatory web status"),
        param("web_status_wrong_regex.yml", id="web status wrong regex"),
    ],
)
@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_bad_cop_objects_fail(file, with_colors):
    exodam_content = ExodamContent(load_yaml(TEST_PATH_NOT_VALID / file))
    w_msg = []
    result = dog_house(
        exodam_content,
        ExodictType.EDITORIAL,
        False,
        w_msg,
        color=with_colors,
    )

    with soft_assertions():
        assert_that(result).is_false()
        assert_that("".join(w_msg)).contains("Exodam news syntax isn't good")
