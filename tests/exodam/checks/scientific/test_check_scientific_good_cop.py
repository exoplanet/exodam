# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exodam import ExodamContent, dog_house
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType

TEST_PATH = BASE_DIR / "tests/test_files/check/"


@pytest.mark.parametrize(
    "file",
    [
        param("all_good.yaml", id="all good"),
        param("relationship_null.yaml", id="relationship is null"),
        param("missing_units.yaml", id="missing units"),
        param("missing_objects_internal.yaml", id="missing objects -> internal"),
        param("internal_missing_opt_key.yaml", id="internal missing optional key"),
        param(
            "references_missing_opt_key_in_pub.yaml",
            id="references missing optional key in pub",
        ),
        param("references_missing_opt_key.yaml", id="references missing optional key"),
        param("references_only_doi.yaml", id="references only doi"),
        param("references_only_url.yaml", id="references only url"),
        param(
            "parameters_one_in_all_sub_categories.yaml",
            id="parameters with one param in all categories",
        ),
        param("multi_objects.yaml", id="multi objects"),
        param("multi_gs_valid.yaml", id="multi grav sys"),
        param("empty_gs.yaml", id="empty grav sys"),
        param("permissive.yaml", id="permissive exodam"),
        param("unknown_key.yaml", id="unknown key"),
    ],
)
@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_good_cop_succeeds(file, with_colors):
    exodam_content = ExodamContent(load_yaml(TEST_PATH / file))
    w_msg = []
    result = dog_house(
        exodam_content,
        ExodictType.SCIENTIFIC,
        True,
        w_msg,
        color=with_colors,
    )

    with soft_assertions():
        assert_that(result).is_true()
        assert_that("".join(w_msg)).contains("Permissive: Exodam syntax is good")


@pytest.mark.parametrize(
    "file",
    [
        param(
            "objects_type_regex_error.yaml",
            id="permissive: wrong regex for object type",
        ),
        param(
            "identity_alternate_names_lst_of_not_str.yaml",
            id="permissive: identity: alternate names list of not str",
        ),
        param(
            "identity_alternate_names_not_lst.yaml",
            id="permissive: identity: alternate names not list",
        ),
        param("identity_doi_not_str.yaml", id="permissive: identity: doi not str"),
        param("identity_no_doi.yaml", id="permissive: identity: no doi"),
        param(
            "identity_exotype_regex_error.yaml",
            id="permissive: identity: wrong regex for exotype",
        ),
        param("grav_sys_wrong_regex.yaml", id="permissive:"),
        param("objects_type_not_lst.yaml", id="permissive: objects type: not list"),
        param(
            "objects_type_lst_not_str.yaml",
            id="permissive: objects type list of not str",
        ),
    ],
)
@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_good_cop_failed(file, with_colors):
    exodam_content = ExodamContent(load_yaml(TEST_PATH / file))
    w_msg = []
    result = dog_house(
        exodam_content,
        ExodictType.SCIENTIFIC,
        True,
        w_msg,
        color=with_colors,
    )

    with soft_assertions():
        assert_that(result).is_false()
        assert_that("".join(w_msg)).contains("Permissive: Exodam syntax isn't good")


@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_good_cop_without_doi_succeesed(with_colors):
    exodam_content = ExodamContent(load_yaml(TEST_PATH / "valid_without_doi.yml"))
    w_msg = []
    result = dog_house(
        exodam_content,
        ExodictType.SCIENTIFIC,
        True,
        w_msg,
        False,
        color=with_colors,
    )

    with soft_assertions():
        assert_that(result).is_true()
        assert_that("".join(w_msg)).contains("Permissive: Exodam syntax is good")
