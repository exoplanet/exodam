# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that, soft_assertions

# First party imports
from exodam import ExodamContent, dog_house
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType

TEST_PATH = BASE_DIR / "tests/test_files/check/"


@pytest.mark.parametrize(
    "file",
    [
        param("all_good.yaml", id="all good"),
        param("relationship_null.yaml", id="relationship is null"),
        param("missing_units.yaml", id="missing units"),
        param("missing_objects_internal.yaml", id="missing objects -> internal"),
        param("internal_missing_opt_key.yaml", id="internal missing optional key"),
        param(
            "references_missing_opt_key_in_pub.yaml",
            id="references missing optional key in pub",
        ),
        param("references_missing_opt_key.yaml", id="references missing optional key"),
        param("references_only_doi.yaml", id="references only doi"),
        param("references_only_url.yaml", id="references only url"),
        param(
            "parameters_one_in_all_sub_categories.yaml",
            id="parameters with one param in all categories",
        ),
        param("multi_objects.yaml", id="multi objects"),
        param("multi_gs_valid.yaml", id="multi grav sys"),
        param("empty_gs.yaml", id="empty grav sys"),
    ],
)
@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_bad_cop_succeeds(file, with_colors):
    exodam_content = ExodamContent(load_yaml(TEST_PATH / file))
    w_msg = []
    result = dog_house(
        exodam_content,
        ExodictType.SCIENTIFIC,
        False,
        w_msg,
        color=with_colors,
    )

    with soft_assertions():
        assert_that(result).is_true()
        assert_that("".join(w_msg)).contains(
            "Exodam struct and global information syntax is good",
            "Exodam objects syntax is good",
            "Exodam syntax is good",
        )


@pytest.mark.parametrize(
    "file",
    [
        param("unknown_key.yaml", id="unknown key"),
        param("missing_key.yaml", id="missing key"),
        param("wrong_type_first_level.yaml", id="wrong type of a primary key"),
        param("missing_objects_references.yaml", id="missing objects -> references"),
        param("missing_objects_identity.yaml", id="missing objects -> identity"),
        param("missing_objects_parameters.yaml", id="missing objects -> parameters"),
        param("objects_type_regex_error.yaml", id="regex error on objects type"),
        param("grav_sys_wrong_regex.yaml", id="regex error on gravitational system"),
        param("objects_type_not_lst.yaml", id="objects type not list"),
        param("objects_type_lst_not_str.yaml", id="objects type list of not str"),
    ],
)
@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_bad_cop_general_info_fail(file, with_colors):
    exodam_content = ExodamContent(load_yaml(TEST_PATH / file))
    w_msg = []
    result = dog_house(
        exodam_content,
        ExodictType.SCIENTIFIC,
        False,
        w_msg,
        color=with_colors,
    )

    with soft_assertions():
        assert_that(result).is_false()
        assert_that("".join(w_msg)).contains(
            "Exodam struct and global information syntax isn't good"
        )


@pytest.mark.parametrize(
    "file",
    [
        param("identity_no_doi.yaml", id="identity missing doi"),
        param("identity_doi_not_str.yaml", id="identity doi not str"),
        param(
            "identity_alternate_names_not_lst.yaml",
            id="identity alternate names not a list",
        ),
        param(
            "identity_alternate_names_lst_of_not_str.yaml",
            id="identity alternate names list of not string",
        ),
        param(
            "identity_exotype_regex_error.yaml", id="identity wrong regex for exotype"
        ),
        param(
            "internal_object_status_regex_error.yaml",
            id="internal wrong regex for object status",
        ),
        param(
            "internal_display_status_not_bool.yaml",
            id="internal display status not boolean",
        ),
        param("references_not_publication.yaml", id="references no publication"),
        param(
            "references_publication_is_null.yaml", id="references publication is null"
        ),
        param(
            "references_publication_not_dict.yaml",
            id="references publication is not a dict",
        ),
        param(
            "references_publication_type_regex_error.yaml",
            id="references wrong regex for publication type",
        ),
        param(
            "references_publication_status_regex_error.yaml",
            id="references wrong regex for publication status",
        ),
        param(
            "parameters_detection_method_regex_error.yaml",
            id="parameters wrong regex for detection method",
        ),
        param(
            "parameters_discovery_method_regex_error.yaml",
            id="parameters wrong regex for discovery method",
        ),
        param(
            "parameters_mass_meas_method_regex_error.yaml",
            id="parameters wrong regex for mass meas method",
        ),
        param(
            "parameters_rad_meas_method_regex_error.yaml",
            id="parameters wrong regex for rad meas method",
        ),
        param("parameters_not_float_in_vupnu.yaml", id="parameters not float in vupnu"),
        param(
            "parameters_temp_type_regex_error.yaml",
            id="parameters wrong regex for temp type",
        ),
        param(
            "parameters_temp_source_regex_error.yaml",
            id="parameters wrong regex for temp source",
        ),
        param(
            "parameters_molecule_source_regex_error.yaml",
            id="parameters wrong regex for molecule source",
        ),
    ],
)
@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_bad_cop_objects_fail(file, with_colors):
    exodam_content = ExodamContent(load_yaml(TEST_PATH / file))
    w_msg = []
    result = dog_house(
        exodam_content,
        ExodictType.SCIENTIFIC,
        False,
        w_msg,
        color=with_colors,
    )

    with soft_assertions():
        assert_that(result).is_false()
        assert_that("".join(w_msg)).contains("Exodam objects syntax isn't good")


@pytest.mark.parametrize(
    "file, expected_w_msg",
    [
        param(
            "references_publication_doi_and_url_are_null.yaml",
            "One of the parameters `url` and `doi`",
            id="references publication doi and url are null",
        ),
        param("duplicate_name.yaml", "Duplicate objects names:", id="duplicate name"),
        param(
            "duplicate_name_in_alt_names.yaml",
            "Duplicate objects names:",
            id="duplicate name in alternate names",
        ),
        param(
            "invalid_name_of_all_obj_in_multi.yaml",
            "Invalid name(s) in gravitational systems",
            id="grav sys: invalid name of all obj in multi obj",
        ),
        param(
            "invalid_name_of_one_obj_in_multi.yaml",
            "Invalid name(s) in gravitational systems",
            id="grav sys: invalid name of one obj in multi obj",
        ),
        param(
            "invalid_name_one_obj.yaml",
            "Invalid name(s) in gravitational systems",
            id="grav sys: invalid name of one obj in one obj",
        ),
        param(
            "invalid_in_multi_gs.yaml",
            "Invalid name(s) in gravitational systems",
            id="grav sys: Invalid name in multi gs",
        ),
    ],
)
@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_bad_cop_post_validation_fail(file, expected_w_msg, with_colors):
    exodam_content = ExodamContent(load_yaml(TEST_PATH / file))
    w_msg = []
    result = dog_house(
        exodam_content,
        ExodictType.SCIENTIFIC,
        False,
        w_msg,
        color=with_colors,
    )

    with soft_assertions():
        assert_that(result).is_false()
        assert_that("".join(w_msg)).contains(expected_w_msg)
