# Pytest imports
import pytest

# Standard imports
import re

# Third party imports
from assertpy import assert_that, soft_assertions
from flatten_dict import flatten
from flatten_dict.reducers import make_reducer

# First party imports
from exodam import alter_add_implicit_value, dict_to_csv, get_objects_by_type
from exodam_resources import get_vupnu_keys
from exodam_utils import BASE_DIR, load_yaml

# Regex to match anything followed by '_value'
# (position_distance_value : match, name : don't match )
REGEX_OF_VALUE = re.compile(".*__value")

TEST_PATH = BASE_DIR / "tests/test_files/export/"


@pytest.mark.skip(reason="revive whem export will be revive")
def test_alter_implicte_value_no_change():
    vupnu_keys = get_vupnu_keys()
    dict_to_check = load_yaml(f"{TEST_PATH}alter_value_no_change.yaml")
    data = dict_to_check["objects"][0]
    data = flatten(data, reducer=make_reducer(delimiter="__"))

    alter_add_implicit_value(data, vupnu_keys)

    keys_with_value = list(filter(REGEX_OF_VALUE.match, data))

    assert_that(len(keys_with_value)).is_equal_to(0)


@pytest.mark.skip(reason="revive whem export will be revive")
def test_alter_implicte_value_multi_change():
    vupnu_keys = get_vupnu_keys()
    dict_to_check = load_yaml(f"{TEST_PATH}alter_value_multi_change.yaml")
    data = dict_to_check["objects"][0]
    data = flatten(data, reducer=make_reducer(delimiter="__"))

    alter_add_implicit_value(data, vupnu_keys)

    keys_with_value = list(filter(REGEX_OF_VALUE.match, data))

    assert_that(keys_with_value).contains_only(
        "position__distance__value",
        "position__ra__value",
        "atmospheric__molecule__molecule_result__value",
    )


@pytest.mark.skip(reason="revive whem export will be revive")
@pytest.mark.parametrize(
    "input_file, results",
    [
        (
            "get_objects_one_type.yaml",
            dict(star=2, planet=0, satellite=0, small_body=0, pulsar=0, ring=0, disk=0),
        ),
        (
            "get_objects_multi_type.yaml",
            dict(star=2, planet=1, satellite=0, small_body=1, pulsar=0, ring=0, disk=1),
        ),
    ],
)
def test_get_objects_by_type(input_file, results):
    dict_to_check = load_yaml(f"{TEST_PATH}{input_file}")
    data = dict_to_check["objects"]

    objects_sort_by_type = get_objects_by_type(data)

    with soft_assertions():
        assert_that(objects_sort_by_type["star"]).is_length(results["star"])
        assert_that(objects_sort_by_type["planet"]).is_length(results["planet"])
        assert_that(objects_sort_by_type["satellite"]).is_length(results["satellite"])
        assert_that(objects_sort_by_type["small_body"]).is_length(results["small_body"])
        assert_that(objects_sort_by_type["pulsar"]).is_length(results["pulsar"])
        assert_that(objects_sort_by_type["ring"]).is_length(results["ring"])
        assert_that(objects_sort_by_type["disk"]).is_length(results["disk"])


@pytest.mark.skip(reason="revive whem export will be revive")
@pytest.mark.parametrize(
    "input_file",
    [
        (BASE_DIR / "tests/test_files/full_planet.yaml"),
        (BASE_DIR / "tests/test_files/export/system.yaml"),
    ],
)
def test_export_csv(tmp_path, input_file):
    dict_to_write = load_yaml(input_file)
    dir_path = tmp_path / f"{input_file[17:-5]}_test"

    try:
        dict_to_csv(dict_to_write, dir_path)
    except (ValueError, FileExistsError) as e:
        pytest.fail(f"Error {e}")
