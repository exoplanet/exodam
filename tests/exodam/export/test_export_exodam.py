# Pytest imports
import pytest
from pytest import param

# Standard imports
from json import loads as json_load
from pathlib import Path

# Third party imports
from assertpy import assert_that

# First party imports
from exodam import ExodamContent, export_exodam
from exodam_utils import BASE_DIR, load_yaml

TEST_PATH = BASE_DIR / "tests/test_files/export/exodam/"

CONTENT = [
    param(
        ExodamContent(load_yaml(TEST_PATH / "all_good.yaml")),
        "all_good.exodam.json",
        id="file all_good",
    ),
    param(
        ExodamContent(load_yaml(TEST_PATH / "permissive.yaml")),
        "permissive.exodam.json",
        id="file permissive",
    ),
    param(
        ExodamContent(load_yaml(TEST_PATH / "valid_without_doi.yml")),
        "valid_without_doi.exodam.json",
        id="file valid without doi",
    ),
]


@pytest.mark.parametrize(
    "data, file_name",
    CONTENT,
)
def test_export_in_exodam(data, file_name, tmp_path):
    export_path_folder = tmp_path / "export_in_exodam"
    export_path_folder.mkdir()
    export_path = export_path_folder / file_name

    export_exodam(data, export_path)

    with open(TEST_PATH / f"check/{file_name}") as check_file:
        check_content = json_load(check_file.read())

    with open(export_path) as created_file:
        created_content = json_load(created_file.read())

    assert_that(check_content).is_equal_to(created_content)


def test_export_exodam_not_a_directory():
    export_path = Path("abc") / "toto.exodam.json"
    with pytest.raises(NotADirectoryError):
        export_exodam(CONTENT[0], export_path)


@pytest.mark.parametrize(
    "file_name",
    [
        param("toto.json", id="only .json"),
        param("toto.exodam", id="only .exodam"),
        param("toto.md", id="other suffix"),
        param("toto.html.j2", id="other suffixes"),
    ],
)
def test_export_exodam_wrong_suffix(tmp_path, file_name):
    export_path_folder = tmp_path / "export_in_exodam"
    export_path_folder.mkdir()
    export_path = export_path_folder / file_name
    with pytest.raises(ValueError):
        export_exodam(CONTENT[0], export_path)
