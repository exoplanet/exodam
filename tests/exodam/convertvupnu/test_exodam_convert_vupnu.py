# Pytest imports
import pytest
from pytest import param

# Third party imports
from assertpy import assert_that

# First party imports
from exodam import (
    get_vupnu_keys,
    transform_object_into_vupnu,
    transform_one_object_into_vupnu,
)
from exodam_resources import get_vupnu_keys as vupnu_keys_list
from exodam_utils import BASE_DIR, load_yaml
from exodict import ExodictType, get_exodict

TEST_PATH = BASE_DIR / "tests/test_files/convertvupnu/"


@pytest.mark.parametrize(
    "input_file", ["star_obj.yaml", "planet_obj.yaml", "partial_vpu.yaml"]
)
def test_transform_one_obj(input_file):
    vupnu_keys = get_vupnu_keys(vupnu_keys_list())
    data = load_yaml(TEST_PATH / input_file)["objects"][0]
    exodict = get_exodict(ExodictType.SCIENTIFIC)
    transform_one_object_into_vupnu(data, exodict, vupnu_keys, warning_messages=[])

    assert_that(data["parameters"]["position"]).contains_entry(
        {
            "distance": {
                "value": 18.2,
                "unit": {"name": "parsec", "symbol": "pc", "wiki_data_rdf": "Q12129"},
                "pos_uncert": None,
                "neg_uncert": None,
            }
        }
    )


@pytest.mark.parametrize(
    "input_file", ["star_obj.yaml", "planet_obj.yaml", "partial_vpu.yaml"]
)
@pytest.mark.parametrize(
    "with_colors",
    [
        param(True, id="option with color - True"),
        param(False, id="option with color - False"),
    ],
)
def test_transform_obj(input_file, with_colors):
    data = load_yaml(TEST_PATH / input_file)["objects"]
    exodict = get_exodict(ExodictType.SCIENTIFIC)
    transform_object_into_vupnu(
        data,
        exodict,
        warning_messages=[],
        with_colors=with_colors,
    )

    for obj in data:
        assert_that(obj["parameters"]["position"]).contains_entry(
            {
                "distance": {
                    "value": 18.2,
                    "unit": {
                        "name": "parsec",
                        "symbol": "pc",
                        "wiki_data_rdf": "Q12129",
                    },
                    "pos_uncert": None,
                    "neg_uncert": None,
                }
            }
        )
