-- CREATE SCHEMA EXODAM
CREATE SCHEMA exodam;

-- CREATE skeleton table
CREATE TABLE exodam.exodam_skel (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE exodam types table
CREATE TABLE exodam.exodam_types (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE known keys table
CREATE TABLE exodam.exodam_known_keys (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE vupnu keys table
CREATE TABLE exodam.exodam_vupnu_keys (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE ENUM type for data type
CREATE TYPE exodam.exodam_cerberus_type AS ENUM (
    'type__objects',
    'type__permissive',
    'type__base'
);

-- CREATE ENUM type for name
CREATE TYPE exodam.exodam_cerberus_data_name AS ENUM (
    'identity',
    'internal',
    'parameters',
    'references',
    'permissive',
    'keys',
    'objects_type',
    'relationship',
    'units'
);

-- CREATE cerberus table
CREATE TABLE exodam.exodam_cerberus (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE cerberus data table
CREATE TABLE exodam.exodam_cerberus_data (
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE cerberus2data table
CREATE TABLE exodam.exodam_cerberus2data (
    id serial NOT NULL PRIMARY KEY,
    id_cerberus integer REFERENCES exodam.exodam_cerberus (id),
    name exodam.exodam_cerberus_data_name NOT NULL,
    cerberus_type exodam.exodam_cerberus_type NOT NULL,
    id_cerberus_data integer REFERENCES exodam.exodam_cerberus_data (id),
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE ENUM type for name
CREATE TYPE exodam.exodict_data_name AS ENUM (
    'exo_type',
    'identification',
    'internal',
    'parameters',
    'reference',
    'relationship'
);

-- CREATE exodict table
CREATE TABLE exodam.exodict (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE exodict data table
CREATE TABLE exodam.exodict_data (
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE exodict2data table
CREATE TABLE exodam.exodict2data (
    id serial NOT NULL PRIMARY KEY,
    id_exodict integer REFERENCES exodam.exodict (id),
    name exodam.exodict_data_name NOT NULL,
    id_data integer REFERENCES exodam.exodict_data (id),
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE ENUM type for name
CREATE TYPE exodam.exodict_editorial_data_name AS ENUM (
    'news'
);

-- CREATE exodict editorial table
CREATE TABLE exodam.exodict_editorial (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE exodict editorial data table
CREATE TABLE exodam.exodict_editorial_data (
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE exodict editorial2data table
CREATE TABLE exodam.exodict_editorial2data (
    id serial NOT NULL PRIMARY KEY,
    id_exodict integer REFERENCES exodam.exodict_editorial (id),
    name exodam.exodict_editorial_data_name NOT NULL,
    id_data integer REFERENCES exodam.exodict_editorial_data (id),
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE exodict exodam editorial skel table
CREATE TABLE exodam.exodam_editorial_skel (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE editorial known keys table
CREATE TABLE exodam.exodam_editorial_known_keys (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE ENUM type for editorial data type
CREATE TYPE exodam.exodam_editorial_cerberus_type AS ENUM (
    'type__base'
);

-- CREATE ENUM type for editorial name
CREATE TYPE exodam.exodam_editorial_cerberus_data_name AS ENUM (
    'news'
);

-- CREATE editorial cerberus table
CREATE TABLE exodam.exodam_editorial_cerberus (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE editorial cerberus data table
CREATE TABLE exodam.exodam_editorial_cerberus_data (
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE editorial cerberus2data table
CREATE TABLE exodam.exodam_editorial_cerberus2data (
    id serial NOT NULL PRIMARY KEY,
    id_cerberus integer REFERENCES exodam.exodam_editorial_cerberus (id),
    name exodam.exodam_editorial_cerberus_data_name NOT NULL,
    cerberus_type exodam.exodam_editorial_cerberus_type NOT NULL,
    id_cerberus_data integer REFERENCES exodam.exodam_cerberus_data (id),
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE ENUM type for name
CREATE TYPE exodict_editorial_data_name AS ENUM (
    'news'
);

-- CREATE exodict editorial table
CREATE TABLE exodict_editorial (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE exodict editorial data table
CREATE TABLE exodict_editorial_data (
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE exodict editorial2data table
CREATE TABLE exodict_editorial2data (
    id serial NOT NULL PRIMARY KEY,
    id_exodict integer REFERENCES exodict_editorial (id),
    name exodict_editorial_data_name NOT NULL,
    id_data integer REFERENCES exodict_editorial_data (id),
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE exodict exodam editorial skel table
CREATE TABLE exodam_editorial_skel (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE editorial known keys table
CREATE TABLE exodam_editorial_known_keys (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE ENUM type for editorial data type
CREATE TYPE exodam_editorial_cerberus_type AS ENUM (
    'type__base'
);

-- CREATE ENUM type for editorial name
CREATE TYPE exodam_editorial_cerberus_data_name AS ENUM (
    'news'
);

-- CREATE editorial cerberus table
CREATE TABLE exodam_editorial_cerberus (
    id serial NOT NULL PRIMARY KEY,
    refereed boolean DEFAULT FALSE NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE editorial cerberus data table
CREATE TABLE exodam_editorial_cerberus_data (
    id serial NOT NULL PRIMARY KEY,
    data jsonb NOT NULL,
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);

-- CREATE editorial cerberus2data table
CREATE TABLE exodam_editorial_cerberus2data (
    id serial NOT NULL PRIMARY KEY,
    id_cerberus integer REFERENCES exodam_editorial_cerberus (id),
    name exodam_editorial_cerberus_data_name NOT NULL,
    cerberus_type exodam_editorial_cerberus_type NOT NULL,
    id_cerberus_data integer REFERENCES exodam.exodam_cerberus_data (id),
    created timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);
