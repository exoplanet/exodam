"""Exceptions for the exodam_resource module."""

# First party imports
from exodam_utils import ExodamError


class ExodamResourceError(ExodamError):
    """Base Exception for the Exodam Resource module."""


class WhatTheFuckError(ExodamResourceError):
    """Initialize a WhatTheFuckError exception."""
