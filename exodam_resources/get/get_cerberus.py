"""Function to get content of cerberus."""

# Standard imports
from typing import Any, cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import SQLEnumerable

# First party imports
from exodam_utils import conn_manager, get_refereed_id
from exodict import ExodictType

# Local imports
from ..exception import WhatTheFuckError
from ..utils import CerberusName, CerberusResources, CerberusType


def get_cerberus_content(
    exodict_type: ExodictType,
    conn: Connection | None = None,
    name_or_type: CerberusResources | None = None,
) -> dict[str, Any]:
    """
    Get content of one or all cerberus check.

    If we give a name or a type we search in the db the corresponding resource(s),
    otherwise we get all resources which are of reference.

    Args:
        exodict_type: Exodict Type we want cerberus validation resource(s).
        conn: Connection to a db. Use in test.
        name_or_type: Name or type of the resource we want.
            By default, None to obtain all reference resources.

    Returns:
        Given name or type or all cerberus check resources.

    Raises:
        ValueError: If `name_or_type` is of the wrong type or is not equal to None.
        WhatTheFuckError: This error should never be raised.
    """
    if name_or_type is not None and not isinstance(
        name_or_type, (CerberusName, CerberusType)
    ):
        allow_values = CerberusName.as_str_list()
        allow_values.extend(CerberusType.as_str_list())
        raise ValueError(
            f"{name_or_type} is not a valid cerberus name or type."
            " Valid value for `name_or_type`: "
            f"{allow_values} or None.",
        )

    tables = exodict_type.value
    record: Enumerable | None = None
    with conn_manager(conn) as connection:
        ref_cerberus_id = get_refereed_id(connection, tables.cerberus)

        sqle_all_cerberus = SQLEnumerable(connection, tables.cerberus2data.name).join(
            inner=SQLEnumerable(connection, tables.cerberus_data.name),
            outer_key=lambda cerberus2data: cerberus2data.id_cerberus_data,
            inner_key=lambda cerberus_data: cerberus_data.id,
            # Type ignore because:
            # https://github.com/python/mypy/issues/15459
            result_function=(
                lambda cerb_data, cerb2data: {  # type: ignore[misc, arg-type]
                    "name": cerb2data.name,
                    "data": cerb_data.data,
                }
            ),
        )

        # TODO: trouver un meilleur moyen de faire
        match exodict_type:
            case ExodictType.SCIENTIFIC:
                sqle_all_cerberus.where(
                    lambda x: x.exodam_cerberus2data.id_cerberus == ref_cerberus_id
                )
            case ExodictType.EDITORIAL:
                sqle_all_cerberus.where(
                    lambda x: x.exodam_editorial_cerberus2data.id_cerberus
                    == ref_cerberus_id
                )

        match name_or_type:
            case None:
                record = cast(Enumerable, sqle_all_cerberus.execute())
            case CerberusType():
                cerberus_type = cast(CerberusType, name_or_type.value)

                # TODO: trouver un meilleur moyen de faire
                match exodict_type:
                    case ExodictType.SCIENTIFIC:
                        fquery_filter_on_type = (  # noqa: E731
                            lambda x: x.exodam_cerberus2data.cerberus_type
                            == cerberus_type
                        )
                    case ExodictType.EDITORIAL:
                        fquery_filter_on_type = (  # noqa: E731
                            lambda x: x.exodam_editorial_cerberus2data.cerberus_type
                            == cerberus_type
                        )

                record = cast(
                    Enumerable,
                    sqle_all_cerberus.where(
                        fquery_filter_on_type  # type: ignore[arg-type, return-value]
                    ).execute(),
                )
            case CerberusName():
                cerberus_name = cast(CerberusName, name_or_type.value)

                # TODO: trouver un meilleur moyen de faire
                match exodict_type:
                    case ExodictType.SCIENTIFIC:
                        fquery_filter_on_name = (  # noqa: E731
                            lambda x: x.exodam_cerberus2data.name == cerberus_name
                        )
                    case ExodictType.EDITORIAL:
                        fquery_filter_on_name = (  # noqa: E731
                            lambda x: x.exodam_editorial_cerberus2data.name
                            == cerberus_name
                        )

                record = cast(
                    Enumerable,
                    sqle_all_cerberus.where(
                        fquery_filter_on_name  # type: ignore[arg-type, return-value]
                    ).execute(),
                )

                return cast(dict[str, Any], record.to_list()[0].data)
            # No cover because it's just a security but we never enter in this else...
            case _:  # pragma: no cover
                raise WhatTheFuckError(
                    "Sorry, we don't know what's going on. This error should "
                    "not be raised."
                )

    return {cerberus.name: cerberus.data for cerberus in record.to_list()}
