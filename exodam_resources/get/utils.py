"""Useful functions to get exodam resources."""

# Third party imports
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import SQLEnumerable

# First party imports
from exodam_utils import Table, conn_manager


def get_exodam_resources(conn: Connection | None, table: Table) -> Enumerable:
    """
    Get exodam resources.

    Args:
        conn: Connection to a db.
        table: Table for the selection in the db.

    Returns:
        A representation of an exodam resources.
    """
    with conn_manager(conn) as connection:
        return (
            SQLEnumerable(connection, table.name)
            .select(lambda x: x.data)
            .where(lambda x: x.refereed == True)  # noqa: E712
            .first()
            .execute()
        )
