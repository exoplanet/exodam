"""Function to get known keys."""

# Standard imports
from typing import cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable

# First party imports
from exodict import ExodictType

# Local imports
from .utils import get_exodam_resources


def get_known_keys(
    exodict_type: ExodictType,
    conn: Connection | None = None,
) -> list[str]:
    """
    Get known keys.

    Args:
        exodict_type: Exodict type we want the skel.
        conn: Connection to a db. Use in test.

    Returns:
        All known keys for detecting unknown keys.
    """
    table = exodict_type.value.known_keys
    known_keys: Enumerable = get_exodam_resources(
        conn,
        table,
    )
    return cast(list, known_keys.to_list()[0].data["known_keys"])
