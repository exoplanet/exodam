"""Function to get yaml skeleton."""

# Standard imports
from typing import cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable

# First party imports
from exodam_utils import ExodamSqlTables

# Local imports
from .utils import get_exodam_resources


def get_vupnu_keys(conn: Connection | None = None) -> list[str]:
    """
    Get vupnu keys.

    Args:
        conn: Connection to a db. Use in test.

    Returns:
        All vupnu keys.
    """
    vupnu_keys: Enumerable = get_exodam_resources(
        conn,
        ExodamSqlTables.exodam_vupnu_keys,
    )

    return cast(list, vupnu_keys.to_list()[0].data["vupnu_keys"])
