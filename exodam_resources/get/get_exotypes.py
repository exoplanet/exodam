"""Function to get exdam types."""

# Standard imports
from typing import cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable

# First party imports
from exodam_utils import ExodamSqlTables

# Local imports
from .utils import get_exodam_resources


def get_exodam_types(conn: Connection | None = None) -> list[str]:
    """
    Get exodam types.

    Args:
        conn: Connection to a db. Use in test.

    Returns:
        All possible exodam types.
    """
    exodam_type: Enumerable = get_exodam_resources(
        conn,
        ExodamSqlTables.exodam_types,
    )
    return cast(list, exodam_type.to_list()[0].data["exodam_types"])
