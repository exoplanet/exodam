"""Function to get yaml skeleton."""

# Standard imports
from typing import Any, cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable

# First party imports
from exodict import ExodictType

# Local imports
from .utils import get_exodam_resources


def get_yml_skel(
    exodict_type: ExodictType,
    conn: Connection | None = None,
) -> dict[str, Any]:
    """
    Get yaml skeleton.

    Args:
        exodict_type: Exodict type we want the skel.
        conn: Connection to a db. Use in test.

    Returns:
        A representation of yaml skeleton.
    """
    table = exodict_type.value.skel
    skel: Enumerable = get_exodam_resources(conn, table)
    return cast(dict[str, Any], skel.to_list()[0].data)
