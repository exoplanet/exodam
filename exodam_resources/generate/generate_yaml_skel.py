"""Generate yaml skeleton from exodict."""

# Standard imports
from collections.abc import Callable
from typing import Any

# Third party imports
from psycopg import Connection

# First party imports
from exodict import ExodictType, get_exodict

# Local imports
from .utils import update_and_insert


def _get_useful_keys(
    exodict: dict[str, Any],
    condition: Callable = lambda x: False,
) -> dict[str, Any] | None:
    """
    Get key used in the yaml skeleton.

    Args:
        exodict: dictionary from which we want to extract keys.
        condition: Lambda function to give more conditions.

    Returns:
        All keys in a dictionary who fulfills the conditions.
    """
    result = {}
    if isinstance(exodict, dict):
        for key, value in exodict.items():
            if isinstance(value, dict) and condition(value):
                result[key] = _get_useful_keys(value, condition)

    return result if result else None


def generate_yml_skel(
    conn: Connection | None,
    exodict_type: ExodictType,
) -> dict[str, Any]:
    """
    Generate the yaml skeleton.

    Args:
        conn: Connection to a db. Use in test.
        exodict_type: Exodict type we want the skel.

    Returns:
        Generated yaml skeleton.
    """
    exodict_content = get_exodict(exodict_type, conn)

    result = {}

    for key, value in exodict_content.items():
        if key not in ["units", "objects"]:
            result[key] = _get_useful_keys(value, lambda x: "label" in x.keys())

    return result


def generate_and_insert_yml_skel(
    exodict_type: ExodictType,
    conn: Connection | None = None,
) -> None:
    """
    Generate the yaml skeleton and insert it into the database.

    This generate skeleton is the new referent so we remove this status to
    other skeleton in the database.

    Args:
        exodict_type: Exodict type we want generate the skel.
        conn: Connection to a db. Use in test.
    """
    update_and_insert(
        conn, exodict_type.value.skel, generate_yml_skel(conn, exodict_type)
    )
