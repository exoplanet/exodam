"""Useful functions to generate exodam resources."""

# Standard imports
from typing import Any

# Third party imports
from psycopg import Connection
from py_linq_sql import SQLEnumerable

# First party imports
from exodam_utils import Table, conn_manager


def update_and_insert(
    conn: Connection | None,
    table: Table,
    refereed_data: dict[str, Any],
) -> None:
    """
    Update a table and insert new refereed data into this table.

    Args:
        conn: Connection to a db.
        table: Table for the insertion in the db.
        refereed_data: New refereed data to insert.
    """
    with conn_manager(conn) as connection:
        # TODO: remove the type ignore and the noqa E501 when this issue is resolve
        # https://gitlab.obspm.fr/exoplanet/py-linq-sql/-/issues/41

        # Update old refereed to remove his referent status
        SQLEnumerable(connection, table.name).update(
            lambda x: x.refereed == False  # type: ignore[arg-type, return-value] # noqa: E712 E501
        ).where(
            lambda x: x.refereed == True  # noqa: E712
        ).execute()

        # Insert the new referent
        SQLEnumerable(connection, table.name).insert(
            table.insert_columns,
            (True, refereed_data),
        ).execute()
