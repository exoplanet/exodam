"""Generate known keys from exodict."""

# Standard imports
import re
from collections.abc import Callable
from typing import Any

# Third party imports
from psycopg import Connection

# First party imports
from exodam_utils import REGEX_GS, REGEX_UNIT
from exodict import ExodictType, get_exodict

# Local imports
from ..get.get_exotypes import get_exodam_types
from .utils import update_and_insert


def _get_key_list(
    exodict: dict[str, Any],
    condition: Callable = lambda x: False,
) -> list[str]:
    """
    Get recursively list of keys in a exodict.

    Args:
        exodict: dictionary from which we want to extract the keys.
        condition: Lambda function to give more conditions.

    Returns:
        All keys in a dictionary who fulfills the conditions.
    """
    if not isinstance(exodict, dict) or condition(exodict):
        return []

    keys = list(exodict.keys())
    result = []

    for key in keys:
        result.append(key)
        result.extend(_get_key_list(exodict[key], condition))

    return result


def get_all_keys_for_exodict_type(exodict_type: ExodictType) -> list[str]:
    """
    Get base all keys for an exodict type.

    Args:
        exodict_type: Exodict type we want the base all keys.

    Returns:
        Base all keys.
    """
    all_keys = ["wiki_data_rdf"]
    match exodict_type:
        case ExodictType.SCIENTIFIC:
            all_keys.extend(
                [
                    "value",
                    "pos_uncert",
                    "neg_uncert",
                    "uncert",
                    "unit",
                    "objects_type",
                    "symbol",
                ]
            )
        case ExodictType.EDITORIAL:
            pass

    return all_keys


def get_other_keys_for_exodict_type(
    conn: Connection | None,
    exodict_type: ExodictType,
) -> list[str]:
    """
    Get other keys for an exodict type.

    Args:
        conn: Connection to a db. Use in test.
        exodict_type: Exodict type we want the other keys.

    Returns:
        Other keys.
    """
    other_keys = ["required", "nullable"]
    match exodict_type:
        case ExodictType.SCIENTIFIC:
            other_keys.extend(get_exodam_types(conn))
        case ExodictType.EDITORIAL:
            pass

    return other_keys


def generate_keys(conn: Connection | None, exodict_type: ExodictType) -> list[str]:
    """
    Generate known keys from exodict.

    Args:
        conn: Connection to a db. Use in test.
        exodict_type: Exodict type we want generate known keys.
    """
    exodict_content = get_exodict(exodict_type, conn)

    result = []
    # tmp contains key for vupnu and units
    match exodict_type:
        case ExodictType.SCIENTIFIC:
            all_keys = get_all_keys_for_exodict_type(ExodictType.SCIENTIFIC)
            others_keys = get_other_keys_for_exodict_type(conn, ExodictType.SCIENTIFIC)
        case ExodictType.EDITORIAL:
            all_keys = get_all_keys_for_exodict_type(ExodictType.EDITORIAL)
            others_keys = get_other_keys_for_exodict_type(conn, ExodictType.EDITORIAL)

    all_keys.extend(
        _get_key_list(
            exodict_content,
            lambda x: "definition" in x.keys() or "wiki_data_rdf" in x.keys(),
        )
    )

    for key in set(all_keys):
        if not (
            re.match(REGEX_GS, key) or re.match(REGEX_UNIT, key) or key in others_keys
        ):
            result.append(key)

    return result


def generate_and_insert_known_keys(
    exodict_type: ExodictType,
    conn: Connection | None = None,
) -> None:
    """
    Generete known keys.

    This generated known_keys are new referents so we remove this status to
    other known_keys in the database.

    Args:
        exodict_type: Exodict type we want generate known keys.
        conn: Connection to a db. Use in test.
    """
    update_and_insert(
        conn,
        exodict_type.value.known_keys,
        {"known_keys": generate_keys(conn, exodict_type)},
    )
