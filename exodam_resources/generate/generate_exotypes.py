"""Generate exodam types from exodict."""

# Third party imports
from psycopg import Connection

# First party imports
from exodam_utils import ExodamSqlTables
from exodict import ExodictScientificName, ExodictType, get_exodict

# Local imports
from .utils import update_and_insert


def generate_exodam_types(conn: Connection | None) -> list[str]:
    """
    Generate exodam types from exodict.

    Args:
        conn: Connection to a db. Use in test.

    Returns:
        All possible exodam types.
    """
    exotype_dict = get_exodict(
        ExodictType.SCIENTIFIC,
        conn,
        ExodictScientificName.EXO_TYPE,
    )["objects"]
    return list(exotype_dict.keys())


def generate_and_insert_exodam_types(conn: Connection | None = None) -> None:
    """
    Generete exodam types.

    This generate types are new referents so we remove this status to
    other types in the database.

    Args:
        conn: Connection to a db. Use in test.
    """
    update_and_insert(
        conn,
        ExodamSqlTables.exodam_types,
        {"exodam_types": generate_exodam_types(conn)},
    )
