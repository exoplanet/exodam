"""Generate cerberus validation resources from exodict."""

# Standard imports
from collections.abc import Collection
from typing import Any, TypeAlias, cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import SQLEnumerable

# First party imports
from exodam_utils import Table, conn_manager, get_refereed_id
from exodict import ExodictType

# Local imports
from ...utils import EditorialCerberusName as eCN
from ...utils import EditorialCerberusType as eCT
from ...utils import ScientificCerberusName as sCN
from ...utils import ScientificCerberusType as sCT
from .generate_editorial_part_of_cerberus import generate_news
from .generate_scientific_part_of_cerberus import (
    generate_identity,
    generate_internal,
    generate_keys,
    generate_objects_type,
    generate_parameters,
    generate_permissive,
    generate_references,
    generate_relationship,
    generate_units,
)

cerberus_generated_info_type: TypeAlias = list[dict[str, str | Collection[str]]]


def _insert_cerberus_data(conn: Connection, table: Table, data: dict[str, Any]) -> int:
    """
    Insert in a db a cerberus data.

    Args:
        conn: Connection to the database.
        table: Table to insert data.
        data: Data to insert.

    Returns:
        Id of the inserted element.
    """
    SQLEnumerable(conn, table.name).insert(table.insert_columns, (data)).execute()

    record: Enumerable = (
        SQLEnumerable(conn, table.name)
        .select(lambda x: x.id)
        .where(lambda x: x.data == data)
        .last()
        .execute()
    )

    return cast(int, record.to_list()[0].id)


def _insert_all_cerberus_data(
    conn: Connection,
    table: Table,
    all_cerberus_info: cerberus_generated_info_type,
) -> dict[str, int]:
    """
    Insert in a db all cerberus data.

    Args:
        conn: Connection to the database.
        table: Table to insert data.
        all_cerberus_info: Information on cerberus to insert.

    Returns:
        Ids of inserted elements.
    """
    return {
        cast(str, info["name"]): _insert_cerberus_data(
            conn,
            table,
            cast(dict[str, Any], info["data"]),
        )
        for info in all_cerberus_info
    }


def _insert_all_cerberus2data(
    conn: Connection,
    table: Table,
    all_cerberus_info: cerberus_generated_info_type,
    cerberus_ref_id: int,
    data_ids: dict[str, int],
) -> None:
    """
    Insert in a db all cerberus2data elements.

    Args:
        conn: Connection to the database.
        table: Table to insert data.
        all_cerberus_info: Information on cerberus to insert.
        cerberus_ref_id: Id of the reference cerberus element in the database.
        data_ids: Ids of data in the database.
    """
    elements_to_insert = [
        (cerberus_ref_id, info["name"], info["type"], data_ids[cast(str, info["name"])])
        for info in all_cerberus_info
    ]

    SQLEnumerable(conn, table.name).insert(
        table.insert_columns,
        elements_to_insert,  # type: ignore[arg-type]
    ).execute()


def _insert_cerberus(
    conn: Connection | None,
    exodict_type: ExodictType,
    data_info: cerberus_generated_info_type,
) -> None:
    """
    Insert cerberus resources.

    Args:
        conn: Connection to the database to insert.
        exodict_type: Exodict type of the data to insert.
        data_info: Info to insert.
    """
    tables = exodict_type.value
    with conn_manager(conn) as connection:
        # Update old refereed to remove his referent status

        # TODO: remove the type ignore and the noqa E501 when this issue is resolve
        # https://gitlab.obspm.fr/exoplanet/py-linq-sql/-/issues/41
        SQLEnumerable(connection, tables.cerberus.name).update(
            lambda x: x.refereed == False  # type: ignore[arg-type, return-value] # noqa: E712 E501
        ).where(
            lambda x: x.refereed == True  # noqa: E712
        ).execute()

        # Insert a new refereed exodam cerberus

        # TODO: REMOVE the type ignore when this issue will be fixed.
        # https://gitlab.obspm.fr/exoplanet/py-linq-sql/-/issues/46
        SQLEnumerable(connection, tables.cerberus.name).simple_insert(
            refereed=True  # type: ignore[arg-type]
        ).execute()

        cerberus_ref_id = get_refereed_id(connection, tables.cerberus)
        data_ids = _insert_all_cerberus_data(
            connection,
            tables.cerberus_data,
            data_info,
        )
        _insert_all_cerberus2data(
            connection,
            tables.cerberus2data,
            data_info,
            cerberus_ref_id,
            data_ids,
        )


def _get_data_info(exodict_type: ExodictType) -> cerberus_generated_info_type:
    """
    Get data info depending on an exodict type.

    Args:
        exodict_type: Exodict type for get data info.

    Returns:
        Data info depending on given exodict type.
    """
    match exodict_type:
        case ExodictType.SCIENTIFIC:
            return [
                {
                    "type": sCT.TYPE__BASE.value,
                    "name": sCN.KEYS.value,
                    "data": generate_keys(),
                },
                {
                    "type": sCT.TYPE__BASE.value,
                    "name": sCN.OBJECTS_TYPE.value,
                    "data": generate_objects_type(),
                },
                {
                    "type": sCT.TYPE__BASE.value,
                    "name": sCN.RELATIONSHIP.value,
                    "data": generate_relationship(),
                },
                {
                    "type": sCT.TYPE__BASE.value,
                    "name": sCN.UNITS.value,
                    "data": generate_units(),
                },
                {
                    "type": sCT.TYPE__PERMISSIVE.value,
                    "name": sCN.PERMISSIVE.value,
                    "data": generate_permissive(),
                },
                {
                    "type": sCT.TYPE__OBJECTS.value,
                    "name": sCN.IDENTITY.value,
                    "data": generate_identity(),
                },
                {
                    "type": sCT.TYPE__OBJECTS.value,
                    "name": sCN.INTERNAL.value,
                    "data": generate_internal(),
                },
                {
                    "type": sCT.TYPE__OBJECTS.value,
                    "name": sCN.REFERENCES.value,
                    "data": generate_references(),
                },
                {
                    "type": sCT.TYPE__OBJECTS.value,
                    "name": sCN.PARAMETERS.value,
                    "data": {"parameters": generate_parameters()},
                },
            ]
        case ExodictType.EDITORIAL:
            return [
                {
                    "type": eCT.TYPE__BASE.value,
                    "name": eCN.NEWS.value,
                    "data": generate_news(),
                }
            ]
        # No cover because it's just a security
        case _:  # pragma: no cover
            raise NotImplementedError()


def generate_cerberus_and_insert(
    exodict_type: ExodictType,
    conn: Connection | None = None,
) -> None:
    """
    Generete cerberus validation resources.

    This generate cerberus are new referents so we remove this status to
    other cerberus in the database.

    Args:
        exodict_type: Exodict type of cerberus to generate.
        conn: Connection to a db. Use in test.
    """
    all_data_info = _get_data_info(exodict_type)
    _insert_cerberus(conn, exodict_type, all_data_info)
