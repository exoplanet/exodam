"""Generate scientific cerberus validation parts from exodict."""

# Standard imports
from typing import Any

# First party imports
from exodam_utils import REGEX_UNIT
from exodict import ExodictScientificName, ExodictType, get_exodict

# Local imports
from .cerberus_utils import get_cerberus_rules_from_exodict, get_one_any_of


def _generate_objects() -> dict[str, Any]:
    """
    Get cerberus rules for 'objects'.

    Returns:
        Cerberus rules for 'objects'.
    """
    objects_identity_exodict = get_exodict(
        ExodictType.SCIENTIFIC,
        name=ExodictScientificName.IDENTIFICATION,
    )

    result = {
        "type": "list",
        "required": True,
        "schema": {
            "type": "dict",
            "schema": {
                "identity": get_cerberus_rules_from_exodict(
                    objects_identity_exodict["identity"]
                )
            },
        },
    }
    return {"objects": result}


def generate_relationship() -> dict[str, Any]:
    """
    Get cerberus rules for 'relationship'.

    Returns:
        Cerberus rules for 'relationship'.
    """
    relationship_exodict = get_exodict(
        ExodictType.SCIENTIFIC,
        name=ExodictScientificName.RELATIONSHIP,
    )

    return {
        "relationship": get_cerberus_rules_from_exodict(
            relationship_exodict["relationship"]
        )
    }


# TODO: Au revoir.
def generate_objects_type() -> dict[str, Any]:
    """
    Get cerberus rules for 'objects_type'.

    Returns:
        Cerberus rules for 'objects_type'.
    """
    exo_type = get_exodict(ExodictType.SCIENTIFIC, name=ExodictScientificName.EXO_TYPE)
    return {
        "objects_type": {
            "type": "list",
            "required": True,
            "schema": {
                "type": "string",
                "anyof": get_one_any_of(exo_type["objects"].keys()),
            },
        }
    }


def generate_permissive() -> dict[str, Any]:
    """
    Get cerberus rules for permissive checking.

    Returns:
        Cerberus rules for permissive checking.
    """
    res = {}
    res.update(generate_objects_type())
    res.update(_generate_objects())
    res.update(generate_relationship())
    return res


def generate_identity() -> dict[str, Any]:
    """
    Get cerberus rules for 'identity'.

    Returns:
        Cerberus rules for 'identity'.
    """
    identity_exodict = get_exodict(
        ExodictType.SCIENTIFIC,
        name=ExodictScientificName.IDENTIFICATION,
    )
    return {"identity": get_cerberus_rules_from_exodict(identity_exodict["identity"])}


def generate_references() -> dict[str, Any]:
    """
    Get cerberus rules for 'references'.

    Returns:
        Cerberus rules for 'references'.
    """
    references_exodict = get_exodict(
        ExodictType.SCIENTIFIC,
        name=ExodictScientificName.REFERENCE,
    )
    return {
        "references": get_cerberus_rules_from_exodict(references_exodict["references"])
    }


def generate_internal() -> dict[str, Any]:
    """
    Get cerberus rules for 'internal'.

    Returns:
        Cerberus rules for 'internal'.
    """
    internal_exodict = get_exodict(
        ExodictType.SCIENTIFIC,
        name=ExodictScientificName.INTERNAL,
    )
    return {"internal": get_cerberus_rules_from_exodict(internal_exodict["internal"])}


def generate_keys() -> dict[str, Any]:
    """
    Get cerberus rules for 'keys'.

    We generate it manually because it a unique stricutre a little bit "complex".

    Returns:
        Cerberus rules for 'keys'.
    """
    return {
        "objects": {
            "type": "list",
            "required": True,
            "schema": {
                "type": "dict",
                "schema": {
                    "identity": {"type": "dict", "required": True},
                    "parameters": {"type": "dict", "required": True},
                    "references": {"type": "dict", "required": True},
                    "internal": {"type": "dict"},
                },
            },
        }
    }


def generate_units() -> dict[str, Any]:
    """
    Get cerberus rules for 'units'.

    We generate it manually because it a unique stricutre a little bit "complex".

    Returns:
        Cerberus rules for 'units'.
    """
    return {
        "units": {
            "type": "dict",
            "keysrules": {"type": "string", "regex": REGEX_UNIT},
            "valuesrules": {
                "type": "list",
                "schema": {
                    "type": "dict",
                    "schema": {
                        "name": {"type": "string", "required": True},
                        "symbol": {
                            "type": "list",
                            "required": True,
                            "schema": {"type": "string"},
                        },
                        "wiki_data_rdf": {
                            "type": "string",
                            "required": True,
                            "nullable": True,
                        },
                    },
                },
            },
        }
    }


def generate_parameters() -> dict[str, Any]:
    """
    Get cerberus rules for 'parameters'.

    Warning:
        This function return just the cerberus dictionary. In the master function you
        need to create a new dict with key = "parameters" and key the result of this
        function.

    Returns:
        Cerberus rules for 'parameters'.
    """
    parameters_exodict = get_exodict(
        ExodictType.SCIENTIFIC,
        name=ExodictScientificName.PARAMETERS,
    )
    return get_cerberus_rules_from_exodict(parameters_exodict["parameters"])
