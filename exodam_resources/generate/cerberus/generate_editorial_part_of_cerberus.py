"""Generate editorial cerberus validation parts from exodict."""

# Standard imports
from typing import Any

# First party imports
from exodict import ExodictEditorialName, ExodictType, get_exodict

# Local imports
from .cerberus_utils import get_cerberus_rules_from_exodict


def generate_news() -> dict[str, Any]:
    """
    Get cerberus rules for 'news'.

    Returns:
        Cerberus rules for 'news'.
    """
    news_exodict = get_exodict(ExodictType.EDITORIAL, name=ExodictEditorialName.NEWS)

    return {
        "news": {
            "type": "list",
            "schema": get_cerberus_rules_from_exodict(news_exodict["news"]),
        },
    }
