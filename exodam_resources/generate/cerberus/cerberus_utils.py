"""Useful function to generate cerberus parts."""

# Standard imports
from typing import Any

# Dictionary structure for vupnu parameters
# The vupnu parameters are those that can be a float or a dict to specify
# the unit and/or the uncertainties
VUPNU = {
    "type": ["float", "dict"],
    "nullable": True,
    "schema": {
        "value": {"type": "float"},
        "uncert": {"type": "float"},
        "pos_uncert": {"type": "float"},
        "neg_uncert": {"type": "float"},
        "unit": {"type": "string"},
    },
}


def _get_correct_type(value: str) -> dict[str, Any]:
    """
    Get the correct type form for a 'type' given the dictionary.

    Args:
        values: Value to interpret.

    Returns:
        The type correspond to the value.

    Raises:
        ValueError: If `value` can't be interpreted.

    Examples:
        >>> _get_correct_type("str")
        {'type': 'string'}

        >>> _get_correct_type("string")
        {'type': 'string'}

        >>> _get_correct_type("bool")
        {'type': 'boolean'}

        >>> _get_correct_type("boolean")
        {'type': 'boolean'}

        >>> _get_correct_type("dict")
        {'type': 'dict'}

        >>> _get_correct_type("list")
        {'type': 'list', 'schema': {'type': 'string'}}

        >>> _get_correct_type("vupnu")
        {'type': ['float', 'dict'], 'nullable': True, 'schema': {
        ...     'value': {'type': 'float'},
        ...     'uncert': {'type': 'float'},
        ...     'pos_uncert': {'type': 'float'},
        ...     'neg_uncert': {'type': 'float'},
        ...     'unit': {'type': 'string'}}}

        >>> try:
        ...     _get_correct_type("toto")
        ... except (ValueError):
        ...     print(False)
        False
    """
    match value:
        case "str" | "string":
            return {"type": "string"}
        case "bool" | "boolean":
            return {"type": "boolean"}
        case "dict":
            return {"type": "dict"}
        case "list":
            return {"type": "list", "schema": {"type": "string"}}
        case "vupnu":
            return VUPNU
        case _:
            raise ValueError(f"{value} can't be interpreted for cerberus generation")


def get_one_any_of(values: list[str]) -> list[dict[str, str]]:
    """
    Get a sub dict for anyof or oneof.

    Args:
        values: All possible values.

    Returns:
        list of Types and regex of all possible values.

    Examples:
        >>> get_one_any_of(['a', 'b'])
        [{'type': 'string', 'regex': 'a'}, {'type': 'string', 'regex': 'b'}]
    """
    return [{"type": "string", "regex": element} for element in values]


def get_cerberus_rules_from_exodict(exodict: dict[str, Any]) -> dict[str, Any]:
    """
    Get recursively cerberus rules for an complete exodict.

    Args:
        exodict: The Exodict source to get rules.

    Returns:
        Cerberus rules for `exodict`.
    """
    result = {}

    for key, value in exodict.items():
        result.update(_get_correct_type(exodict.get("type", "dict")))

        match key:
            case "required" | "nullable":
                result[key] = True
            case "keysrules":
                result["keysrules"] = {"type": "string", "regex": value}
            case "valuesrules":
                result["valuesrules"] = _get_correct_type(value)
            case "oneof" | "anyof":
                result[key] = get_one_any_of(value)
            case _ if isinstance(value, dict) and key != "label":
                result["type"] = "dict"
                result["schema"] = (
                    result["schema"] if result.get("schema", None) else {}
                )
                result["schema"][key] = get_cerberus_rules_from_exodict(value)
            case _:
                pass

    return result
