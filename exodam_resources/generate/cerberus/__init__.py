"""Generate cerberus module."""

# Local imports
from .generate_cerberus import generate_cerberus_and_insert  # noqa: F401
