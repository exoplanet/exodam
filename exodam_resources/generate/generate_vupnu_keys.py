"""Generate vupnu keys from exodict."""

# Standard imports
from typing import Any

# Third party imports
from flatten_dict import flatten
from flatten_dict.reducers import make_reducer
from psycopg import Connection

# First party imports
from exodam_utils import ExodamSqlTables
from exodict import ExodictScientificName, ExodictType, get_exodict

# Local imports
from .utils import update_and_insert

# TODO: Get from a config file
DICT_FLATTEN_DELIMITER = "_!_&_"
VUPNU_KEY_SEPARATOR = "__"


def _get_normalized_name_for_key(key: str) -> str:
    """
    Get normalized name for a key of parameters exodict.

    Args:
        key: Key to normalize.

    Returns:
        Normalized key.

    Examples:
        >>> _get_normalized_name_for_key("position_!_&_radial_velocity_!_&_type")
        'position__radial_velocity'

        >>> _get_normalized_name_for_key("a_!_&_b_!_&_c_!_&_type")
        'a__b__c'
    """
    return key.removesuffix(f"{DICT_FLATTEN_DELIMITER}type").replace(
        DICT_FLATTEN_DELIMITER, VUPNU_KEY_SEPARATOR
    )


def _generate_list_form_content(content: dict[str, Any]) -> list[str]:
    """
    Generate vupnu key list from given content.

    Args:
        content: Given content contains keys.

    Returns:
        Key list with `vupnu` type.

    Examples:
        >>> content = {
        ...     "parameters": {
        ...         "toto": {
        ...             "a": {"label": "A", "type": "vupnu"},
        ...             "b": {"label": "B", "type": "str"},
        ...         }
        ...     }
        ... }
        >>> _generate_list_form_content(content)
        ['toto__a']
    """
    result = []
    flatten_param_content = flatten(
        content["parameters"], reducer=make_reducer(DICT_FLATTEN_DELIMITER)
    )

    for key, value in flatten_param_content.items():
        if f"{DICT_FLATTEN_DELIMITER}type" in key and value == "vupnu":
            result.append(_get_normalized_name_for_key(key))

    return result


def generate_vupnu_keys() -> list[str]:
    """
    Generate vupnu key.

    Returns:
        All vupnu keys.
    """
    parameters_content = get_exodict(
        ExodictType.SCIENTIFIC,
        name=ExodictScientificName.PARAMETERS,
    )
    return _generate_list_form_content(parameters_content)


def generate_and_insert_vupnu_keys(conn: Connection | None = None) -> None:
    """
    Generete vupnu keys.

    This generated vupnu_keys are new referents so we remove this status to
    other vupnu_keys in the database.

    Args:
        conn: Connection to a db. Use in test.
    """
    update_and_insert(
        conn,
        ExodamSqlTables.exodam_vupnu_keys,
        {"vupnu_keys": generate_vupnu_keys()},
    )
