"""Useful stuff for exodam resources module."""

# Standard imports
from enum import Enum


class CerberusResources(Enum):
    """Enum type cerberus resource."""

    @classmethod
    def as_str_list(cls: type["CerberusResources"]) -> list[str]:
        """Return a list of all values."""
        return [element.value for element in list(cls)]  # type: ignore[var-annotated]


class CerberusName(CerberusResources):
    """Enum type for name of cerberus resource."""


class CerberusType(CerberusResources):
    """Enum type for type of cerberus resource."""


class ScientificCerberusName(CerberusName):
    """Enum type for name of scientific cerberus resource."""

    IDENTITY = "identity"
    INTERNAL = "internal"
    PARAMETERS = "parameters"
    REFERENCES = "references"
    PERMISSIVE = "permissive"
    KEYS = "keys"
    OBJECTS_TYPE = "objects_type"
    RELATIONSHIP = "relationship"
    UNITS = "units"


class ScientificCerberusType(CerberusType):
    """Enum type for type of scientific cerberus resource."""

    TYPE__OBJECTS = "type__objects"
    TYPE__PERMISSIVE = "type__permissive"
    TYPE__BASE = "type__base"


class EditorialCerberusName(CerberusName):
    """Enum type for name of editorial cerberus resource."""

    NEWS = "news"


class EditorialCerberusType(CerberusType):
    """Enum type for type of editorial cerberus resource."""

    TYPE__BASE = "type__base"
