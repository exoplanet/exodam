# Changelog

<!-- markdownlint-disable-file MD024 -->

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.56.0] - 15-02-2024

### Fixed

- `ScientificGenerateParams` and `EditorialGenerateParams` can be imported from exodam with:

```python
  from exodam import ScientificGenerateParams, EditorialGenerateParams
```

## [0.55.0] - 15-02-2024

### Fixed

- `ExodamError` and `ExodictType` can be imported from exodam with:

```python
  from exodam import ExodamError, ExodictType
```

## [0.54.0] - 15-02-2024

### Changed

- Change how we give exodict new file to generate (and create) a new exodict.
  Now we given content of a file(dict) and not a path.

## [0.53.0] - 30-11-2023

### Added

- Add editorial exodict

### Changed

- Change code of:
  - get exodict
  - get yaml skel
  - get known keys
  - get cerberus
  - generate exodict
  - generate yaml skel
  - generate known keys
  - generate cerberus
  - detect unknown
  - watch dogs
  - finalize
  to adapt it to the 2 exodict.

## [0.52.0] - 22-11-2023

### Changed

- Adapt exodam with the new way to get and generate exodict.
- Change generate exodict to generate exodict in a database.
- Change get exodict to take exodict in a database.
- Change generate cerberus to generate the cerberus validation resources in a database.
- Change get cerberus to take the cerberus validation resources in a database.
- Adapt exodam with the new way to get and generate vupnu keys list.
- Change generate vupnu keys list to generate the vupnu keys list in a database.
- Change get vupnu keys list to take the vupnu keys list in a database.
- Adapt exodam with the new way to get and generate known keys list.
- Change generate known keys list to generate the known keys list in a database.
- Change get known keys list to take the known keys list in a database.
- Change generate exodam types to generate types in a database.
- Change get exodam types to take types in a database.
- Adapt exodam with the new way to get and generate yaml skeleton.
- Change generate yaml skeleton to generate the skel in a database.
- Change get yaml skeleton to take the skel in a database.

## [0.51.0] - 17-10-2023

### Changed

- Change functions with rich color to allow you to set colors or not.

## [0.50.0] - 04-10-2023

### Removed

- Remove the json export.

## [0.49.0] - 04-10-2023

### Added

- Add a way to finalize for API and for cli.

### Changed

- Change hash of exodict. Use `sha256`.

## [0.48.0] - 04-10-2023

### Changed

- Change `finalize_a_file` to add the dict to the exodam generate file.

## [0.47.0] - 03-10-2023

### Added

- Add generation and getting of vupnu keys in exodam_ressources.

## [0.46.0] - 03-10-2023

### Changed

- Refactor finalization of a yaml file.

## [0.45.0] - 19-09-2023

### Changed

- Exodam package become Exodam-lib package.

## [0.44.0] - 18-09-2023

### Added

- Add generation and getter of yaml skel from the exodict.

## [0.43.0] - 18-09-2023

### Changed

- Refactor of give skel to have example of yaml file.

## [0.42.0] - 24-03-2023

### Added

- Add a new package `exodam_utils` to manage function useful in all module.

## [0.41.0] - 24-03-2023

### Changed

- Refactor of diff. Now we use the system diff.

## [0.40.0] - 21-03-2023

### Added

- Add ExocamContent type to canonize dict form yaml exodam files.

### Changed

- Change some functions in `ExodamContent` methods.

### Removed

- Remove canonize module, replace by ExodamContent who sort the dict when we init a
  new object.

## [0.39.0] - 20-03-2023

### Added

- Add function to get a content of an exodict yaml file in the exodict module.
- Add generation of exodict in the exodict module.

## [0.38.0] - 16-03-2023

### Added

- Add generation and getting of exodam_types in exodam_resources.
- Add generation and getting of known keys in exodam_resources.
- Add cerberus part in `exodam_resources`.
- Add a new module: `exodam_resources` to manage all useful resources for the project.

## [0.37.0] - 14-03-2023

### Added

- Add a way to validate a file without doi.

### Changed

- Rework generation of cerberus from exodict.
- Rework on the detection and prefixing of unknown keys.
- Rework on the gravitational systems validation.
- Rework on the cerberus permissive validation.
- Rework on the yaml cerberus files.
- Rework on the cerberus strict validation.

## [0.36.0] - 27-02-2023

### Added

- Add labels in exodict and add checking for labels
- Add validation in the generation of an exodict.

### Changed

- Refactor all exodict base file and generation of an exodict file.
- Adapt the generation of the exodict with the new struct of the folder: .exo_dictionary
- Refactor .exo_dictionary folder to merge objects and planet_satellite file.

## [0.35.2] - 15-02-2023

### Fixed

- Fix generation of exodict with planet and/or satellite.

## [0.35.1] - 15-02-2023

### Fixed

- Fix if no unit in exodict, it is a `"PureNumber"`.

## [0.35.0] - 31-01-2022

### Added

- Add a verb `generate_exodict` to get an exodict, we can have a default exodict or
  specify some file for all different part.
- Add a feature to get a .exodict json file with all good dictionaries for an exodam file.

## [0.33.1] - 19-01-2022

### Fixed

- Fix check which validated a file while several objects had the same name

## [0.33.0] - 19-01-2022

### Added

- Add a french translation for README.md
- Add a real README.md

## [0.32.0] - 17-01-2022

### Added

- Add a publication check which checks if there is a publication and if it contains at
  least one URL or one DOI

## [0.31.0] - 14-01-2022

### Changed

- `imp_par` become `impact_param` in all the code
- `comet` become `small_body` in all the code

## [0.30.4] - 13-01-2022

### Fixed

- Fix typo in exodict `magnetic_filed` into `magnetic_field`
- Fix type in exodict `comet` into `small_body`
- Fix typo in exodict `specific_planet_parametre` into `specific_planet_parameter`

## [0.30.0] - 13-01-2022

### Added

- Add [typer](https://typer.tiangolo.com/) and use it instead of CLI

## [0.29.0] - 12-01-2022

### Added

- Add pytestdoc and resolve pydocstring errors

## [0.28.5] - 12-01-2022

### Fixed

- Now finalize check if create_exodam_file worked correctly and display the correct message.

## [0.28.4] - 12-01-2022

### Fixed

- Fix '1 required argument missing' in finalize.py (doctests)
- Fix '1 required argument missing' in transformvupnu.py (doctests)
- Fix '1 required argument missing' in convertvupnu.py (pytest)
- Fix name of an exodict in generatecerberus.py

## [0.28.0] - 11-01-2022

### Added

- Add structured warning message when we used default unit for parameters

Example :

```shell
Warning : for the following parameters we used the default units :
beta pic
        - ra
        - dec
        - age
beta pic ter
        - distance
beta Pic toto
        - distance
        - ra
        - dec
        - age

```

## [0.27.0] - 10-01-2022

### Added

- Add an option to check in CLI : --no_doi and his tests, if no doi is use exodam check
  all parameters but the field DOI are not required

## [0.26.0] - 10-01-2022

### Added

- Add rich traceback for a prettier display of Traceback
- Add [rich](https://github.com/Textualize/rich) to prettier command line user interface

### Changed

- Change all print with colorama into print with console.print of rich
- Modify name of RAunit and DECunit into hms and dms

## [0.25.0] - 07-01-2022

### Added

- Add an exo_dictionary for publication
- Add [just](https://github.com/casey/just) to simplify commands in project
- Add [poetry](https://python-poetry.org/) to manage the project

### Changed

- Change the exo_dictionaries planet with the new specifications from Quentin and
  Françoise

## [0.24.0] - 05-01-2022

### Added

- Add doc and tests for this feature
- Add convert vupnu in finalize
- Add function to convert all simple value can be vupnu into vupnu and get the default
  unit if it is not specified

## [0.23.0] - 20-12-2021

### Added

- Add a little dictionary for gs (gravitational system)

### Changed

- Satellites now have the same parameters as planets (change tests and generate cerberus
  to macth the new definition of satellite)
- Orbital parameters are no longer reserved for planets
- Default for multiple-choice units is the first unit in the list (MJ, Hour, RJ)
- Metallicty no longer has unity

## [0.22.1] - 14-12-2021

### Added

- Add test for generate cerberus in tmp_path
- Add functions to display ExodamDictFile prettier
- Add an Exception : EmptyYaml

### Changed

- Update Contributing file and docs
- All print elsewhere than in CLI were removed in to be put in CLI
- Now all custom Exception need a message
- Now export_exodam create if doesn't exist a directory exodam_files and write the
  .exodam file in this directory by default or in a directory whose path is specified

### Fixed

- Fix a bug in pytest, now we use the real tmp folder for test with the fixture tmp_path
  cf. [pytest doc](https://docs.pytest.org/en/6.2.x/tmpdir.html)

### Removed

- Remove check.py and its tests, now just use the function dog_house

## [0.21.0] - 06-12-2021

### Added

- Add some function and their tests to generate cerberus validation file from an
  exo_dictionary

## [0.20.0] - 30-11-2021

### Added

- Add a function the improve the display of error in cerberus check

## [0.19.0] - 16-11-2021

### Changed

- Use match case rather than if, elif, else when it is necessary
- Change all Optional[Any] to None | Any, new union syntx in python3.10
- Now exodam use to python3.10

## [0.18.3] - 10-11-2021

### Added

- Add a docs with sphinx

### Changed

- Change the docstring in the module srcs to generate correctly the doc

### Fixed

- Fix the doctest in .srcs.detectunknown.get_unknown_keys_in_list(). With a set sometime
  test bug because the set isn't sort.
- Add spectral_type in the yaml example file for init
- Now mass_meas_methods can be RV (Radial Velocity)

## [0.17.0] - 02-11-2021

### Added

- Add a verb to finalize a export. take 2 path and ask 1 name.
- Add a function (and its tests) to finalize a export: take two file (yaml exodam file
  and a .exodict file) and a name for the exported file and create .exodam file with the
  data to describe a system and a dictionary to document the system.

## [0.16.5] - 21-10-2021

### Added

- Add 4 types alias, GravitationalSystem, SubGravitationalSystem, ExodamDictFile,
  ExodamObject
- Add doctests in various functions.
- Add a test export, import diff to check to check if we export a yaml file and reimport
  it, the 2 files are identical.
- Add a file with all dict and list us to check, detect_unknown, sort,...
  .resources_files/utils.yaml
- Add the csv_init folder with all the correct file to import, the folder replaces
  csv_basic.csv which corresponded to the old export

### Changed

- Improve docstring in various functions.
- Change the functions and tests to get the dict and list to sort, detect_unknown,.. for
  load the new files contain these information : .resources_files/utils.yaml
- Change vupnu, we no longer use tag : vupnu in cerberus check file, now use the
  Key-Merge from yaml. tag : vupnu ->

```yaml
Vupnu : &vupnu
    type : [float, dict]
    nullable : True
    schema :
      value :
        type : float
      uncert :
        type : float
      pos_uncert :
        type : float
      neg_uncert :
        type : float
      unit :
        type : string
```

replace_vupnu was deleted. This function is outdated. [merge yaml
doc](https://yaml.org/type/merge.html).

- Change the test for import not gs, now if no gs.csv file return None
- Change path of the yaml file to import. now the file give by import is
  ./'file_name'.yaml not ./'dir_name'/'file_name'.yaml
- Change the init verb, now create a yaml file or a folder with csv files all the
  examples file are in .init/
- Change the location and name of yaml init file

### Fixed

- Fix the import bug, exodam not longer in srcs, no needed anymore import toto and
  import .toto
- Fix the regex for the name of objects in import ".*?[a-z].*" -> ".*?[a-z1-9].*" . Now
  accept name with only number.
- Fix export: if the export is not forced and the file is not valid they did the export
  anyway, now if one of the two is True we do the export otherwise no
- Fix name of some parameters in different files (Magnitude_X -> magnitude_X,
  Rotation_... -> rotation...)
- Fix function and tests in export for the new faltten delimiter '__'

## [0.15.0] - 19-10-2021

### Added

- Add export, import, diff to exodam CLI for verb with their options.

## [0.14.1] - 18-10-2021

### Fixed

- Now canonize sort the parameters in objects

## [0.14.0] - 12-10-2021

### Added

- Add functions to import a csv file, take a 'dir_name' with the csv files to import and
  a 'file_name' who is the name of the yaml exodam file in output. Get the general
  information, Objects information and gs (general and gs is mandatory, objects need at
  least 1 element), write it in the dict and write the dict in a yaml file.
- Add the tests for the import FEATURE

## [0.13.0] - 08-10-2021

### Added

- Add function diff which take two dict, check if the DOI is the same and canonize
  ['objects'] then check if the objects part are identical
- Add test for the verb diff

## [0.12.1] - 07-10-2021

### Added

- Add to export CLI function json to export in json
- Add a verb, 'canonize' to sort a dict with this schema : exo_type (star -> planet ->
  satellite -> comet -> pulsar -> ring -> disk) -> alphabetic by name (a -> ... -> z)

### Changed

- Change how we use the export command (exodam.py export \[OPTIONS\] PATH), with option
  force and format (csv or json, csv by default)

### Fixed

- Bug fix in tojson pytest write the path and not the dict in the export file .json

## [0.11.1] - 05-10-2021

### Added

- Improve the test with pytest mark parametrize and assertpy
- Add a function to extract the name of the different objects in the objects part in
  yaml exodam file (extract_name_of_object)
- Add a function to delete from list of unknown the correct sg with regex

```yaml
^sg[1-9]+$
```

- Add a check for the gs part of a yaml exodam file

### Changed

- Modification of the pytest folder structure for clarity
- Change the syntax of yaml exodam file for the new version (cf Issue #26)

### Fixed

- Load the cerberus unknown check file in the master function to optimise the
  detect_unknwon_recurssive function
- The function detect_unknown now look if there are several times an unknown values and
  add only 1 time in the list of unknown values
- Add the type satellite in exo_type in different cerberus file and check file
- The function out_of_sg is undated change it for get_objects and get_gs

## [0.10.0] - 01-10-2021

### Added

- Add canonize, take a dict and sort it with this schema : exo_type (star -> planet ->
  satellite -> comet -> pulsar -> ring -> disk) -> alphabetic by name (a -> ... -> z)

## [0.9.0] - 29-09-2021

### Added

- Add a licence and modify exodam.py to add contributor authors etc...

### Changed

- Change some tests in pytest

## [0.8.0] - 24-09-2021

### Added

- Add a function to export yaml file in json

## [0.7.0] - 24-09-2021

### Added

- Add a verb "INIT" who create a yaml or csv example file

## [0.6.1] - 22-09-2021

### Fixed

- Fix the errors when a list doesn't contain list or dict in function 'we_have_a_list'
  because str can has no name attribute

## [0.6.0] - 22-09-2021

### Added

- Add export exodam function with input name (default export-exodam-date). Take a
  dictionary and write it in a .exodam file with json.dumbs()

## [0.5.2] - 21-09-2021

### Fixed

- Fix the functions prefix_in_list and check_in_list to accept other types than dict and
  list nad fix the prefix test with it

## [O.5.1] - 20-09-2021

### Fixed

- Fix various molecule name. Now molecule_name take a list with all the molecule name
  (and validate it with regex)

## [0.5.0] - 20-09-2021

### Added

- Add prefixing function to prefix unknown keys and add test for this function

### Changed

- Change the test for unknown-value detetction, now use detect_unknown_value_recurcisive
  the function to detect

## [0.4.5] - 20-09-2021

### Fixed

- Fix a test that was poorly written (if len(0) == 0 and not l == ["chat"] -> if not
  len(l) == 0)

## [0.4.4] - 20-09-2021

### Fixed

- Fix name of some values (molecule : {name : ....} -> molecule : {molecule_name :
  ....]})

## [O.4.3] - 17-09-2021

### Fixed

- Fix type of magnetic_field in unknown_check.yaml (float or dict -> bool)

### Added

- Add test for an empty obl field in permissive and strict
- Add test for an empty opt field in permissive and strict
- Add nullable = True rules in cerberus check files for the not required field
- Add unknown_values in permissive check

### Changed

- Change of all test with assert(check_file(...blabla...)) == True (or False) to
  assert(check_file(...blabla...)) or assert(not check_file(...blabla))
- Update Changelog and version

## [O.3.2] - 17-09-2021

### Fixed

- Fix an error in name of functions and file : detectunknown (unknown -> unknown)

### Added

- Add tests for detect unknown values

## [O.3.1] - 17-09-2021

### Fixed

- Fix one name in cerberus check (lanbda -> lambda)

### Added

- Add a function to detect unknown values for the permissive check

## [0.2.1] - 16-09-2021

### Fixed

- Fix the tests in accordance with the new global check function

### Added

- Add a global check function who call the other

## [0.1.1] - 16-09-2021

### Fixed

- Add a tmp directory to pytest
- Fix some little bug link at cerberus verification
- Yaml format : change parameter_parameter in parameter_value (example :
  distance_distance in distance_value)
- Now we support list in yaml file

### Changed

- Change how do we deal with errors and make some doc
- Update files of validation
- Now export is a commande
- Change how we use click, create a grouped with option version and sub command check
  with 1 argv path and 2 option display and export. Try some test, change load_file for
  a clearer code and add some comments

### Added

- Add pytest for planet object
- Add some test for cerberus verification
- Add some color and message for the users in check modules
- Add planet check
- Add a function who replace 'tag : vupnu by the correct int or dict for each values
  which can take precision
- Add some color in message
- Permissive check as been create and it's work
- Add some test on check and export
- Add some yaml file to check syntax of yaml-exodam files and add first test of check
  this feature
- Add obj.yaml, predoc for yaml file to make the doc and cerberus syntax
- Create a first version of the csv export module with command check -e, give of first
  version example of yaml file and add some test for check
- Import click and use it to manage the 2 parameters, --version (-v) and --check (-c)
- Add some comments and check if the file is .yaml or .yml
- Add exodam.py who check argv and launch functions, check.py who check if the file in
  argv is a correct YAML file and .help, the --help for the exodam script
- Add some plugin in requirement-dev and test become true with assert True
