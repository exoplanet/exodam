"""Function to get exodict file content."""

# Standard imports
from typing import Any, cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import SQLEnumerable

# First party imports
from exodam_utils import conn_manager, get_refereed_id

# Local imports
from ..utils import ExodictName, ExodictType


def get_exodict(
    exodict_type: ExodictType,
    conn: Connection | None = None,
    name: ExodictName | None = None,
) -> dict[str, Any]:
    """
    Get content of one or all exodict resources.

    If we give a name we search in the db the corresponding resource,
    otherwise we get all resources which are of reference.

    Args:
        exodict_type: Type of exodict to get.
        conn: Connection to a db. Used for tests.
        name: Name of the resource we want.
            By default, None to obtain all reference resources.

    Returns:
        Given name or all exodict resources.

    Raises:
        ValueError: If `name` is of the wrong type or is not equal to None.
        PyLinqSQLError: If anything goes wrong with the database connection.
    """
    if name is not None and not isinstance(name, ExodictName):
        raise ValueError(
            f"{name} is not a valid exodict name."
            " Valid value for `name`: "
            f"{ExodictName.as_str_list()} or None.",
        )

    record: Enumerable | None = None

    tables = exodict_type.value
    with conn_manager(conn) as connection:
        ref_exodict_id = get_refereed_id(connection, tables.exodict)

        sqle_all_exodict_rsc = SQLEnumerable(connection, tables.dict2data.name).join(
            inner=SQLEnumerable(connection, tables.data.name),
            outer_key=lambda exodcit2data: exodcit2data.id_data,
            inner_key=lambda exodict_data: exodict_data.id,
            # Type ignore because:
            # https://github.com/python/mypy/issues/15459
            result_function=(
                lambda exodict_data, exodict2data: {  # type: ignore[misc, arg-type]
                    "name": exodict2data.name,
                    "data": exodict_data.data,
                }
            ),
        )

        # TODO: trouver un meilleur moyen de faire
        match exodict_type:
            case ExodictType.SCIENTIFIC:
                sqle_all_exodict_rsc.where(
                    lambda x: x.exodict2data.id_exodict == ref_exodict_id
                )
            case ExodictType.EDITORIAL:
                sqle_all_exodict_rsc.where(
                    lambda x: x.exodict_editorial2data.id_exodict == ref_exodict_id
                )

        if name is None:
            record = cast(Enumerable, sqle_all_exodict_rsc.execute())
            result = {}
            for exodict in record.to_list():
                result.update(exodict.data)
            return result

        # TODO: trouver un meilleur moyen de faire
        match exodict_type:
            case ExodictType.SCIENTIFIC:
                fquery_filter_on_name = (  # noqa: E731
                    lambda x: x.exodict2data.name == cast(ExodictName, name).value
                )
            case ExodictType.EDITORIAL:
                fquery_filter_on_name = (  # noqa: E731
                    lambda x: x.exodict_editorial2data.name
                    == cast(ExodictName, name).value
                )

        record = cast(
            Enumerable,
            sqle_all_exodict_rsc.where(
                fquery_filter_on_name  # type: ignore[arg-type, return-value]
            ).execute(),
        )

        return cast(dict[str, Any], record.to_list()[0].data)


# TODO: Get the version in the exodict
def get_exodict_version() -> str:
    """
    Get the exodict version.

    For the moement just return a string.

    Returns:
        The exodict version.

    Examples:
        >>> get_exodict_version()
        '1.0.0-beta'
    """
    return "1.0.0-beta"
