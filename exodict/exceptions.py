"""Exceptions for the exodict module."""

# First party imports
from exodam_utils import ExodamError


class ExodictError(ExodamError):
    """Base Exception for the Exodict module."""


class LabelError(ExodictError):
    """Initialize a LabelError exception."""


class MissingMandatoryKeyError(ExodictError):
    """Initialize a MissingMandatoryKeyError exception."""


class MandatoryFieldError(ExodictError):
    """Exception raised when a mandatory field is none."""

    def __init__(self, name: str, expected: list[str]):
        """Initialize a MandatoryFieldError."""
        super().__init__(f"The field {name} is mandatory. Expected values: {expected}")
