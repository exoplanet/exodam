"""All useful function to check labels in exodict."""

# Standard imports
import re
from typing import Any

# Local imports
from ..exceptions import LabelError

# Useful to validate labels.full_ascii. We need to match a header style to be compatible
# with csv and most fairly unstandardized files.
_HEADER_NAME_PATTERN = re.compile(r"^[_a-zA-Z][0-9a-zA-Z_]*$")


def _ismathml(label: str) -> bool:
    """
    Check if label contains mathml.

    Args:
        label: string to check.

    Returns:
        True if label is mathml, False otherwise.

    Examples:
        >>> _ismathml("toto")
        False

        >>> _ismathml("<math>toto")
        False

        >>> _ismathml("toto</math>")
        False

        >>> _ismathml("<math>toto</math>")
        True
    """
    return label.startswith("<math>") and label.endswith("</math>")


def _is_valid_label(  # noqa: PLR0911
    path: str,
    labels: dict[str, str],
) -> tuple[bool, str]:
    """
    Check if the given labels is valid.

    Args:
        path: Path of the labels we check.
        labels: Labels we want to check.

    Returns:
        True and an empty message or False and an error message.
    """
    short = labels.get("short", None)
    long = labels.get("long", None)
    full_ascii = labels.get("full_ascii", None)

    if long is None:
        return (False, f"{path}: The 'long' label is mandatory")
    if not isinstance(long, str):
        return (False, f"{path}: The 'long' label must be a string")

    if full_ascii is None:
        return (False, f"{path}: The 'full_ascii' label is mandatory")
    if not isinstance(full_ascii, str):
        return (False, f"{path}: The 'full_ascii' label must be a string")
    if not full_ascii.isascii():
        return (False, f"{path}: The 'full_ascii' label must be write in ASCII")
    if not _HEADER_NAME_PATTERN.match(full_ascii):
        return (
            False,
            f"{path}: The 'full_ascii' label must be a valid csv header (begin with "
            "underscore or all letter and contains only letter, underscore and digits)",
        )

    if short is not None:
        if not isinstance(short, str):
            return (False, f"{path}: The 'short' label must be a string")
        if not _ismathml(short):
            if len(short) >= len(long):
                return (
                    False,
                    f"{path}: The 'short' label must be shorter of the 'long' label",
                )

    return (True, "")


def _get_key_in_exodict(
    search_key: str,
    exodict: dict[str, dict[str, Any]],
    path: str = "",
) -> list[tuple[str, dict[str, str]]]:
    """
    Get all `search_key` and path dict in an exodict.

    Args:
        search_key: Key to found.
        exodict: Dict in which we seek.
        path: Parameter to save the path of the search dict.

    Returns:
        All value of dict with the key `search key` and path of these dictionary.

    Examples:
        >>> exodict = {"toto" : {"a": 1, "b": {"c":3}}, "titi": {"c": 4}}
        >>> _get_key_in_exodict("c", exodict)
        [('toto 👉 b', 3), ('titi', 4)]
    """
    result = []

    for key, value in exodict.items():
        if key == search_key:
            result.append((path, value))
            continue

        if isinstance(value, dict):
            new_path = f"{path} 👉 {key}" if path else key
            result.extend(_get_key_in_exodict(search_key, value, new_path))

    return result


def check_labels(exodict: dict[str, dict[str, Any]]) -> None:
    """
    Check all label in an exodict.

    Args:
        exodict: Dict in which we check labels.

    Raises:
        LabelError: If at least one label isn't valid.
    """
    labels_w_paths = _get_key_in_exodict("label", exodict)
    result, warning_message = [], []
    for path, label in labels_w_paths:
        res, msg = _is_valid_label(path, label)
        result.append(res)
        if not res:
            warning_message.append(msg)

    if not all(result):
        print("\n".join(warning_message))
        raise LabelError("\n".join(warning_message))
