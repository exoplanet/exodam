"""Generate an exodict with given dict and/or default exocdict."""

# Standard imports
import json
from pathlib import Path
from typing import Any

# Third party imports
from psycopg import Connection

# Local imports
from ..exceptions import MissingMandatoryKeyError
from ..get import get_exodict
from ..utils import (
    EditorialGenerateParams,
    ExodictType,
    ScientificGenerateParams,
    merge_exodict,
)
from .check_labels import check_labels
from .generate_exodict_validation import get_key_list, get_old_exodict_check_list
from .insert_in_db import insert_complex_data


def _duplicate_keys(
    result: dict[str, dict[str, Any]],
    content_to_check: dict[str, dict[str, Any]],
) -> list[str] | None:
    """
    Get all duplicate key. All key already in the result and in the content.

    When we build the exodict we check if we have a key in the file that we will add to
    the final dict and which is already in the final dict.
    If we found a key we return it otherwise we return None.

    Args:
        result: The final dict that we are building.
        content: Dict we will add to result.

    Returns:
        Keys in the content dict and already in the result dict or None.
    """
    result_keys = result.keys()
    content_keys = content_to_check.keys()
    if any(key in result_keys for key in content_keys):
        return list(set(result_keys) & set(content_keys))
    return None


def _get_missing_keys(
    conn: Connection | None,
    exodict: dict[str, Any],
    exodict_type: ExodictType,
) -> list[str]:
    """
    Get missing mandatory keys in an exodict.

    Args:
        conn: Connection to a db. Use in test.
        exodict: Exodict to check.
        exodict_type: Exodict type.

    Returns:
        All missing keys, if no key missing, we return an emptyt list.
    """
    check_list = get_old_exodict_check_list(conn, exodict_type)
    exodict_keys = get_key_list(exodict)

    for key in exodict_keys:
        if key in check_list:
            check_list.remove(key)

    return check_list


def _get_exodict_resource_from_db(
    conn: Connection | None, resource: str, exodict_type: ExodictType
) -> dict[str, Any]:
    """
    Get an exodict resource form db.

    Args:
        conn: Connection to a db. Use in test.
        resource: Resource we want to recover.
        exodict_type: Exodict type.

    Returns:
        Resource recover.
    """
    return get_exodict(
        exodict_type,
        conn,
        exodict_type.get_exodict_name().from_str(resource),
    )


def _generate_exodict(
    conn: Connection | None,
    given_ressources: dict[str, dict[str, Any] | None],
    exodict_type: ExodictType,
) -> dict[str, Any]:
    """
    Generate exodict from referent exodict resources and given resources.

    Args:
        conn: Connection to a db. Use in test.
        given_ressources: New given resources.
        exodict_type: Type of exodict we want generate.

    Returns:
        Generated exodict from db and given file(s).
    """
    from_file = {
        key: value for key, value in given_ressources.items() if value is not None
    }
    from_db = [key for key, value in given_ressources.items() if value is None]

    check: dict[str, Any] = {}
    exodict = {}
    for resource in from_db:
        content = _get_exodict_resource_from_db(conn, resource, exodict_type)

        duplicate_keys = _duplicate_keys(check, content)
        # No cover because its just a security
        if duplicate_keys is not None:
            raise KeyError(f"Duplicate key {duplicate_keys} found")  # pragma: no cover

        check.update(content)
        exodict[resource] = content

    for name, content in from_file.items():
        duplicate_keys = _duplicate_keys(check, content)
        if duplicate_keys is not None:
            raise KeyError(f"Duplicate key {duplicate_keys} found")

        check.update(content)
        exodict[name] = content

    missing_keys = _get_missing_keys(conn, check, exodict_type)

    if missing_keys:
        raise MissingMandatoryKeyError(f"Missing key in exodict: {missing_keys}")

    check_labels(check)

    return exodict


def generate_exodict_and_write_in_file(
    export_path: Path,
    params: EditorialGenerateParams | ScientificGenerateParams,
    conn: Connection | None = None,
) -> None:
    """
    Generete editorial exodict and write it in the given path file.

    Args:
        export_path: Path for generated exodict. Need to be a json file.
        params: New parameters to generate exodict.
        conn: Connection to a db. Use in test.
    """
    exodict_type = (
        ExodictType.EDITORIAL
        if isinstance(params, EditorialGenerateParams)
        else ExodictType.SCIENTIFIC
    )
    exodict_elements = _generate_exodict(
        conn,
        params.as_dict(),
        exodict_type,
    )
    with open(export_path, "w", encoding="utf-8") as exodict_file:
        exodict_file.write(json.dumps(merge_exodict(exodict_elements)))


def generate_and_insert_exodict(
    params: EditorialGenerateParams | ScientificGenerateParams,
    exodict_type: ExodictType,
    conn: Connection | None = None,
) -> None:
    """
    Generete editorial exodict and insert it in the db.

    This generate exodict are new referents so we remove this status to
    other cerberus in the database.

    Args:
        params: New parameters to generate exodict.
        exodict_type: Type of exodict we want generate.
        conn: Connection to a db. Use in test.
    """
    exodict_elements = _generate_exodict(conn, params.as_dict(), exodict_type)

    exodict_info = [
        {"name": exodict_type.get_exodict_name().from_str(key).value, "data": value}
        for key, value in exodict_elements.items()
    ]

    tables = exodict_type.value
    insert_complex_data(
        conn,
        exodict_info,
        tables.exodict,
        tables.dict2data,
        tables.data,
    )
