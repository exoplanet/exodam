"""All function about mandatory keys in an exodict."""

# Standard imports
from collections.abc import Callable
from typing import Any

# Third party imports
from psycopg import Connection

# Local imports
from ..get import get_exodict
from ..utils import ExodictType


def get_key_list(
    exodict: dict[str, Any],
    condition: Callable = lambda x: False,
) -> list[str]:
    """
    Get recursively list of keys in a exodict.

    Args:
        exodict: dictionary from which we want to extract the keys.
        condition: Lambda function to give more conditions.

    Returns:
        All keys in a dictionary who fulfills the conditions.
    """
    if not isinstance(exodict, dict) or condition(exodict):
        return []

    keys = list(exodict.keys())
    result = []

    for key in keys:
        result.append(key)
        result.extend(get_key_list(exodict[key], condition))

    return result


def get_old_exodict_check_list(
    conn: Connection | None,
    exodict_type: ExodictType,
) -> list[str]:
    """
    Get a check list for exodict.

    Args:
        conn: Connection to a db. Use in test.
        exodict_type: Exodict type.

    Returns:
        The check list.
    """
    old_exodict = get_exodict(exodict_type, conn)
    return get_key_list(
        old_exodict,
        lambda x: "definition" in x.keys() or "wiki_data_rdf" in x.keys(),
    )
