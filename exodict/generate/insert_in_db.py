"""Useful function to insert an complex data into db."""

# Standard imports
from typing import Any, TypeAlias, cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import SQLEnumerable

# First party imports
from exodam_utils import Table, conn_manager, get_refereed_id

exodict_info_type: TypeAlias = list[dict[str, str | dict[str, Any]]]


def _insert_exodict_data(
    conn: Connection,
    table: Table,
    data: dict[str, Any],
) -> int:
    """
    Insert datat into a db.

    Args:
        conn: Connection to the database.
        table: Table to insert.
        data: Data to insert.

    Returns:
        Id of the inserted element.
    """
    SQLEnumerable(conn, table.name).insert(table.insert_columns, (data)).execute()

    record: Enumerable = (
        SQLEnumerable(conn, table.name)
        .select(lambda x: x.id)
        .where(lambda x: x.data == data)
        .last()
        .execute()
    )

    return cast(int, record.to_list()[0].id)


def _insert_all_exodict_data(
    conn: Connection,
    table: Table,
    all_info: exodict_info_type,
) -> dict[str, int]:
    """
    Insert all data into a db.

    Args:
        conn: Connection to the database.
        table: Table to insert.
        all_info: Information on data to insert.

    Returns:
        Ids of inserted elements.
    """
    return {
        cast(str, info["name"]): _insert_exodict_data(
            conn,
            table,
            cast(dict[str, Any], info["data"]),
        )
        for info in all_info
    }


def _insert_all_exodict2data(
    conn: Connection,
    table: Table,
    all_info: exodict_info_type,
    ref_id: int,
    data_ids: dict[str, int],
) -> None:
    """
    Insert 2data into a db.

    Args:
        conn: Connection to the database.
        table: Table to insert.
        all_info: Information on data to insert.
        ref_id: Id of the reference element in the database.
        data_ids: Ids of data in the database.
    """
    elements_to_insert = [
        (ref_id, info["name"], data_ids[cast(str, info["name"])]) for info in all_info
    ]

    SQLEnumerable(conn, table.name).insert(
        table.insert_columns,
        elements_to_insert,  # type: ignore[arg-type]
    ).execute()


def insert_complex_data(
    conn: Connection | None,
    data_info: exodict_info_type,
    main_table: Table,
    to_data_table: Table,
    data_table: Table,
) -> None:
    """
    Insert complex data in database.

    Args:
        conn: Connection to the database.
        data_info: Info of data to insert.
        main_table: Main table.
        to_data_table: 2data table.
        data_table: Data table.
    """
    with conn_manager(conn) as connection:
        # Update old refereed to remove his referent status

        # TODO: remove the type ignore and the noqa E501 when this issue is resolve
        # https://gitlab.obspm.fr/exoplanet/py-linq-sql/-/issues/41
        SQLEnumerable(connection, main_table.name).update(
            lambda x: x.refereed == False  # type: ignore[arg-type, return-value] # noqa: E712 E501
        ).where(
            lambda x: x.refereed == True  # noqa: E712
        ).execute()

        # Insert a new refereed

        # TODO: REMOVE the type ignore when this issue will be fixed.
        # https://gitlab.obspm.fr/exoplanet/py-linq-sql/-/issues/46
        SQLEnumerable(connection, main_table.name).simple_insert(
            refereed=True  # type: ignore[arg-type]
        ).execute()

        exodict_ref_id = get_refereed_id(connection, main_table)
        data_ids = _insert_all_exodict_data(
            connection,
            data_table,
            data_info,
        )
        _insert_all_exodict2data(
            connection,
            to_data_table,
            data_info,
            exodict_ref_id,
            data_ids,
        )
