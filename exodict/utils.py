"""Useful stuff for exodict module."""

# Future imports
from __future__ import annotations

# Standard imports
from dataclasses import dataclass, fields
from enum import Enum
from typing import Any

# Third party imports
from dotmap import DotMap

# First party imports
from exodam_utils import ExodamSqlTables

# Local imports
from .exceptions import MandatoryFieldError


class ExodictName(Enum):
    """Enum type for exodict name resource."""

    @classmethod
    def as_str_list(cls: type[ExodictName]) -> list[str]:
        """Return a list of all values."""
        return [element.value for element in list(cls)]  # type: ignore[var-annotated]

    @classmethod
    def from_str(
        cls: type[ExodictName],
        string: str,
    ) -> ExodictName:
        """
        Return the element the enum cls corresponding to the string.

        Args:
            string: string corresponding to an element of the enum cls.

        Returns:
            An element of the enum cls.

        Raises:
            MandatoryFieldError: Raised when given `string` is None.
            TypeError: Raised if given `string` isn't of str type.
        """
        if not string:
            raise MandatoryFieldError(cls.__name__, cls.as_str_list())

        if not isinstance(string, str):
            raise TypeError(f"Str is expected. All values: {cls.as_str_list()}")

        for choice in cls:
            if string.lower() == choice.value.lower():
                return choice

        raise ValueError(f"Given value {string} is not an element of {cls}.")


class ExodictScientificName(ExodictName):
    """Enum type for exodict scientific name resource."""

    EXO_TYPE = "exo_type"
    IDENTIFICATION = "identification"
    INTERNAL = "internal"
    PARAMETERS = "parameters"
    REFERENCE = "reference"
    RELATIONSHIP = "relationship"


class ExodictEditorialName(ExodictName):
    """Enum type for exodict editorial name resource."""

    NEWS = "news"


class ExodictType(Enum):
    """Enum type for exodict type."""

    SCIENTIFIC = DotMap(
        {
            "exodict": ExodamSqlTables.exodict,
            "data": ExodamSqlTables.exodict_data,
            "dict2data": ExodamSqlTables.exodict2data,
            "skel": ExodamSqlTables.exodam_skel,
            "known_keys": ExodamSqlTables.exodam_known_keys,
            "cerberus": ExodamSqlTables.exodam_cerberus,
            "cerberus2data": ExodamSqlTables.exodam_cerberus2data,
            "cerberus_data": ExodamSqlTables.exodam_cerberus_data,
        }
    )

    EDITORIAL = DotMap(
        {
            "exodict": ExodamSqlTables.exodict_editorial,
            "data": ExodamSqlTables.exodict_editorial_data,
            "dict2data": ExodamSqlTables.exodict_editorial2data,
            "skel": ExodamSqlTables.exodam_editorial_skel,
            "known_keys": ExodamSqlTables.exodam_editorial_known_keys,
            "cerberus": ExodamSqlTables.exodam_editorial_cerberus,
            "cerberus2data": ExodamSqlTables.exodam_editorial_cerberus2data,
            "cerberus_data": ExodamSqlTables.exodam_editorial_cerberus_data,
        }
    )

    def get_exodict_name(
        self,
    ) -> type[ExodictName]:
        """
        Get corresponding ExodictName for a type.

        Returns:
            Corresponding ExodictType for an element of this Enum.
        """
        return (
            ExodictEditorialName
            if self == ExodictType.EDITORIAL
            else ExodictScientificName
        )


@dataclass
class GenerateParams:
    """GenerateParams class to manage parameters of exodict generation."""

    def as_dict(self) -> dict:
        """
        Get element of the class as a dictionary.

        Returns:
            Element of the class as a dictionary.
        """
        return {field.name: getattr(self, field.name) for field in fields(self)}


# TODO: Test it
@dataclass
class ScientificGenerateParams(GenerateParams):
    """Class to manage parameters of scientific exodict generation."""

    exo_type: dict[str, Any] | None = None
    "`exo_type` new resource. None for no new resource."

    identification: dict[str, Any] | None = None
    "`identification` new resource. None for no new resource."

    internal: dict[str, Any] | None = None
    "`internal` new resource. None for no new resource."

    parameters: dict[str, Any] | None = None
    "`parameters` new resource. None for no new resource."

    reference: dict[str, Any] | None = None
    "`reference` new resource. None for no new resource."

    relationship: dict[str, Any] | None = None
    "`relationship` new resource. None for no new resource."


# TODO: Test it
@dataclass
class EditorialGenerateParams(GenerateParams):
    """Class to manage parameters of editorial exodict generation."""

    news: dict[str, Any] | None = None
    "`news` new resource. None for no new resource."


def merge_exodict(expected_data: dict[str, dict[str, Any]]) -> dict[str, Any]:
    """
    Get expected data for whole exodict.

    Args:
        expected_data: Expected_data to merge.

    Returns:
        Expected data for a whole exodcit.

    Examples:
        >>> merge_exodict({"a": {"aa": 1}, "b": {"bb": 2}, "c": {"cc": 3}})
        {'aa': 1, 'bb': 2, 'cc': 3}
    """
    result = {}
    for value in expected_data.values():
        result.update(value)
    return result
