# The runner of the projects is a temporary runner embedded in Ulysse laptop.
# We have to switch to one of the docker runners of the observatory or any obs hosted runner.

# If the runner shut down we can make an other on the gitlab.
# Documentation: https://gitlab.obspm.fr/exoplanet/py-linq-sql/-/settings/ci_cd in the section "specific runner"

# If your runner fail with the error:

# ERROR: Job failed: prepare environment: Process exited with status 1.
# Check https://docs.gitlab.com/runner/shells/index.html#shell-profile-loading
# for more information"

# You need to delete '.bash_logout' following files if exist: "/home/gitlab-runner/" and "/home/<usernames>/"
# sudo rm /home/gitlab-runner/.bash_logout
# sudo rm /home/<username>/.bash_logout

image: "gitlab.obspm.fr:4567/exoplanet/docker-exo:latest"

stages:
  - install_poetry
  - install_dependencies
  - pre-commit
  - cov_tests

install_poetry:
  cache: []
  stage: install_poetry
  script:
    - lsb_release -d
    - echo "Installing poetry"
    - which poetry | grep -o poetry > /dev/null &&  curl -sSL https://install.python-poetry.org | python3 - --uninstall || echo "poetry not installed"
    - curl -sSL https://install.python-poetry.org | python3 -
    - poetry --version
    - echo "Poetry is installed"

# ---------------
# | PYTHON 3.10 |
# ---------------

install_dependencies_py310:
  cache: []
  needs: [install_poetry]
  stage: install_dependencies
  script:
    - export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring
    - poetry env use python3.10
    - echo "Installing dependencies with poetry"
    - poetry install
    - echo "Installing pre-commit"
    - poetry run pre-commit clean
    - poetry run pre-commit install --hook-type pre-merge-commit
    - poetry run pre-commit install --hook-type pre-push
    - poetry run pre-commit install --hook-type post-rewrite
    - poetry run pre-commit install-hooks
    - poetry run pre-commit install
    - echo "End of installations"

py310_pre-commit_hooks:
  stage: pre-commit
  cache: []
  needs: [install_poetry, install_dependencies_py310]
  script:
    - echo "Running pre-commit 'pre-commit hooks'"
    - poetry run pre-commit run no-commit-to-branch --all-files
    - poetry run pre-commit run check-merge-conflict --all-files
    - poetry run pre-commit run debug-statements --all-files
    - poetry run pre-commit run trailing-whitespace --all-files
    - poetry run pre-commit run end-of-file-fixer --all-files
    - poetry run pre-commit run check-yaml --all-files
    - poetry run pre-commit run check-toml --all-files
    - poetry run pre-commit run check-shebang-scripts-are-executable --all-files
    - echo "'pre-commit hooks' run finished"
  dependencies:
    - install_dependencies_py310

py310_formater:
  stage: pre-commit
  cache: []
  needs: [install_poetry, install_dependencies_py310]
  script:
    - echo "Running formater"
    - poetry run pre-commit run isort --all-files
    - poetry run pre-commit run black --all-files
    - poetry run pre-commit run codespell --all-files
    - echo "Formater run finished"
  dependencies:
    - install_dependencies_py310

py310_linters:
  stage: pre-commit
  cache: []
  needs: [install_poetry, install_dependencies_py310]
  script:
    - echo "Running linters"
    - poetry run pre-commit run sqlfluff-lint --all-files
    - poetry run pre-commit run mypy_exodam_lib --all-files
    - poetry run pre-commit run mypy_resources --all-files
    - poetry run pre-commit run mypy_utils --all-files
    - poetry run pre-commit run mypy_exodict --all-files
    - poetry run pre-commit run ruff --all-files
    - poetry run pre-commit run markdownlint-cli2 --all-files
    - echo "Linters run finished"
  dependencies:
    - install_dependencies_py310

py310_tests_for_pre-commit:
  stage: pre-commit
  cache: []
  needs: [install_poetry, install_dependencies_py310]
  script:
    - echo "Running tests"
    - poetry run pre-commit run doctest_md --all-files
    - poetry run pytest --junitxml=testsunit_py310.xml
    - echo "Tests run finished"
  dependencies:
    - install_dependencies_py310
  artifacts:
    expire_in: never
    when: always
    paths:
      - testsunit_py310.xml
    reports:
      junit: testsunit_py310.xml

# ---------------
# | PYTHON 3.11 |
# ---------------

install_dependencies_py311:
  cache: []
  needs: [install_poetry]
  stage: install_dependencies
  script:
    - export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring
    - poetry env use python3.11
    - echo "Installing dependencies with poetry"
    - poetry install
    - echo "Installing pre-commit"
    - poetry run pre-commit clean
    - poetry run pre-commit install --hook-type pre-merge-commit
    - poetry run pre-commit install --hook-type pre-push
    - poetry run pre-commit install --hook-type post-rewrite
    - poetry run pre-commit install-hooks
    - poetry run pre-commit install
    - echo "End of installations"

py311_pre-commit_hooks:
  stage: pre-commit
  cache: []
  needs: [install_poetry, install_dependencies_py311]
  script:
    - echo "Running pre-commit 'pre-commit hooks'"
    - poetry run pre-commit run no-commit-to-branch --all-files
    - poetry run pre-commit run check-merge-conflict --all-files
    - poetry run pre-commit run debug-statements --all-files
    - poetry run pre-commit run trailing-whitespace --all-files
    - poetry run pre-commit run end-of-file-fixer --all-files
    - poetry run pre-commit run check-yaml --all-files
    - poetry run pre-commit run check-toml --all-files
    - poetry run pre-commit run check-shebang-scripts-are-executable --all-files
    - echo "'pre-commit hooks' run finished"
  dependencies:
    - install_dependencies_py311

py311_formater:
  stage: pre-commit
  cache: []
  needs: [install_poetry, install_dependencies_py311]
  script:
    - echo "Running formater"
    - poetry run pre-commit run isort --all-files
    - poetry run pre-commit run black --all-files
    - poetry run pre-commit run ruff --all-files
    - poetry run pre-commit run codespell --all-files
    - echo "Formater run finished"
  dependencies:
    - install_dependencies_py311

py311_linters:
  stage: pre-commit
  cache: []
  needs: [install_poetry, install_dependencies_py311]
  script:
    - echo "Running linters"
    - poetry run pre-commit run sqlfluff-lint --all-files
    - poetry run pre-commit run mypy_exodam_lib --all-files
    - poetry run pre-commit run mypy_resources --all-files
    - poetry run pre-commit run mypy_utils --all-files
    - poetry run pre-commit run mypy_exodict --all-files
    - poetry run pre-commit run markdownlint-cli2 --all-files
    - echo "Linters run finished"
  dependencies:
    - install_dependencies_py311

py311_tests_for_pre-commit:
  stage: pre-commit
  cache: []
  needs: [install_poetry, install_dependencies_py311]
  script:
    - echo "Running tests"
    - poetry run pre-commit run doctest_md --all-files
    - poetry run pytest --junitxml=testsunit_py311.xml
    - echo "Tests run finished"
  dependencies:
    - install_dependencies_py311
  artifacts:
    expire_in: never
    when: always
    paths:
      - testsunit_py311.xml
    reports:
      junit: testsunit_py311.xml

# ------------
# | COVERAGE |
# ------------

tests_for_coverage:
  stage: cov_tests
  cache: []
  needs: [install_poetry, install_dependencies_py311]
  before_script:
    - echo "Cleaning temporary files and folders"
    - find . \( -name __pycache__ -o -name "*.pyc" \) -delete
  script:
    - poetry run coverage run -m pytest
    - poetry run coverage report
    - poetry run coverage xml
  after_script:
    - echo "Cleaning temporary files and folders"
    - find . \( -name __pycache__ -o -name "*.pyc" \) -delete
  dependencies:
    - install_dependencies_py311
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    expire_in: never
    when: always
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
