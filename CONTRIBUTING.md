# Contributing to EXODAM (Exoplanetary Data Model)

**First thanks for contributing.**

The following explains how to contribute to the project.

## Table Of Contents

- [Contributing to EXODAM (Exoplanetary Data Model)](#contributing-to-exodam-exoplanetary-data-model)
  - [Table Of Contents](#table-of-contents)
  - [What do you need to install exodam on your computer](#what-do-you-need-to-install-exodam-on-your-computer)
    - [What version of python](#what-version-of-python)
    - [Dependencies](#dependencies)
      - [Install procedure for the dependencies](#install-procedure-for-the-dependencies)
      - [Install procedure for development dependencies](#install-procedure-for-development-dependencies)
  - [Project structure](#project-structure)
    - [Structure of Exodam](#structure-of-exodam)
  - [How to](#how-to)
    - [How to use tests](#how-to-use-tests)
    - [How to report a bug](#how-to-report-a-bug)
    - [How to submit changes or enhancements](#how-to-submit-changes-or-enhancements)
    - [How to Merge Request](#how-to-merge-request)
  - [Pre-commit](#pre-commit)
  - [Style Guide](#style-guide)
    - [Formatter](#formatter)
      - [Black](#black)
      - [Isort](#isort)
    - [Linter](#linter)
      - [Pydocstyle](#pydocstyle)
      - [Mypy](#mypy)
      - [Pylint](#pylint)
      - [Flake8](#flake8)
      - [Flakehell](#flakehell)
  - [Contacts](#contacts)

## What do you need to install exodam on your computer

### What version of python

Exodam use the python version 3.10.0. To install this version use :

```bash
sudo apt install python3.10
```

Verify the version.

```bash
$ python3 --version
Python 3.10.0
```

### Dependencies

Exodam use some dependencies :

- [pyyaml](https://pyyaml.org/wiki/PyYAMLDocumentation)
- [types-PyYaml](https://pyyaml.org/wiki/PyYAMLDocumentation)
- [cerberus](https://docs.python-cerberus.org/en/stable/index.html#)
- [click](https://click.palletsprojects.com/en/8.0.x/)
- [arrow](https://arrow.readthedocs.io/en/latest/#)
- [flatten-dict](https://github.com/ianlini/flatten-dict)
- [rich](https://rich.readthedocs.io/en/stable/index.html)
- [typer](https://github.com/tiangolo/typer)
- [dotmap](https://github.com/drgrib/dotmap)

and some dependencies for the development :

- [pytest](https://docs.pytest.org/en/6.2.x/contents.html)
- [pytest-sugar](https://github.com/Teemu/pytest-sugar/)
- [pytest-pudb](https://github.com/wronglink/pytest-pudb)
- [pytest-bdd](https://github.com/pytest-dev/pytest-bdd)
- [pytest-cov](https://github.com/pytest-dev/pytest-cov)
- [xdoctest](https://xdoctest.readthedocs.io/en/latest/autoapi/xdoctest/index.html)
- [assertpy](https://github.com/assertpy/assertpy)
- [pre-commit](https://pre-commit.com)
- [black](https://github.com/psf/black)
- [isort](https://pycqa.github.io/isort/)
- [pydocstyle](http://www.pydocstyle.org/en/stable/)
- [mypy](https://mypy.readthedocs.io/en/stable/)
- [pylint](https://pylint.pycqa.org/en/latest/)
- [plerr](https://github.com/vald-phoenix/pylint-errors)
- [flake8]([flake-9](https://github.com/pycqa/flake8))
- [flakehell](https://flakehell.readthedocs.io/)
- [flake8-bandit](https://github.com/tylerwince/flake8-bandit)
- [flake8-commas](https://github.com/PyCQA/flake8-commas/)
- [flake8-eradicate](https://github.com/wemake-services/flake8-eradicate)
- [dlint](https://github.com/dlint-py/dlint)
- [codespell](https://github.com/codespell-project/codespell/)
- [sphinx](https://www.sphinx-doc.org/en/master/)
- [sphinx-rtd-theme](https://sphinx-themes.org/sample-sites/sphinx-rtd-theme/)
- [myst_parser](https://myst-parser.readthedocs.io/en/latest/)

#### Install procedure for the dependencies

You only need the classic dependencies to use exodam, they are all available on Pypi.

*To install the dependencies you need [poetry](https://python-poetry.org/) and [just](https://github.com/casey/just)*

```bash
just install
```

Or you can directly use poetry:

```bash
poetry install --no-dev --remove-untracked
```

#### Install procedure for development dependencies

You need to install all this package to contribute to the project,
they are all available on Pypi.

*To install the dependencies you need [poetry](https://python-poetry.org/) and [just](https://github.com/casey/just)*

```bash
just install-all
```

Or you can directly use poetry (and npm for markdownlint)

```bash
poetry install --remove-untracked
sudo npm install
sudo npm install markdownlint-cli2 --global
```

## Project structure

Exodam is structured with 2 principal folders and one file;

- exodam.py
- srcs
- tests

*exodam* contains the code of the terminal 'interface' to launch the program with some
commands.

*srcs* contains all the code of the program separate into different folders to
find your way around the code.

*tests* contains all tests for the program (more than 200).

### Structure of Exodam

![Struct](./contribute/img/struct.svg)

## How to

### How to use tests

To launch test you can use the alias just:

```bash
just pytest
```

If all goes well you should have something like this:
![Tests](./contribute/img/tests.png)

### How to report a bug

This section guides you through submitting a bug report for Exodam.
Before reporting check the [issues](https://gitlab.obspm.fr/exoplanet/exodam/-/issues) and
[CHANGELOG.md](CHANGELOG.md) as you might find out that you don't need to create one.
If there is no trace of this bug create a issue [here](https://gitlab.obspm.fr/exoplanet/exodam/-/issues)
following the [template](./contribute/templates/issues/bug_report.md),
**please include as many details as possible**.

### How to submit changes or enhancements

This section guides you through submitting changes or enhancements for Exodam.
Before proposing changes or enhancements check the [issues](https://gitlab.obspm.fr/exoplanet/exodam/-/issues)
and [CHANGELOG.md](CHANGELOG.md) as you might find out that you don't need to create one.
If the enhancement or change has never been proposed create a issue [here](https://gitlab.obspm.fr/exoplanet/exodam/-/issues)
following the [template](./contribute/templates/issues/feature.md),
**please include as many details as possible**.

### How to Merge Request

This section guides you to summit a merge request for Exodam.
Before create a merge request make sure you respond to an [issue](https://gitlab.obspm.fr/exoplanet/exodam/-/issues)
or have opened one.
Please follow these steps to have your contribution considered by the maintainers:

1. Follow all instructions in the [template](./contribute/templates/merge_request/merge_request.md)
2. Follow the [styleguides](#style-guide)

## Pre-commit

Exodam use pre-commit before commit and merge.
Pre-commit is a git hook scripts are useful for identifying simple issues before
submission to code review. You must install pre-commit before commit on the git.

```bash
just preinstall
```

## Style Guide

Exodam use some formatter and linter to improve understanding of the code by other developers.

### Formatter

You can run all formatter on all code and test files with:

```bash
just onmy31
```

#### Black

Black is a formatter to save time and mental energy for more important matters.
Black is run by pre-commit but you can run it manually.

```bash
black #filename
```

[black documentation](https://github.com/psf/black)

#### Isort

Isort is a formatter to sort imports alphabetically,
and automatically separated into sections and by type.
Isort is run by pre-commit but you can run it manually.

```bash
isort #filename
```

[isort documentation](https://pycqa.github.io/isort/)

### Linter

You can run all linter on code with:

```bash
just lint
```

#### Pydocstyle

Pydocstyle is a linter for checking compliance with Python docstring conventions.
Pydocstyle is run by pre-commit but you can run it manually.

```bash
just pydocstyle #path
```

or with just on all code with:

```bash
just pydocstyle
```

[Pydocstyle documentation](http://www.pydocstyle.org/en/stable/)

#### Mypy

Mypy is a static type checker for Python.
Mypy is run by pre-commit but you can run it manually.

```bash
just mypy
```

[Mypy documentation](https://mypy.readthedocs.io/en/stable/)

#### Pylint

Pylint is a static code analyse for Python.
Pylint is run by pre-commit but you can run it manually.

```bash
just pylint #path
```

or with just on all code with:

```bash
just pylint
```

[Pylint documentation](https://pylint.pycqa.org/en/latest/index.html)

For more information on pylint errors you can use [plerr](https://github.com/vald-phoenix/pylint-errors)
with error code.

```bash
plerr W013
```

#### Flake8

Flake8 us a wrapper of PyFlakes, pycodestule and 'Ned Batchelder’s McCabe script'.
Flake8 is run by pre-commit.

[Flake8 documentation](https://flake8.pycqa.org/en/latest/index.html#)

#### Flakehell

Flakehell is an evolved version of flake8.
Flakehell is run by pre-commit but you can run it manually.

```bash
just flakehell #path
```

or with just on all code with:

```bash
just flakhell
```

[Flakehell documentation](https://flakehell.readthedocs.io/)

## Contacts

- Author : Ulysse CHOSSON (LESIA)
- Maintainer : Ulysse CHOSSON (LESIA)
- Email : <ulysse.chosson@obspm.fr>
- Contributors :
  - Quentin KRAL (LESIA)
  - Pierre-Yves MARTIN (LESIA)
