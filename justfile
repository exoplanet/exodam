# Remove __pycache__ and .pyc files and folders
clean:
    find . \( -name __pycache__ -o -name "*.pyc" \) -delete

# Clear and display pwd
clear :
    clear
    pwd

# Install no-dev dependencies with poetry
install: clean clear
    poetry install --only main --sync

# install all dependencies with poetry and npm
install-all: clean clear
    poetry install --sync
    npm install
    sudo npm install markdownlint-cli2@0.4.0 --global

# Launch only slow test
pytest-slow: clear
    python3 -m pytest -m "slow"
    just clean

# Launch all tests
pytest-fast: clear
    python3 -m pytest -m "not slow"
    just clean

# Update pre-commit hook and make a clean install of pre-commit dependencies
preupdate: clean clear
    pre-commit clean
    pre-commit autoupdate
    preinstall

# Do a clean install of pre-commit dependencies
preinstall: clean clear
    pre-commit install --hook-type pre-merge-commit
    pre-commit install --hook-type pre-push
    pre-commit install --hook-type post-rewrite
    pre-commit install-hooks
    pre-commit install

# Simulate a pre-commit check on added files
prepre: clean clear
    #!/usr/bin/env sh
    set -eux pipefail
    git status
    pre-commit run --all-files

# Launch sqlfluff linter
sqlfluff path=".": clean clear
    sqlfluff lint {{ path }}

# Launch the mypy type linter on the module exodam
mypy_lib: clean clear
    mypy --pretty -p exodam --config-file pyproject.toml

# Launch the mypy type linter on the module exodam_utils
mypy_utils: clean clear
    mypy --pretty -p exodam_utils --config-file pyproject.toml

# Launch the mypy type linter on the module exodam_resources
mypy_resources: clean clear
    mypy --pretty -p exodam_resources --config-file pyproject.toml

# Launch the mypy type linter on the module exodam_resources
mypy_exodict: clean clear
    mypy --pretty -p exodict --config-file pyproject.toml

# Run ruff
ruff path="exodam exodam_resources exodam_utils exodict": clean clear
    ruff {{path}}

# Run markdownlint
lintmd path='"**/*.md" "#node_modules"': clean clear
    markdownlint-cli2-config ".markdownlint-cli2.yaml" {{ path }}

# Run all linter
lint : clean clear onmy31 mypy_lib mypy_resources mypy_utils mypy_exodict ruff sqlfluff lintmd

# Run black and isort
onmy31 path ="exodam exodam_utils exodam_resources exodict tests": clean clear
    black {{path}}
    isort {{path}}

# auto interactive rebase
autorebase: clean clear
    git rebase -i $(git merge-base $(git branch --show-current) main)

# rebase on main
rebaseM: clean clear
    git checkout main
    git pull
    git checkout -
    git rebase main

# Launch coverage on all
coverage: clean clear
    coverage run -m pytest
    clear
    coverage report -m --skip-covered --precision=3

# clean the folder exodict_files:
clean-exodict:
    rm exodict_files/*.exodict.json

# List all just commands
list :
    just --list
