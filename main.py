"""
EXODAM is a program to help in creation/manipulation of exoplanets description files.

Its can be used for all and other exo-objects (exomoons, discs, comets...).

It is a TYPER program that can launch the different verbs (actions) with their
respective options.
"""

__version__ = "0.36.0"
__status__ = "development"

# Standard imports
from enum import Enum
from typing import Dict, List

# Third party imports
import arrow
import typer
from rich.console import Console

# First party imports
from exodam import (
    CerberusErrors,
    InvalidYamlError,
    create_exodam_file,
    csv_to_yaml,
    dict_to_csv,
    dict_to_sorted_list,
    dog_house,
    generate_default_csv_file,
    generate_default_yaml_file,
    get_exodict,
    load_file,
    show_diff_two_dict,
    write_dict_in_json,
)

__author__ = "Ulysse CHOSSON for LESIA"
__email__ = "ulysse.chosson@obspm.fr"
__copyright__ = "LESIA"
__licence__ = "EUPL v1.2"
__contributors__ = ["Quentin Kral (LESIA)", "Pierre-Yves MARTIN (LESIA)"]

CONSOLE = Console()
APP = typer.Typer()

# TODO: Supprimer le fichier et faire exodam cli.
# Ne pas s'occuper de ce fichier il va totalment disparaitre. Il reste pour mes
# tests perso et avoir un trace pour quand je referais le CLI.


class _ExportFormat(str, Enum):
    """Class for the export format. Exodam can export in csv or json."""

    csv = "csv"
    json = "json"


class _InitFormat(str, Enum):
    """Class for the init format. Exodam can give basics files in yaml or csv."""

    yaml = "yaml"
    csv = "csv"


def _display_warning(warning_messages: List[str]) -> None:
    for message in warning_messages:
        match message:
            case CerberusErrors():
                CONSOLE.print(message.__str__())
            case dict() | list():
                CONSOLE.print(message)
            case _:
                CONSOLE.print(message)


@APP.command()
def version():
    """
    Command launch when user write exodam if --version or -v print the current version.

    Args :
        - version : True or False to print version or not
    """
    print(__version__)


@APP.command()
def check(
    file_path: str,
    display: bool = typer.Option(
        False, "--display", help="Use to display the given yaml file before validation"
    ),
    permissive: bool = typer.Option(
        False, "--permissive", help="Use for a permissive validation"
    ),
    no_doi: bool = typer.Option(
        False, "--no_doi", help="Use to validate a file without DOI"
    ),
):
    """
    Command use to check if input is a yaml exodam valid file.

    Args :
        - input : path of the file to verify
        - display : True or False to print the dict of the yaml file or not
        - permissive : True or False to check with the permissive option or not

    """
    warning_messages, exodam_dict = [], load_file(file_path)

    if not exodam_dict:
        CONSOLE.print("Not yaml valid file [bold red]⨯[/bold red]\n")
        raise InvalidYamlError

    CONSOLE.print("Yaml valid file [bold green]✓[/bold green]\n")

    if display:
        CONSOLE.print(f"dictionary from : {file_path}")
        CONSOLE.print(exodam_dict)
        CONSOLE.print()

    if permissive:
        CONSOLE.print(
            "Warning : you use permissive option, "
            "this test checks only if the syntax of the header is correct",
            style="bold white on orange3",
        )
        CONSOLE.print("Header check : ", style="bold white on #c074c0")

    CONSOLE.print(
        f"{file_path} :\n[bold white on cyan]Check yaml file : [/bold white on cyan]"
    )
    match no_doi:
        case True:
            CONSOLE.print(
                "Warning : you use no-doi option, "
                "this test check all parameters but the DOI parameter is not required",
                style="bold white on orange3",
            )
            dog_house(exodam_dict, permissive, warning_messages, False)
        case _:
            dog_house(exodam_dict, permissive, warning_messages)

    _display_warning(warning_messages)


@APP.command()
def canonize(file_path: str):
    """
    Command use to canonize a yaml file.

    Sort it with this schema : exo_type
    (star -> planet -> satellite -> comet -> pulsar -> ring -> disk)
    -> alphabetic by name (a -> ... -> z).

    Args :
        - path : file to canonize

    """
    data = load_file(file_path)
    canonize_dict = dict_to_sorted_list(data)
    CONSOLE.print(canonize_dict)


@APP.command()
def finalize(exodam_path: str, exodict_path: str):
    """
    Finalize an exodam file.

    Args :
        exodam_path : path of the exodam file
        exodict_path : path of the exodict file that corresponds to the exodam file
    """
    warning_messages = []
    file_name = typer.prompt(
        "Name of the .exodam file which will be created, default : ",
        type=str,
        default=f"exodam_file_{arrow.utcnow().format('YYYY-MM-DD_HH:mm:ss')}",
    )

    created = create_exodam_file(exodam_path, exodict_path, file_name, warning_messages)

    _display_warning(warning_messages)

    if created:
        CONSOLE.print(f'\nExodam file has been create : "{file_name}.exodam"')


@APP.command()
def diff(
    path1: str,
    path2: str,
    permissive: bool = typer.Option(
        False, "--permissive", help="Use for a permissive validation"
    ),
):
    """
    Make the diff between 2 yaml exodam files, check the files.

    If there are valid check the diff.

    If file1 and file2 are not equal show the diff.

    Args :
        - path1 : path of the dirst file
        - path2 : path of the seconde file
    """
    data1, data2, warning_messages = load_file(path1), load_file(path2), []

    check_data1 = dog_house(data1, permissive, warning_messages)
    warning_messages.append("\n")

    check_data2 = dog_house(data2, permissive, warning_messages)
    warning_messages.append("\n")

    if not check_data1:
        warning_messages.append(f"file : '{path1}' are not valid.")

    if not check_data2:
        warning_messages.append(f"file : '{path2}' are not valid.")

    match (check_data1, check_data2):
        case (True, True):
            warning_messages.append(
                f"[bold white on cyan]Difference between : '{path1}' "
                f"and '{path2}'[/bold white on cyan]"
            )
            show_diff_two_dict(data1, data2, warning_messages)
        case _:
            warning_messages.append(
                "[bold white on red]Correct these errors before running the diff "
                "command again.[/bold white on red]"
            )

    _display_warning(warning_messages)


@APP.command()
def export(
    path: str,
    format: _ExportFormat = typer.Argument(
        _ExportFormat.csv, help="Use to choose the format of your export (csv of json)"
    ),
    force: bool = typer.Option(
        False, "--force", help="Use to force the export without validation"
    ),
):
    """
    Command use to check and export a yaml file in csv or json.

    Get the name of file (for json) or name of folder (for csv) with click.prompt
    (default : export_(csv/json)_date). If force is used we dont check the file.

    Args :
        - path : path of the file to export in csv or json
        - format : a string to specify the format, json or csv.
            If format is not specify export in csv by default
        - force : if force is True (exodam export --force [PATH]),
            export don't check the file before export it
    """
    data, valid, warning_messages = load_file(path), False, []

    if not force:
        valid = dog_house(data, permissive=False, warning_messages=warning_messages)
        _display_warning(warning_messages)

    else:
        CONSOLE.print(
            "WARNING : You force the non verification of the file, "
            "it can be Invalid for exodam",
            style="bold white on orange3",
        )

    if valid or force:
        match format:
            case "csv":
                dir_name = typer.prompt(
                    "Name of the folder which will be created and which will contain "
                    "the export (default : 'export_csv_date'): ",
                    type=str,
                    default="export_csv_"
                    f"{arrow.utcnow().format('YYYY-MM-DD_HH:mm:ss')}",
                )
                CONSOLE.print(f"\n{dict_to_csv(data, dir_name)}")
            case "json":
                file_name = typer.prompt(
                    "Name of the file whiwh will be created and which will contain the "
                    "export (default : 'export_json_date'): ",
                    type=str,
                    default="export_json_"
                    f"{arrow.utcnow().format('YYYY-MM-DD_HH:mm:ss')}",
                )
                CONSOLE.print(f"\n{write_dict_in_json(data, file_name)}")


@APP.command()
def import_csv(dir_path: str):
    """
    Import a csv files in folder dir_path to a yaml exodam file.

    The name is get by click.prompt (default : 'import_csv_date.yaml').

    Args :
        - dir_path : Path of the directory contains the files to import
    """
    file_name = typer.prompt(
        "Name of the file which will be created and which will contain the import "
        "(default 'import_csv_date.yaml'): ",
        type=str,
        default=f"import_csv_{arrow.utcnow().format('YYYY-MM-DD_HH:mm:ss')}",
    )
    CONSOLE.print(f"\n{csv_to_yaml(dir_path, file_name)}")


@APP.command()
def init(
    format: _InitFormat = typer.Argument(
        _InitFormat.yaml, help="Use to choose the format of basics files (yaml or csv)"
    )
) -> None:
    """
    Command use to create a basic file in csv or in yaml.

    Args :
        - format : a string to specify the format, yaml or csv.
            If format is not specify export in yaml by default
    """
    match format:
        case "csv":
            generate_default_csv_file()
            CONSOLE.print('Folder : "csv_exemple/" was created with all files.')
        case "json":
            generate_default_yaml_file()
            CONSOLE.print('File : "yaml_exemple.yaml" was created.')


@APP.command()
def generate_exodict(
    all_default: bool = typer.Option(
        False, "--all-default", help="Use to get the default exodict file"
    )
) -> None:
    """
    Generate an exodict json file with the default or given dictionaries.

    Args :
        - all_default : boolean option to generate an exodict json file
            with all default exodictionaries.
    """
    dictionaries: Dict[str, str | None] = dict(
        doi=None,
        basic=None,
        objects=None,
        parameters=None,
        general=None,
        publication=None,
        gs=None,
    )
    if all_default:
        # more_charac = typer.confirm(
        #     "Should the exodcit contain parameters for planets and satellites ?"
        # )
        file_name = typer.prompt(
            "Name of the file which will be created and which will contain the exodict "
            "(default './exodict_files/exodict_date.exodict.json'): ",
            type=str,
            default=f"exodict_{arrow.utcnow().format('YYYY-MM-DD_HH:mm:ss')}",
        )
        get_exodict(file_name)
        CONSOLE.print(f'File : "./exodict_files/{file_name}.exodict.json" was created.')
    else:
        for key in dictionaries:
            path = typer.prompt(
                f"Path of your dictionary for {key} "
                "(empty if you want to use the default dictionary): ",
                type=str,
                default="",
            )
            dictionaries[key] = path
        CONSOLE.print(
            "The exodict that will be generated will use the following files "
            "(Empty for default dictionaries."
        )
        CONSOLE.print(dictionaries)
        typer.confirm("Do you confirm ?", abort=False)
        # more_charac = typer.confirm(
        #     "Should the exodcit contain parameters for planets and satellites ?"
        # )
        file_name = typer.prompt(
            "Name of the file which will be created and which will contain the exodict "
            "(default './exodict_files/exodict_date.exodict.json'): ",
            type=str,
            default=f"exodict_{arrow.utcnow().format('YYYY-MM-DD_HH:mm:ss')}",
        )
        get_exodict(dictionaries, file_name)
        CONSOLE.print(f'File : "./exodict_files/{file_name}.exodict.json" was created.')


if __name__ == "__main__":
    APP()
