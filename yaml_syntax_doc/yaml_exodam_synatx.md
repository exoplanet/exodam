# Yaml exodam syntax

![Exodam icone](./img/exodam-logo-v0.png)

## Table Of Contents

- [Yaml exodam syntax](#yaml-exodam-syntax)
  - [Table Of Contents](#table-of-contents)
  - [File organization](#file-organization)
    - [DOI](#doi)
    - [Objects](#objects)
    - [Gs](#gs)
  - [How to write an exodam yaml file](#how-to-write-an-exodam-yaml-file)
    - [How to write objects](#how-to-write-objects)
    - [How to write gravitational system](#how-to-write-gravitational-system)
    - [Permissive syntax](#permissive-syntax)
  - [Parameters](#parameters)
    - [Position](#position)
    - [Magnitude](#magnitude)
    - [Orbital](#orbital)
    - [Specific planet parameter](#specific-planet-parameter)
    - [Atmospheric](#atmospheric)
    - [Physical](#physical)
  - [Examples](#examples)
    - [One planet system](#one-planet-system)
    - [Basic system file](#basic-system-file)
    - [Complex system file](#complex-system-file)
    - [Binary system file](#binary-system-file)
    - [Triple stars system file](#triple-stars-system-file)
    - [beta Pic system](#beta-pic-system)
    - [Solar system](#solar-system)
    - [Permissive file](#permissive-file)
  - [Appendix](#appendix)
-[Appendix](#appendix)

## File organization

The yaml exodam syntax file concontains 3 big categories

1. DOI
2. objects
3. gs

### DOI

This part is a recipient string which just contains the DOI of the file, this part is mandatory

### Objects

Objects is a list of dictionary which will contain all your astro objects, this part is
mandatory and is explained [here](#how-to-write-objects).

### Gs

Gs is a list of dictionary with all the gravitational relation of the objects described
in the category objects, this part is mandatoory but can be empty and is described in
detail [here](#how-to-write-gravitational-system).

## How to write an exodam yaml file

### How to write objects

To write the objects part you need to declare each object as an element of the list.

```yaml
objects :
    - object 1
    - object 2
    - object 3
    - ...
```

Each object has 3 compulsory parameters

```yaml
name : name
exo_type : type
general :
    status : status
    discovered_date : date
    publication_status :
     status of the publication

```

Name -> must be a string

Exo_type -> must be a string in this list

```python
['star', 'planet', 'satellite', 'pulsar', 'comet', 'disk', 'ring']
```

General must be a dictionary contain 3 parameters

- status -> must be a string in this list

```python
    ['Confirmed', 'Candidate', 'Controversial', 'Retraced']
```

- discovered_date -> must be a string representing a date
- publication_status -> must be a letter in this list

```python
    ['R', 'S', 'C', 'W']
    #[R — Published in a refereed paper, S — Submitted to a professional journal,
    # C — Announced on a professional conference, W — Announced on a website]
```

All other parameters are optional. You can see all parameters in the [Parameters](#parameters)
The order of the objects has no importance

You can see some example [here](#examples)

### How to write gravitational system

To write a gravitational system you need to declare all subsystem as an element of the list.

```yaml
gs :
    -   sg1:
            -  object 1
            -  object 2
    -   sg2:
            -  sg1
            -  object 3
```

You can use a subsystem in an other subsystem. All object declare in subsystem must be
declare before in objects part and must be an element of a list. All subsystem need to
call sg followed by a number (sg1, sg2, sg99, sg15864)

If you wasn't declare any gravitational system just write

```yaml
gs :
```

You can see some example [here](#examples)

### Permissive syntax

You can use permissive option in exodam check :

```shell
exodam check --permissive [file path]
```

If you use it, exodam check only if you have a DOI and objects list which contain
dictionaty with name and exo_type.

Exodam display a Warning message

```shell
Warning : you use permissive option,
this test checks only if the syntax of the header is correct
```

and give you the number and the list of the unknown values (values not in exodam dictionary)

```shell
3 unknown values detected
['cat', 'dog', 'goose']
```

The unknown values will be prefix with tag 'UknVal' in the .exodam file.

You can see an example of permissive yaml exodam file [here](#permissive-file)

## Parameters

Each parameters is in a dictionary, to add a distance you need to add the position
dictionary like this

```yaml
position :
    distance : 10
```

same for magnitude, orbital, specific planet parameter, atmospheric, temp, molecule and
physical parameters. To add molecule name you need to write

```yaml
atmospheric :
    molecule :
        molecule_name:
            - Na
            - H
            - CO
```

Orbital parameters and specific planet parameter are only for planets.

To write specific planet parameter you need to write like this (with _)

```yaml
specific_planet_parametre :
    detection_method : Timing
```

Some parameters can be specified with unit and uncert (pos_uncert, neg_uncert or just
uncert if pos and neg are equals). You don't need to put all field if you just want
specified the unit or uncert. To specified a parameter write this parameter like this

```yaml
position :
    distance :
        value : 19.2
        uncert : 0.1
        unit : pc
```

The two syntax are equals, you can use one of the two

```yaml
position :
    distance : 19.2
```

or

```yaml
position :
    distance :
        value : 19.2
```

You can see the list of parameters can be specified in the [appendix](#appendix)

You can see an example [here](#beta-pic-system) (beta Pic system)

### Position

- distance : Distance of the star to the observer
- ra : Right Ascension
- dec : Declination
- pro_motion : Proper motion of the object
- pro_motion_ra : Proper motion in RA of the object
- pro_motion_dec : Proper motion in Dec of the object
- radial_velocity : radial velocity of the object

### Magnitude

- Magnitude_V : Magnitude in band V
- Magnitude_H : Magnitude in band H
- Magnitude_K : Magnitude in band K
- Magnitude_J : Magnitude in band J
- Magnitude_I : Magnitude in band I

### Orbital

- period : Orbital period of the planet
- semi_major_axis : Semi-major axis of the planet
- eccentrity : Eccentrity of the planet's orbit from 0; circular orbit; to almost 1;
  very elongated orbit
- inclination : Inclination of planet's orbit; angle between the planet orbit and the
  sky plane
- omega : Periapse longitude angle between the periapse and the line of nodes in the
  orbit plane
- angular_dis : Angular distance of the planet

### Specific planet parameter

- detection_method : Method by which the planet was first identified.
  - RV
  - Timing
  - Microlensing
  - Imaging
  - Primary transi
  - Secondary transi
  - astrometry
  - TTV
  - Other
- t_peri : Time of passage at the periapse for eccentric orbits
- t_conj : Time of the star-planet upper conjunction
- t_0 : Time of passage at the center of the transit light curve for the primary transit
- t0_sec : Time of passage at the center of the transit light curve for the secondary
  transit
- lanbda_ang : Sky-projected angle between the planetary orbital spin and the stellar
  rotational spin (Rossiter-McLaughlin anomaly)
- imp_par : Minimum 0 or 1 (yes) or (no)
- tvr : Time of zero increasing radial velocity (i.e. when the planet moves towards the
  observer) for circular orbits
- k : Semi-amplitude of the radial velocity curve
- mass_meas_method : Method of measurement of the planet mass. Mass meas method must be
  one of this :
  - RV
  - Timing
  - Controversial
  - Microlensing
  - Astrometry
  - TTV
  - Spectrum
  - Theoretical
- rad_meas_method : method of measurement of the planet radius. Rad meas method must be
  one of this :
  - Primary transit
  - Theoretical
  - Flux

### Atmospheric

- hot_pt : Longitude of the planet hottest point
- ag : Geometric Albedo of planet
- temp
  - temp_type : Type of Temperature measurement. Temp type must be one of this :
    - disk average
    - day averge
    - night average
    - spatial variations
    - vertical profile
    - 3D map
    - hottest point longitude
  - temp_note : Note on Temperature measurement
  - temp_source : Source of the temperature data; Temp source must be one of this :
    - measurements
    - ~~modeling~~
  - temp_result : Temperature value
  - temp_fig : Figures showing temperature result
- molecule :
  - molecule_type : Type of Molecule measurement. Molecule type must be one of this :
    - Detected
    - Disk average
    - vertical profil
    - detected 1 sigma
    - detected 2 sigma
    - detected 3 sigma
    - detected 4 sigma
    - detected 5 sigma
    - detected, 6 sigma
  - molecule_name : Name of melocule. Molecule name must be one of this :
    - CH4
    - CO
    - CO2
    - H
    - H2O
    - Na
    - TiO
    - VO
    - O2
    - H2
    - K
    - Mg
    - C
    - OI
    - NH3
    - N2
    - Si
    - He
    - SO
    - Fe
    - Fe+
    - Ti+
    - Li
    - SH
    - N
    - AIO
    - TiH
    - CrH
    - ScH
    - Sr
    - Cr
    - Sc
    - Y
  - molecule_note : Note on Molecule measurement
  - molecule_result : Molecule mixing ratio value
  - molecule_source : Source of the molecule data.Molecule source must be one of this :
    - measurements
    - modeling
  - molecule_fig : Figures showing molecule result

### Physical

- mass : mass of the object
- radius : radius of the object
- density : Densityof the object
- Rotation_period : Rotation period of the object
- v_sin(i) : Vsin(i) of the object
- logg : Surface gravity of the object
- t_calc : Planet temperature as calculated by authors based on a planet model
- t_meas : Planet temperature as measured by authors
- spectral_type : Spectral type of the object
- metallicity : Decimal logarithm of the massive elements (« metals ») to hydrogen ratio
  in solar units
- magnetic_filed : Stellar magnetic field True or False for (yes) or (no)
- teff : Effective temperature
- age : Age of this object

## Examples

### One planet system

The following file contains only one planet and is valid with the strct check.

![one planet](./img/one_planet.svg)

```yaml
DOI : 10.1038/nphys1170
objects :
    -   name : beta pic
        exo_type : planet
        general :
            status : Confirmed
            discovered_date : 2011 04 11
            publication_status : R
gs :
```

### Basic system file

The following file contains :
    - 1 star
    - 2 planets
 and is valid with the strict check.

![basic system](./img/basic_system.svg)

```yaml
DOI : 10.1038/nphys1170
objects:
    -   name : omicron
        exo_type : star
        position :
            distance :
                value: 19.3
                uncert : 0.2
                unit : pc
            ra : 05:47:17.0
            dec : -51:03:59
        physical :
            age : 0.4
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : omicron a
        exo_type : planet
        alternate_names : omi a
        position :
            distance : 19.3
            ra : 05:47:17.0
            dec : -51:03:59
        physical :
            age : 0.04
        general :
            status : Confirmed
            discovered_date : 2011 11 04
            publication_status : C
    -   name : omicron b
        exo_type : planet
        position :
            distance : 12.9
        magnitude :
            magnitude_V : 18
            magnitude_H : 12
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
gs :
    -   sg1 :
            -   omicron
            -   omicron a
            -   omicron b
```

### Complex system file

The following file is valid with the strict check.
The file contains :
    - 1 star
    - 1 planet with 2 satellites
    - 1 planet with 1 satellite
    - 1 planet
    - 1 comet
The order of the objects has no importance.

![complex system](./img/complex_system.svg)

```yaml
DOI : blabla12345blabla
objects :
    -   name : omicron
        exo_type : star
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : omicron a
        exo_type : planet
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : loulou a
        exo_type : satellite
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : loulou b
        exo_type : satellite
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : omicron b
        exo_type : planet
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : riri a
        exo_type : satellite
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : omicron c
        exo_type : planet
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : carbon & silicon
        exo_type : comet
        # Keyworld to say we saw it there but isn't in the gravitational system
        KEYWORD : False
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
gs :
    -   sg1 :
            -   omicron a
            -   loulou a
            -   loulou b
    -   sg2 :
            -   omicron b
            -   riri a
    -   sg3 :
            -   omicron
            -   sg1
            -   sg2
            -   omicron c
```

### Binary system file

The following file is valid with the strict check.
The file contains :
    - 2 stars
    - 1 planet with 1 satellite
    - 1 planet
The order of the objects has no importance.

![binary system](./img/binary_system.svg)

```yaml
DOI : blabla12345blabla
objects :
    -   name : omicron
        exo_type : star
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : iota
        exo_type : star
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : omicron a
        exo_type : planet
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : loulou a
        exo_type : satellite
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : omicron b
        exo_type : planet
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
gs :
    -   sg1 :
            -   omicron
                iota
    -   sg2 :
            -   omicron a
                loulou a
    -   sg3 :
            -   sg1
                sg2
                omicorn b
```

### Triple stars system file

The following file is valid with the strict check.
The file contains :
    - 3 stars
    - 3 planets
The order of the objects has no importance.

![triple stars system](./img/triple_stars_system.svg)

```yaml
DOI : blabla12345blabla
objects :
    -   name : omicron a
        exo_type : planet
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : phi
        exo_type : star
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : phi a
        exo_type : planet
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : omicron
        exo_type : star
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : iota
        exo_type : star
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
    -   name : omicron b
        exo_type : planet
        general :
            status : Candidate
            discovered_date : 2011 11 04
            publication_status : W
gs :
    -   sg1 :
            -   omicron
                iota
    -   sg2 :
            -   sg1
                omicron b
    -   sg3 :
            -   omicron
                iota
                phi
    -   sg4 :
            -   phi
                phi a
    -   sg5 :
            -   sg3
                omicron a
```

### beta Pic system

The following file represents the beta Pic system, is valid with the stric check.

![beta pic system](./img/bpic.png)

```yaml
DOI : 12345exemples12345
objects :
    -   name : beta Pic
        exo_type : star
        alternate_names : bet Pic
        position :
            distance :
                value : 19.3
                unit : pc
                uncert : 0.2
            ra : 05:47:17.0
            dec : -51:03:59
        magnitude :
            magnitude_V : 3.86
        physical :
            mass :
                value : 1.73
                unit : Msun
                uncert : 0.05
            spectral_type : A6V
            age :
                value : 0.04
                unit : Gyr
                uncert : 0.004
        general :
            status : Confirmed
            discovered_date : 09/1994
            publication_status : R
            url_simbad : http://simbad.u-strasbg.fr/simbad/sim-id?protocol=html&Ident=beta%20Pic
    -   name : beta Pic c
        exo_type : planet
        alternate_names : bet Pic c
        orbital :
                period :
                    value : 1227.0
                    unit : day
                    uncert : 110
                semi_major_axis :
                    value : 2.68
                    uncert : 0.02
                    unit : AU
                eccentrity :
                    value : 0.32
                    uncert : 0.02
                inclination :
                    value : 89.95
                    uncert : 0.1
                    unit : deg
                omega :
                    value : 66.0
                    unit : deg
                    uncert : 1.8
                angular_dis :
        specific_planet_parametre :
                detection_method : RV
                t_peri :
                    value : 2454116.0
                    unit : JD
                    uncert : 9.0
                mass_meas_method : RV
                rad_meas_method : Flux
        physical :
            mass :
                value : 8.89
                unit : Mj
                uncert : 0.75
            radius :
                value : 1.2
                uncert : 0.1
                unit : Rj
            t_meas :
                value : 1250.0
                unit : K
        general :
            status : Confirmed
            discovered_date : "2019"
            publication_status : W
            url_simbad : http://simbad.u-strasbg.fr/simbad/sim-basic?Ident=beta+pic+c
    -   name : bate pic b
        exo_type : planet
        alternate_names : bet pic b
        orbital :
                period :
                    value : 7707.0
                    pos_uncert : +730.0
                    neg_uncert : -300.0
                    unit : day
                semi_major_axis :
                    value : 9.93
                    uncert : 0.03
                    unit : AU
                eccentrity :
                    value : 0.103
                    uncert : 0.003
                inclination :
                    value : 89.0
                    pos_uncert : +0.01
                    neg_uncert : -0.02
                    unit : deg
                omega :
                    value : 199.3
                    pos_uncert : +2.8
                    neg_uncert : -3.1
                    unit : deg
                angular_dis :
        specific_planet_parametre :
                detection_method : Imaging
                t_peri :
                    value : 2456380.0
                    pos_uncert : +80.0
                    neg_uncert : -60.0
                    unit : JD
                mass_meas_method : RV
                rad_meas_method : Flux
        atmospheric :
                molecule :
                    molecule_type : detected
                    molecule_name :
                            - C
                            - H
                            - N
                            - OI
                    molecule_source : measurements
        physical :
            mass :
                value : 11.9
                pos_uncert : +2.93
                neg_uncert : -3.04
                unit : Mj
            radius :
                value : 1.65
                uncert : 0.06
                unit : Rj
        general :
            status : Confirmed
            discovered_date : "2008"
            publication_status : R
            url_simbad : http://simbad.u-strasbg.fr/simbad/sim-basic?Ident=beta+pic+b

gs :
    - sg1 :
        - beta pic
        - beta pic b
        - beta pic c
```

### Solar system

The following file represents the solar system, is valid with the stric check.
The order of the objects has no importance.
The generals parameters are not correct and all objects are not represented.

![solar system](./img/solar.png)

```yaml
DOI : 10.1038/nphys1170
objects :
    -   name : sun
        exo_type : star
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : mercury
        exo_type : planet
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : venus
        exo_type : planet
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : earth
        exo_type : planet
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : mars
        exo_type : planet
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : jupiter
        exo_type : planet
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : saturn
        exo_type : planet
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : uranus
        exo_type : planet
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : neptune
        exo_type : planet
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : moon
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : phobos
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : deimos
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : io
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : europa
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : ganymede
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : callisto
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : mimas
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : enceladus
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : tethys
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : dione
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : rhea
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : titan
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : hyperion
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : iapetus
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : phoebe
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R*
    -   name : puck
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : miranda
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : ariel
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : umbriel
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : titania
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : oberon
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : proteus
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : triton
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
    -   name : nereid
        exo_type : satellite
        general :
            status : Confirmed
            discovery_date : 2021 01 01
            publication_status : R
gs :
    - sg1 :
        - earth
        - moon
    - sg2 :
        - mars
        - phobos
        - deimos
    - sg3 :
        - jupiter
        - io
        - europa
        - ganymede
        - callisto
    - sg4 :
        - saturn
        - mimas
        - enceladus
        - tethys
        - dione
        - rhea
        - titan
        - hyperion
        - iapetus
        - phoebe
    - sg6 :
        - urans
        - puck
        - miranda
        - ariel
        - umbriel
        - titania
        - oberon
    - sg7
```

### Permissive file

The following file is valid with the permissive option and is contains 1 star and 3
unknown values: ["cat : kitten", "dog : puppy", "goose : gosling"].

```yaml
DOI : 10.1038/nphys1170
cat : kitten
objects :
    -   name : beta pic
        exo_type : star
        position :
            distance :
                value: 19.3
                uncert : 0.2
                unit : pc
        dog : puppy
    -   name : beta Pic b
        exo_type : planet
goose : gosling
```

## Appendix

Parameters can be specified:

- distance
- ra
- dec
- pro_motion
- pro_motion_ra
- pro_motion_dec
- radial_velocity
- magnitude_V
- magnitude_H
- magnitude_K
- magnitude_J
- magnitude_I
- period
- semi_major_axis
- eccentrity
- inclination
- omega
- angular_dis
- t_peri
- t_conj
- t_0
- t0_sec
- lambda_ang
- imp_par
- tvr
- k
- hot_pt
- ag
- temp_result
- temp_fig
- molecule_result
- molecule_fig
- mass
- radius
- density
- rotation_period
- v_sin(i)
- logg
- t_calc
- t_meas
- spectral_type
- metallicity
- teff
- age
