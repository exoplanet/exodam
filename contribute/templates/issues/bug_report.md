# Reporting a bug to Exodam

## Prerequisites

1. Check that you have the latest version of Exodam.
2. Check if the bug was not corrected in a more recent version of Exodam.
3. Check if the bug is not already reported.
4. Reporduce the bug in the latest stable version of Exodam.

## Create the issue

### Names

*Named the issue by : "BUG" + '\_module\_' + 'little description'.*

*example : "BUG \_canonize\_ do not sort parameters".*

### Description

*Bug description, as many detailed as possible.*

### Steps to Reproduce

*Steps to reproduce the bug in a list, as many detailed as possible.*
1.
2.
3.

### Expected behavior

*Expected result by Exodam.*

### Actual behavior

*Current result given by Exodam.*

### Reproduces how often

*How many times have you reproduced the bug.*

### Versions

*In which version of exodam the bug is present.*

### Additional Information

*All other information that may be useful.*
