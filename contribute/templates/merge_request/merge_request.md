# Propose a Merge Request to Exodam

Welcome. Please follow the steps below to tell us about your contribution.

1. Copy the correct template for your contribution
    - Are you fixing a bug? Copy the following [template](bug_fix.md)
    - Are you updating documentation? Copy the following [template](doc.md)
    - Are you add or remove a functionality ? Copy the following [template](feature.md)
    - Are you improve a functionality ? Copy the following [template](improvement.md)
2. Fill in all sections of the template
3. Click "Create pull request"
