# Propose an improvement to Exodam

## Prerequisites

- The merge request must only affect an existing feature. To contribute other changes,
  you must use a different template. You can see all templates [here](merge_request.md)
- The merge request must update the test suite to demonstrate the changed functionality.
- The commit message must be clear and mention the issue of the feature.

## Create the merge request

Your merge request must contain **all** this information

### Description of the Change

### Alternate Designs

### Possible Drawbacks

### Verification Process

### Release Notes

### Additional Information
