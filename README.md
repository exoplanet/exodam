<!-- markdownlint-disable-file MD041 -->

![maintenance](https://img.shields.io/maintenance/yes/2023)

# Exodam (Exoplanetary Data Model)

![exodam logo](contribute/img/exodam_lib_logo.png)

**[French README](translated_readme/README_FRENCH.md)**

Exodam is a Python library to help in the creation and manipulation of exoplanets, and
other exo-objects (exomoons, discs, comets...) description files.

The Exodam API makes easy to verify your yaml exodam files and finalize them with an
exodict (or not). You can see the exodict gitlab
[here](https://gitlab.obspm.fr/exoplanet/exodict).

## Table of Content

- [Exodam (Exoplanetary Data Model)](#exodam-exoplanetary-data-model)
  - [Table of Content](#table-of-content)
  - [Contact](#contact)
  - [Install](#install)
  - [How to use Exodam](#how-to-use-exodam)
  - [Current features](#current-features)
    - [Init](#init)
    - [Check](#check)
    - [Canonize](#canonize)
    - [Diff](#diff)
    - [Finalize](#finalize)
    - [Import csv](#import-csv)
    - [Export](#export)
  - [Contributing and info for developers](#contributing-and-info-for-developers)
  - [Full documentation](#full-documentation)

## Contact

- Author : Ulysse CHOSSON (LESIA)
- Maintainer : Ulysse CHOSSON (LESIA)
- Email : <ulysse.chosson@obspm.fr>
- Contributors :
  - Quentin KRAL (LESIA)
  - Françoise Roques (LESIA)
  - Pierre-Yves MARTIN (LESIA)

## Install

For all specific commands to this project, we use [just](https://github.com/casey/just).
**You need to install it.**

After you can install the dependencies:

```bash
$ just install
pwd
/home/uchosson/Documents/exodam
poetry install --no-dev --remove-untracked
Installing dependencies from lock file

No dependencies to install or update

Installing the current project: exodam (0.35.0)
```

And if you need to develop the project, install development dependencies:

```bash
$ just install-all
pwd
/home/uchosson/Documents/exodam
poetry install --remove-untracked
Installing dependencies from lock file

No dependencies to install or update

Installing the current project: exodam (0.35.0)
npm install
up to date, audited 35 packages in 721ms

7 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
sudo npm install markdownlint-cli2@0.4.0 --global

changed 36 packages, and audited 37 packages in 3s

8 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

and the pre-commit dependencies:

```bash
$ just preinstall
pwd
/home/uchosson/Documents/py-linq-sql
pre-commit clean
Cleaned /home/uchosson/.cache/pre-commit.
pre-commit autoupdate
Updating https://github.com/pre-commit/pre-commit-hooks ...
[INFO] Initializing environment for https://github.com/pre-commit/pre-commit-hooks.
already up to date.
Updating https://github.com/pre-commit/pre-commit-hooks ... already up to date.
Updating https://github.com/pycqa/isort ...
[INFO] Initializing environment for https://github.com/pycqa/isort.
already up to date.
Updating https://github.com/ambv/black ...
[INFO] Initializing environment for https://github.com/ambv/black.
already up to date.
Updating https://github.com/codespell-project/codespell ...
[INFO] Initializing environment for https://github.com/codespell-project/codespell.
already up to date.
Updating https://github.com/pycqa/flake8 ...
[INFO] Initializing environment for https://github.com/pycqa/flake8.
already up to date.
Updating https://github.com/DavidAnson/markdownlint-cli2 ...
[INFO] Initializing environment for https://github.com/DavidAnson/markdownlint-cli2.
already up to date.
pre-commit install --hook-type pre-merge-commit
pre-commit installed at .git/hooks/pre-merge-commit
pre-commit install --hook-type pre-push
pre-commit installed at .git/hooks/pre-push
pre-commit install --hook-type post-rewrite
pre-commit installed at .git/hooks/post-rewrite
pre-commit install-hooks
[INFO] Installing environment for https://github.com/pre-commit/pre-commit-hooks.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
[INFO] Installing environment for https://github.com/pycqa/isort.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
[INFO] Installing environment for https://github.com/ambv/black.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
[INFO] Installing environment for https://github.com/codespell-project/codespell.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
[INFO] Installing environment for https://github.com/pycqa/flake8.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
[INFO] Installing environment for https://github.com/DavidAnson/markdownlint-cli2.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
pre-commit install
pre-commit installed at .git/hooks/pre-commit
```

## How to use Exodam

To use exodam you can run it with [poetry](https://python-poetry.org/).

```shell
poetry run python3 exodam.py --help
```

If you don't install just and poetry.

```shell
python3.10 exodam.py --help
```

Exodam use Typer to launch the different verbs (actions) with their respective options.
You can see the current verbs with `--help` option and in [Current features](#current-features)

## Current features

### Init

Init create a empty basic file that you can fill in to create your exodam file. You can
to choose the format yaml or csv.

### Check

Check verify your file to known if the yaml and exodam syntax are correct.
This command can take several options :

- **display**, display your file before validation.
- **permissive**, can validate only the structure of file, DOI, objects, gs, but don't
  look what's going on inside. ***WARNING** a file checked with the permissive option
  may not be correct when finalizing the exodam file with finalize.*
- **no_doi**, can validate a file without DOI.

### Canonize

Canonize take a yaml exodam file and sort objects and parameters with a specific order.

### Diff

Diff check the difference between 2 file. Diff command check the file before execute the
diff, if you want make diff between 2 file without validation you can use the permissive
option with `--permissive`

### Finalize

Finalize create an exodam file with your file and the given exodictionary.

### Import csv

Import csv take a csv ***folder*** and import it to convert it in yaml exodam. Be
careful `exodam.py import-csv` expects a ***folder*** not a file. You can make
`exodam.py init csv` to have an example.

### Export

Export take an yaml exodam file and export it in json or csv. Export check the file
before exportation, if you want export without validation you can use the force option
with `--force`

## Contributing and info for developers

- [Changelog](CHANGELOG.md)
- [Contributing](CONTRIBUTING.md)
- [Our git workflow](workflow.md)

## Full documentation

- Exodam documentation: WIP
- Syntax of a yaml exodam file: WIP
- Exodict documentation: WIP
- [Exodict project](https://gitlab.obspm.fr/exoplanet/exodict)
