"""Finalize an .exodam file."""

# Standard imports
from hashlib import sha256
from pathlib import Path
from typing import Any, cast

# Third party imports
from dotmap import DotMap

# First party imports
from exodam_utils import EXODICT_TYPE, load_yaml
from exodict import ExodictType, get_exodict, get_exodict_version

# Local imports
from ..check import dog_house
from ..exodam_content import ExodamContent
from ..export import export_exodam
from ..utils import get_colors
from .transform_vupnu import transform_object_into_vupnu


def _hexa_hash_exodict(exodict: dict[str, dict[str, Any] | str]) -> str:
    """
    Hash in hexadecimal with sha256 an exodict.

    https://en.wikipedia.org/wiki/SHA-2

    Args:
        exodict: Exodcit to hash.

    Returns:
        hexadecimal sha256 hash of an exodict.

    Examples:
        >>> _hexa_hash_exodict({"toto": 1, "titi": {"tutu": 2, "tata": 3}})
        '51726abc7c16bfe73dad5cb49cab03deef763a7d3434bbf15cbd8bb50e9e3afd'
    """
    hasher = sha256()
    hasher.update(str(exodict).encode())
    return hasher.hexdigest()


def _finalize_a_raw_data(
    raw_data: dict[str, Any],
    exodict_type: ExodictType,
    warning_messages: list[str],
    with_colors: bool,
) -> EXODICT_TYPE | None:
    """
    Finalize a way to create an exodam file.

    1. Canonize data
    2. Check data
    3. Replace simple value for details value with default unit if not given.
    4. Add exodict and his version to the result

    Args:
        raw_data: Raw_data from a yaml file to finalize.
        exodict_type: Type of exodict we want to use.
        warning_messages: All warning messages.
        with_colors: To allow the color in warning_messages. Default: `True`.

    Returns:
        Content of the generate exodam file or None if the file is not valid.
    """
    colors: DotMap = get_colors(with_colors)

    # 1. canonize with ExodamContent
    canonized_data = ExodamContent(raw_data)

    # 2. check
    if not dog_house(
        canonized_data,
        exodict_type,
        permissive=False,
        warning_messages=warning_messages,
        with_doi=True,
        color=with_colors,
    ):
        warning_messages.append(
            f"{colors.red.on}Somethings got wrong in validatation "
            f"of your yaml file{colors.red.off}",
        )
        return None

    exodict = get_exodict(exodict_type)
    # 3. vupnu
    if exodict_type == ExodictType.SCIENTIFIC:
        transform_object_into_vupnu(
            canonized_data["objects"],
            exodict,
            warning_messages,
            with_colors,
        )

    # 4. add exodict version
    exodict_version = f"{get_exodict_version()}-{_hexa_hash_exodict(exodict)}"
    exodict["version"] = exodict_version

    result = {
        "exodict_version": exodict_version,
        "exodict": exodict,
        "data": canonized_data,
    }

    return cast(EXODICT_TYPE, result)


def finalize_for_cli(
    file_path: Path,
    exodict_type: ExodictType,
    export_path: Path,
    warning_messages: list[str],
) -> bool:
    """
    Finalize a way to create an exodam file in cli.

    1. Load yaml file
    2. Canonize data
    3. Check data
    4. Replace simple value for details value with default unit if not given.
    5. Add exodict and his version to the result
    6. Export the result in a `.exodam.json` file.

    Args:
        file_path: Path of the yaml file to finalize.
        exodict_type: Type of exodict we want to use.
        warning_messages: All warning messages.
        export_path: Path of the destination file.
            Must be an exodam file (.exodam.json).

    Returns:
        True if everything is ok,
        False otherwise.
    """
    raw_data = load_yaml(file_path)

    result = _finalize_a_raw_data(
        raw_data,
        exodict_type,
        warning_messages,
        with_colors=True,
    )

    if result is None:
        return False

    export_exodam(ExodamContent(result), export_path)
    return True


def finalize_for_api(
    raw_data: dict[str, Any],
    exodict_type: ExodictType,
    warning_messages: list[str],
) -> EXODICT_TYPE | None:
    """
    Finalize a way to create an exodam file in API.

    1. Load yaml file
    2. Canonize data
    3. Check data
    4. Replace simple value for details value with default unit if not given.
    5. Add exodict and his version to the result

    Args:
        raw_data: Raw_data from a yaml file to finalize.
        exodict_type: Type of exodict we want to use.
        warning_messages: All warning messages.

    Returns:
        Content of the generate exodam file or None if the file is not valid.
    """
    return _finalize_a_raw_data(
        raw_data,
        exodict_type,
        warning_messages,
        with_colors=False,
    )
