"""
Transform all parameters can be specified by vupnu in vupnu dict.

Used default unit if no unit are given.
"""
# Standard imports
from typing import Any, TypedDict, cast

# Third party imports
from dotmap import DotMap

# First party imports
from exodam_resources import get_vupnu_keys as vupnu_keys_list

# Local imports
from ..utils import ExodamObject, ExodictFileDict, get_colors

# TODO : pos_uncert become supp_uncert and neg_uncert become inf_uncert

DEFAULT = 0


class VupnuParam(TypedDict):
    """Dictionary for a vupnu parameter."""

    value: float | None
    unit: str | dict[str, Any] | None
    pos_uncert: float | None
    neg_uncert: float | None


def init_vupnu_param(default_unit: dict[str, Any] | None) -> VupnuParam:
    """
    Initialize a vupnu parameter with a default unit.

    Args :
        - default_unit : default unit of the parameter that we initialize.It can be None

    Returns :
        A VupnuParam which represents a dictionary with 4 keys:
            value (float), unit (str or dict), pos_uncert (float)
            and neg_uncert (float). uncerts can be None.

    Examples :
        >>> init_vupnu_param(
        ...     {"a" : "aaa"}
        ... )
        {'value': None, 'unit': {'a': 'aaa'}, 'pos_uncert': None, 'neg_uncert': None}

        Unit can be None

        >>> init_vupnu_param(
        ...     None
        ... )
        {'value': None, 'unit': None, 'pos_uncert': None, 'neg_uncert': None}
    """
    result: VupnuParam = {
        "value": None,
        "unit": default_unit,
        "pos_uncert": None,
        "neg_uncert": None,
    }
    return result


def get_default_unit(
    param_name: str,
    exo_dict: ExodictFileDict,
) -> dict[str, Any] | None:
    """
    Get the default unit of a parameter `param_name` in an exodictionary : `exo_dict`.

    Args :
        - param_name : name of the parameter whose unit we want
        - exo_dict : exodict contains unit

    Returns :
        Dictionary representing a unit from an exodict

    Examples :
        >>> get_default_unit(
        ...     "b",
        ...     dict(
        ...         a = {},
        ...         b = {'unit' : [{'y' : 'yy'}, {'z' : 'zz'}]}
        ...     )
        ... )
        {'y': 'yy'}

        >>> get_default_unit(
        ...     "c",
        ...     dict(
        ...         a = {},
        ...         b = {'unit' : [{'y' : 'yy'}, {'z' : 'zz'}]}
        ...     )
        ... )
    """
    for key, value in exo_dict.items():
        match (key, value):
            case (key, {"unit": _}) if key == param_name:
                return cast(dict[str, Any], value["unit"][DEFAULT])
            case (_, dict()):
                if res := get_default_unit(param_name, value):
                    return res
            case _:
                pass
    return None


def develop_vupnu_param(
    param_name: str,
    datas_to_develop: float | dict[str, Any],
    exo_dict: ExodictFileDict,
    warning_messages: list[str],
) -> VupnuParam:
    """
    Develop simple value of parameters can be vupnu into vupnu dict.

    Args :
        - param_name : name of the parameter whose unit we want
        - datas_to_develop : data from exodam file to convert in vupnu parameter
        - exo_dict : exodict contains unit

    Returns :
        A VupnuParam dict with default unit, value and if we have uncert pos_uncert
            and neg_uncert.

    Examples :
        If with have only value

        >>> develop_vupnu_param(
        ...     "a",
        ...     12.3,
        ...     dict(
        ...         a = {'unit' : [{'y' : 'yy'}, {'z' : 'zz'}]}
        ...     ),
        ...     []
        ... )
        {'value': 12.3, 'unit': {'y': 'yy'}, 'pos_uncert': None, 'neg_uncert': None}

        If we have partial vupnu

        >>> develop_vupnu_param(
        ...     "a",
        ...     {'value': 12.3, 'unit': 'pc'},
        ...     dict(
        ...         a = {'unit' : [{'y' : 'yy'}, {'z' : 'zz'}]}
        ...     ),
        ...     []
        ... )
        {'value': 12.3, 'unit': 'pc', 'pos_uncert': None, 'neg_uncert': None}
    """
    default_unit = get_default_unit(param_name, exo_dict)
    result = init_vupnu_param(default_unit)

    match datas_to_develop:
        case float() | int():
            result["value"] = datas_to_develop
            warning_messages.append(f"\t- {param_name}")
        case dict():
            result["value"] = datas_to_develop["value"]
            if "unit" in datas_to_develop.keys():
                result["unit"] = datas_to_develop["unit"]
            elif "unit" not in datas_to_develop.keys():
                warning_messages.append(f"\t- {param_name}")
            if "pos_uncert" in datas_to_develop.keys():
                result["pos_uncert"] = datas_to_develop["pos_uncert"]
            if "neg_uncert" in datas_to_develop.keys():
                result["neg_uncert"] = datas_to_develop["neg_uncert"]
            if "uncert" in datas_to_develop.keys():
                result["pos_uncert"] = datas_to_develop["uncert"]
                result["neg_uncert"] = datas_to_develop["uncert"]

    return result


def transform_one_object_into_vupnu(
    obj: dict[str, Any],
    exo_dict: ExodictFileDict,
    vupnu_keys: list[str],
    warning_messages: list[str],
) -> None:
    """
    Transform an object from an exodam file to specified all vupnu keys.

    Args :
        - obj : An exodam object to modify
        - exo_dict : exodict correspond to the exodam file
        - vupnu_keys : all keys can be specified by vupnu
    """
    for key, value in obj.items():
        if key in vupnu_keys:
            obj[key] = develop_vupnu_param(key, value, exo_dict, warning_messages)
        elif isinstance(value, dict):
            transform_one_object_into_vupnu(
                value,
                exo_dict,
                vupnu_keys,
                warning_messages,
            )
        else:
            pass


def get_vupnu_keys(vupnu_keys: list[str]) -> list[str]:
    """
    Get list of vupnu keys from a dict pulled from .resource_files/utils.yaml.

    Args :
        - vupnu_keys : dict of vupnu_keys pulled from .resource_files/utils.yaml

    Returns :
        list of vupnu keys pulled from .resource_files/utils.yaml

    Examples :
        >>> get_vupnu_keys(
        ... [
        ...     "aa_bb__c",
        ...     "aa_bb__d",
        ...     "aa_bb__e"
        ... ])
        ['c', 'd', 'e']
    """
    result = []
    for key in vupnu_keys:
        k = key.split("__")
        result.append(k[len(k) - 1])
    return result


def transform_object_into_vupnu(
    objects: list[ExodamObject],
    exo_dict: ExodictFileDict,
    warning_messages: list[str],
    with_colors: bool,
) -> None:
    """
    Transform all objects from an exodam file to specified all vupnu keys.

    If we specified a parameter with default unit get it in warning message to display
        it to the user.

    Args :
        objects : All objects in an exodam file
        exo_dict : exodict correspond to the exodam file
        with_colors: To allow the color in warning_messages. Default: `True`.
    """
    colors: DotMap = get_colors(with_colors)

    vupnu_keys = get_vupnu_keys(vupnu_keys_list())

    old_len = len(warning_messages)

    warning_messages.append(
        f"{colors.white_on_orange.on}Warning : for the following parameters we used "
        f"the default units : {colors.white_on_orange.off}",
    )
    count = 1

    for obj in objects:
        warning_messages.append(
            f"{colors.underline.on}{obj['identity']['name']}{colors.underline.off}"
        )
        transform_one_object_into_vupnu(obj, exo_dict, vupnu_keys, warning_messages)
        count += 1

    new_len = len(warning_messages)

    if new_len - count == old_len:
        del warning_messages[-(new_len - old_len) :]
