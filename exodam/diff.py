"""Make difference between 2 ExodamFileDict."""
# Standard imports
import json
from tempfile import NamedTemporaryFile
from typing import cast

# Third party imports
from sh import diff as sys_diff

# Local imports
from .exodam_content import ExodamContent


def diff(data_ref: ExodamContent, data_new: ExodamContent) -> str:
    """
    Make diff of two ExodamContent.

    We use the system diff one two temporary file who contains ExodamContents.

    Args:
        data_ref: Data of the first file (the reference).
        data_new: Data of the second file (The 'new' file).

    Returns:
        The result of the diff, if not diffenrece return a empty string,
        else return the diff.

    Examples:
        >>> ref = ExodamContent({"a": 1, "b": {"c": {"d": 2}}})
        >>> new = ExodamContent({"a": 1, "b": {"c": {"d": 2}}})
        >>> res = diff(ref, new)
        >>> res == ""
        True

        >>> ref = ExodamContent({"a": 1, "b": {"c": {"d": 2}}})
        >>> new = ExodamContent({"a": 1, "b": {"c": {"d": 3}}})
        >>> res = diff(ref, new)
        >>> res == ""
        False
    """
    with NamedTemporaryFile() as f_one:
        with NamedTemporaryFile() as f_two:
            f_one.write(bytes(f"{json.dumps(data_ref, indent=2)}\n", encoding="utf-8"))
            f_two.write(bytes(f"{json.dumps(data_new, indent=2)}\n", encoding="utf-8"))
            f_one.seek(0)
            f_two.seek(0)
            return cast(str, sys_diff(f_one.name, f_two.name, _ok_code=[0, 1]))
