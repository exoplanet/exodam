"""Export an ExodamFileDict in .exodam file."""
# Standard imports
import json
from pathlib import Path

# Local imports
from ..exodam_content import ExodamContent


def export_exodam(data: ExodamContent, export_path: Path) -> None:
    """
    Write data from an ExodamContent in an exodam file.

    Extension of exodam file is `.exodam.json`.

    Args:
        data: Data to write in the given file.
        export_path: Path of the destination file.
            Must be an exodam file (.exodam.json).

    Raises:
        NotADirectoryError: If the parent of the given file does not exist.
        ValueError: If suffixes of the given file are not `.exodam.json`.
        OsError: If an something wrong when we open the file or write in the file.
    """
    if not export_path.parent.exists():
        raise NotADirectoryError(f"{export_path} does not exist or is not a directory.")

    if not export_path.suffixes == [".exodam", ".json"]:
        raise ValueError(f"Suffixes of {export_path} must be `.exodam.json`.")

    export_path.touch(exist_ok=True)

    try:
        with open(export_path, "w", encoding="utf-8") as exodam_file:
            exodam_file.write(json.dumps(data))
    except OSError as err:
        raise err
