"""Export an ExodamFileDict in csv."""
# Standard imports
import csv
from pathlib import Path

# Third party imports
from flatten_dict import flatten
from flatten_dict.reducers import make_reducer

# First party imports
from exodam_resources import get_vupnu_keys

# Local imports
from ..utils import DELIMITER, ExodamFileDict, ExodamObject, GravitationalSystem

# TODO: REVIVE EXPORT ONE DAY


def get_dict_fieldnames(
    obj: ExodamObject,
    results: list[str],
    vupnu_keys: list[str],
    subname: str,
) -> list[str]:
    """
    Get all the keys from this object.

    Its usfull to build the headers of the csv file which will host this object.

    For each key if it is in a sub dictionary, prefix it with the name of
        the dictionary of the higher level.

    Through the dictionary :
        - If value are a dictionary
            Recall the function on this dict with results, vupnu_keys,
            and subname = subname + key + _

        - Else
            - check if result (subname + key) are in vupnu_keys,
                if that is the case result was renamed result + '__value'
            - check if result (subname + key) are in results list
                if not add it to the list

    Args :
        - object : an object from the list of objects from loaded yaml exodam file
        - results : list of the key that we have already found, empty on the first call
        - vupnu_keys : all keys can be specify with vupnu
            (Value, Uncert, Pos_uncert, Neg_uncert, Unit)
        - subname : name of the key if it is in a sub dictionary

    Returns :
        All the keys that can be found in this object, without duplicates

    Examples :
        Simple example with a basic object :

        >>> get_dict_fieldnames(
        ...     {
        ...         "name" : "toto",
        ...         "exo_type" : "star",
        ...     },
        ...     results = [],
        ...     vupnu_keys = [],
        ...     subname = "")
        ['name', 'exo_type']

        Example with a sub dictionary :

        >>> get_dict_fieldnames(
        ...     {
        ...         "name" : "toto",
        ...         "exo_type" : "star",
        ...         "physical" : {"note" : "blabla"}
        ...     },
        ...     results = [],
        ...     vupnu_keys = [],
        ...     subname = "")
        ['name', 'exo_type', 'physical__note']

        Example with a sub dictionary and a key was in vupnu_keys :

        >>> get_dict_fieldnames(
        ...     {
        ...         "name" : "toto",
        ...         "exo_type" : "star",
        ...         "position" : {"distance" : 19}
        ...     },
        ...     results = [],
        ...     vupnu_keys = ["position__distance"],
        ...     subname = "")
        ['name', 'exo_type', 'position__distance__value']
    """
    for key, value in obj.items():
        match value:
            case dict():
                results = get_dict_fieldnames(
                    value,
                    results,
                    vupnu_keys,
                    f"{subname}{key}__",
                )
            case _:
                result = f"{subname}{key}"
                if result in vupnu_keys:
                    result = f"{result}__value"
                if result not in results:
                    results.append(result)

    return results


def get_fieldnames(objects: list[ExodamObject]) -> list[str]:
    """
    Get all the keys from all objects.

    Its usfull to build the headers of the csv file which will host the objects.

    For all objects call the sub function 'get_dict_fieldnames',
    if we already have a key it is not added

    Args :
        - objects : list of objects from a loaded yaml exodam file

    Returns :
        All the keys that can be found in all objects, without duplicates
    """
    results: list[str] = []
    vupnu_key = get_vupnu_keys()

    for obj in objects:
        results = get_dict_fieldnames(obj, results, vupnu_key, subname="")

    return results


def alter_add_implicit_value(obj: ExodamObject, vupnu_keys: list[str]) -> None:
    """
    Alter a dict on the value can be specify by vupnu.

    Browse the dictionary obj, if we find vupnu keys none specify,
    alter this value. (ex : position_distance : 19 -> position_distance_value : 19).

    Args :
        - obj : one object which was flatten from the list of objects in
            exodam yaml file
        - vupnu_keys : all keys can be specify with vupnu
            (Value, Uncert, Pos_uncert, Neg_uncert, Unit)
    """
    for key in obj.fromkeys(obj):
        if key in vupnu_keys:
            obj[f"{key}__value"] = obj.pop(key)


def get_objects_by_type(objects: list[ExodamObject]) -> dict[str, list]:
    """
    Get a list of objects and sort it in list of the 'exo_type'.

    Args :
        - objects : list of all objects in exodam yaml file

    Returns :
        Dictionary with the objects sorted in list of his 'exo_type'
    """
    # TODO: get type by exodam dictionary
    results: dict[str, list[ExodamObject]] = {
        "star": [],
        "planet": [],
        "satellite": [],
        "small_body": [],
        "pulsar": [],
        "ring": [],
        "disk": [],
    }

    for obj in objects:
        results[obj["exo_type"]].append(obj)
    return results


def get_gs_in_dict(list_of_gs: GravitationalSystem) -> dict[str, list]:
    """
    Get a list_of gs and make a dictionary of this gs.

    (gs = [{sg1 : [a, b c]}, {sg2 : [sg1, d]}] -> {sg1 : [a, b, c], sg2 : [sg1, d]}).

    Args :
        - list_of_gs : gs loaded from yaml exodam file

    Returns :
        A dictionary with the gs in this schema : {sg1 : [a, b, c], sg2 : [sg1, d]}
    """
    result = {}
    keys = get_fieldnames(list_of_gs)

    for key in keys:
        result[key] = list_of_gs[keys.index(key)][key]

    return result


def write_gs(list_of_gs: GravitationalSystem, dir_name: Path) -> None:
    """
    Get a list of dictionary with all gs from yaml exodam file, get it in a dictionary.

    (gs = [{sg1 : [a, b c]}, {sg2 : [sg1, d]}] ->
    {sg1 : [a, b, c], sg2 : [sg1, d]})
    with get_gs_in_dict and write it in one line in csv file in
    directory dir_name 'dir_name/gs.csv'.

    Args :
        - list_of_gs : gs loaded from yaml exodam file
        - dir_name : name of the export csv directory

    Raises :
        - ValueError if we can't write in file
    """
    dict_to_write = get_gs_in_dict(list_of_gs)

    try:
        with open(f"{dir_name}/gs.csv", "w", encoding="utf-8") as gs_file:
            writer = csv.DictWriter(gs_file, dict_to_write.keys())
            writer.writeheader()
            writer.writerow(dict_to_write)
    except ValueError as err:
        raise err


def write_one_type(
    sublist_of_objects: list[ExodamObject],
    dir_name: Path,
    file_name: str,
) -> None:
    """
    Get a list of dictionary of the same 'exo_type', 1 object by line.

    If the object have keys can be specified by vupnu
    (Value, Uncert, Pos_uncert, Neg_uncert, Unit) but don't have it,
    replace key by key__value with the function 'alter_add_implicite_value'
    position__distance : 19 -> position__distance__value : 19

    Args :
        - sublist_of_objects : list of objects of one 'exo_type'
        - dir_name : name of the export csv directory
        - file_name : name of the csv file which will contain the objects
            of sublist_of_objects. File name can be star, planet, satellite,
            small_body, pulsar, ring or disk

    Raises :
        - ValueError if we can't write in file
    """
    fieldnames = get_fieldnames(sublist_of_objects)

    try:
        with open(f"{dir_name}/{file_name}.csv", "w", encoding="utf-8") as objects_file:
            writer = csv.DictWriter(objects_file, fieldnames)
            writer.writeheader()
            for obj in sublist_of_objects:
                if isinstance(obj, dict):
                    obj = flatten(  # noqa: PLW2901
                        obj, reducer=make_reducer(delimiter=DELIMITER)
                    )
                    alter_add_implicit_value(obj, get_vupnu_keys())
                writer.writerow(obj)
    except ValueError as err:
        raise err


def write_objects(objects: list[ExodamObject], dir_name: Path) -> None:
    """
    Get a dictionary with all objects sort by exo_type.

    For each type call the function to write all objects
    of this type in the file "dir_name/'exo_type'.csv.

    Args :
        - objects : list of all objects in exodam yaml file
        - dir_name : name of the export csv directory
    """
    objects_by_type = get_objects_by_type(objects)

    for obj_type, objs in objects_by_type.items():
        write_one_type(objs, dir_name, obj_type)


def write_general(doi: str, dir_name: Path) -> None:
    """
    Write the doi of the file in a csv file named general.csv.

    Args :
        - doi : DOI of the file that we export
        - dir_name : name of the export csv directory

    Raises :
        - ValueError if we can't write in file
    """
    dict_doi = {"DOI": doi}

    try:
        with open(f"{dir_name}/general.csv", "w", encoding="utf-8") as general_file:
            writer = csv.DictWriter(general_file, fieldnames=["DOI"])
            writer.writeheader()
            writer.writerow(dict_doi)
    except ValueError as err:
        raise err


def dict_to_csv(dict_to_export: ExodamFileDict, dir_name: Path) -> str:
    """
    Write a dictionary loaded from yaml exodam file in multiple .csv files.

    One for each 'exo_type' by following these steps :
    - Create the directory named dir_name
    - write a file "dir_name/general.csv" with the general information (ex : DOI)
    - write a file for each 'exotype' with all objects of this 'exo_type'

    Args :
        - dict_to_export : data loaded from yaml exodam file
        - dir_name : name of the directory where will the files be created
    """
    dir_name.mkdir(exist_ok=True, parents=True)

    write_general(dict_to_export["DOI"], dir_name)

    # N'import quoi, c'est juste en attendant de revenir sur diff
    list_of_objects = [dict_to_export]
    write_objects(list_of_objects, dir_name)

    list_of_gs = dict_to_export["gs"]
    if list_of_gs:
        write_gs(list_of_gs, dir_name)

    return f'csv files has been created in directory : "{dir_name}/"'
