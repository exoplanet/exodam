"""Get cerberus validation error from a cerberus validator."""
# Standard imports
from dataclasses import dataclass
from typing import Any, cast


@dataclass
class CerberusErrors:
    """Class with the correct dict for cerberus errors."""

    # Will contains all regex error
    regex: dict[str, str]

    # Will contains all required_field errors, field required but not in the
    # checked file
    required_field: dict[str, str]

    # Will contains all wrong_type errors, field that have the wrong type
    wrong_type: dict[str, str]

    # Will contains all unknwon_field, field are not known
    unknown_field: dict[str, str]

    # Will contains all nullable errors, field null but who cannot be
    nullable: dict[str, str]

    # Will contains all custom errors
    custom: dict[str, str]

    # Will contains all other errors
    other: dict[str, str]

    def __init__(self) -> None:
        """Initialize an CerberusError object, all dict in Cerberus object are empty."""
        self.regex = {}
        self.required_field = {}
        self.wrong_type = {}
        self.unknown_field = {}
        self.nullable = {}
        self.custom = {}
        self.other = {}

    def my_str(self, color: bool = True) -> str:
        """
        Get a string with all cerberus errors in self (a CerberusError object).

            - If color is equal to True display with color.
            - Otherwise display without color

        Args :
            - color : boolean which is equal to True by default.

        Returns :
            String with all error formatted correctly.
        """
        result, temp_color, color_reset = "", "[white on red]", "[/white on red]"

        if not color:
            temp_color, color_reset = "", ""

        if self.regex:
            result += f"{temp_color}    Regex : {color_reset}\n"
            result += self._get_one_type_errors(self.regex)

        if self.required_field:
            result += f"{temp_color}    Required Field : {color_reset}\n"
            result += self._get_one_type_errors(self.required_field)

        if self.wrong_type:
            result += f"{temp_color}    Wrong Type : {color_reset}\n"
            result += self._get_one_type_errors(self.wrong_type)

        if self.unknown_field:
            result += f"{temp_color}    Unknown Field : {color_reset}\n"
            result += self._get_one_type_errors(self.unknown_field)

        if self.nullable:
            result += f"{temp_color}    Null value : {color_reset}\n"
            result += self._get_one_type_errors(self.nullable)

        if self.custom:
            result += f"{temp_color}    Custom : {color_reset}\n"
            result += self._get_one_type_errors(self.custom)

        if self.other:
            result += f"{temp_color}    Other : {color_reset}\n"
            result += self._get_one_type_errors(self.other)

        return result

    def _get_one_type_errors(self, err: dict[str, str]) -> str:
        """
        Display all errors in err on one line.

        Args :
            - err : Error to display

        Examples :
            >>> ce = CerberusErrors()
            >>> result = ce._get_one_type_errors(
            ... dict(
            ...     a = "aa",
            ...     b = "bb"
            ... ))
            >>> print(result)
                    - a : aa
                    - b : bb
        """
        result = ""
        for key, value in err.items():
            result += f"        - {key} : {value}\n"

        return result


def parse_errors_in_list(
    cerberus_errors: list[Any],
    result: dict[str, str | None],
) -> None | str:
    """
    Parse a list to get the element that corresponds to the value.

    Args :
        - cerberus_errors : contains all errors
        - result : contains the result that will be returned at the end
            of the processing

    Returns :
        A string that corresponds to the value

    Examples :

        >>> parse_errors_in_list(["aa"], {})
        aa
    """
    for element in cerberus_errors:
        match element:
            case list():
                parse_errors_in_list(element, result)
                return None  # return None but result may have be modified
            case dict():
                parse_errors_in_dict(element, result)
                return None  # return None but result may have be modified
            case _:
                return cast(str, element)

    return None


def parse_errors_in_dict(
    cerberus_errors: dict[str, list[Any]],
    result: dict[str, str | None],
) -> dict[str, str | None]:
    """
    Parse a dict to get a simple dict with key.

    Value which key is the parameter where the error
        comes from and value is the error string.

    Args :
        - cerberus_errors : contains all errors
        - result : contains the result that will be returned at the
            end of the processing

    Returns :
        dict with key, value which key is the parameter where the error
            comes from and value is the error.

    Examples :

        >>> parse_errors_in_dict(
        ... dict(
        ...     a = ["aa"],
        ...     b = ["bb"],
        ...     c = [{'d' : "cc"}]
        ... ),
        ... {}
        ... )
        {'a': 'aa', 'b': 'bb', 'd': 'cc', 'c': None}
    """
    for key, value in cerberus_errors.items():
        match value:
            case list():
                result[key] = parse_errors_in_list(value, result)
            case dict():
                result[key] = parse_errors_in_dict(value, result)
            case _:
                result[key] = value

    return result


def parse_errors(cerberus_errors: dict[str, list[Any]]) -> CerberusErrors:
    """
    Parse errors give by cerberus in a dict of dict which if subdict contains key.

    Value which key is the parameter where the error comes from and
        value is the error string.

    Args :
        - cerberus_errors : contains all errors give by cerberus

    Returns :
        dict of dict which if subdict contains key,
            value which key is the parameter where the error comes
            from and value is the error string.

    Examples :

        >>> parse_errors(
        ... dict(
        ...     a = ["unknown field"],
        ...     b = ["none or more than one rule validate"],
        ...     c = ["null value not allowed"]
        ... ))
        CerberusErrors(regex={'b': 'none or more than one rule validate'},
        ... required_field={}, wrong_type={}, unknown_field={'a': 'unknown field'},
        ... nullable={'c': 'null value not allowed'}, custom={}, other={})

        If some errors are not known, they are in an 'Other' category

        >>> parse_errors(
        ... dict(
        ...     a = ["aa"],
        ...     b = ["bb"]
        ... ))
        CerberusErrors(regex={}, required_field={}, wrong_type={}, unknown_field={},
        ... nullable={}, custom={}, other={'a': 'aa', 'b': 'bb'})
    """
    result = CerberusErrors()

    err = parse_errors_in_dict(cerberus_errors, {})

    for key, value in err.items():
        if value:
            if "unknown" in value:
                result.unknown_field[key] = value
            elif "null" in value:
                result.nullable[key] = value
            elif "required" in value:
                result.required_field[key] = value
            elif "must be of" in value:
                result.wrong_type[key] = value
            elif "none or more" in value:
                result.regex[key] = value
            else:
                result.other[key] = value

    return result


def get_cerberus_errors(cerberus_errors: dict[str, list[Any]]) -> CerberusErrors:
    """
    Display all errors form `cerberus_errors` separate in categories.

    Parse dictionary error give by cerberus and add to `warning_messages`
        grouped by categories.

    Args :
        - cerberus_errors : Errors dictionary give by cerberus

    Examples :
        >>> errors = get_cerberus_errors(
        ... dict(
        ...     a = ["unknown field"],
        ...     b = ["none or more than one rule validate"],
        ...     c = ["null value not allowed"]
        ... ))
        >>> print(errors.my_str())
        [white on red]    Regex : [/white on red]
                    - b : none or more than one rule validate
        [white on red]    Unknown Field : [/white on red]
                    - a : unknown field
        [white on red]    Null value : [/white on red]
                    - c : null value not allowed

        If some errors are not known, they are added in 'Other' category

        >>> errors = get_cerberus_errors(
        ... dict(
        ...     a = ["aa"],
        ...     b = ["bb"]
        ... ))
        >>> print(errors.my_str())
        [white on red]    Other : [/white on red]
                - a : aa
                - b : bb
    """
    parsed_errors: CerberusErrors = parse_errors(cerberus_errors)

    return parsed_errors
