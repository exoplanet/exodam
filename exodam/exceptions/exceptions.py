"""Exceptions for the exodam-lib module."""

# First party imports
from exodam_utils import ExodamError


class ExodamLibError(ExodamError):
    """Base Exception for exodam-lib module."""


class ExodamImportError(ExodamLibError):
    """Import base Exception."""


class GeneralNotFoundError(ExodamImportError):
    """Exception class for a missing general.csv file."""


class NoObjectsError(ExodamImportError):
    """Exception class for a missing objects dict in csv file."""


class InvalidExodictError(ExodamLibError):
    """Exception class for an Invalid Exodict."""
