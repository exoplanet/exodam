"""Code for the module: Exodam-lib."""

__version__ = "0.45.0"
__app_name__ = "Exo-Import"
__author__ = "Ulysse CHOSSON for LESIA"
__email__ = "ulysse.chosson@obspm.fr"
__copyright__ = "LESIA"
__licence__ = "EUPL v1.2"
__contributors__ = ["Quentin Kral (LESIA)", "Pierre-Yves MARTIN (LESIA)"]

# First party imports
from exodam_resources import (  # noqa: F401
    generate_cerberus_and_insert,
    generate_exodam_types,
    generate_keys,
    generate_yml_skel,
    get_cerberus_content,
    get_exodam_types,
    get_known_keys,
    get_yml_skel,
)
from exodam_utils import ExodamError  # noqa: F401
from exodict import (  # noqa: F401
    EditorialGenerateParams,
    ExodictEditorialName,
    ExodictError,
    ExodictScientificName,
    ExodictType,
    LabelError,
    MissingMandatoryKeyError,
    ScientificGenerateParams,
    check_labels,
    generate_and_insert_exodict,
    generate_exodict_and_write_in_file,
    get_exodict,
    get_exodict_version,
    merge_exodict,
)

# Local imports
from .check import check_and_rename_unknowns, check_grav_sys, dog_house  # noqa: F401
from .diff import diff  # noqa: F401
from .exceptions import CerberusErrors, get_cerberus_errors  # noqa: F401
from .exodam_content import ExodamContent  # noqa: F401
from .export import (  # noqa: F401
    alter_add_implicit_value,
    dict_to_csv,
    export_exodam,
    get_objects_by_type,
)
from .finalize import (  # noqa: F401
    finalize_for_api,
    finalize_for_cli,
    get_vupnu_keys,
    transform_object_into_vupnu,
    transform_one_object_into_vupnu,
)
from .giveskel import (  # noqa: F401
    generate_default_csv_file,
    generate_default_yaml_file,
)
from .import_from import (  # noqa: F401
    GeneralNotFoundError,
    NoObjectsError,
    csv_to_yaml,
    dict_to_yaml,
    get_general_and_objects,
    get_gs,
    get_system_in_dict,
    read_csv,
)
