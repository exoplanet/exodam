"""ExodamContent type to store content of exodam file."""

# Future imports
from __future__ import annotations

# Standard imports
from typing import Any, cast

# Third party imports
from flatten_dict import flatten
from flatten_dict.reducers import make_reducer

# TODO: Get from a config file
DICT_FLATTEN_DELIMITER = "_!_&_"


class ExodamContent(dict):
    """
    ExodamContent type.

    This type store the content of an exodam file, sorted by keys.
    """

    @staticmethod
    def _static_get_all_keys(dict_content: dict[str, Any]) -> list[str]:
        """
        Get all key of a nested dictionary.

        Args:
            dict_content: Dictionary from which we want to extract the keys.

        Returns:
            All keys of the given dict.

        Examples:
            >>> d = {'a': 1, 'b': 2}
            >>> ExodamContent._static_get_all_keys(d)
            ['a', 'b']

            >>> d = {'a': 1, 'b': {'c': 3}}
            >>> ExodamContent._static_get_all_keys(d)
            ['a', 'b', 'c']

            >>> d = {'a': 1, 'b': [{'c': 2}, {'d': 3}]}
            >>> ExodamContent._static_get_all_keys(d)
            ['a', 'b', 'c', 'd']

            >>> d = {'a': 1, 'b': {'c': [[{'d': 3}, {'e':4}], [{'d': 3}, {'e':4}]]}}
            >>> ExodamContent._static_get_all_keys(d)
            ['a', 'b', 'c', 'd', 'e', 'd', 'e']
        """
        res = []

        match dict_content:
            case dict():
                for key, value in dict_content.items():
                    res.append(key)
                    match value:
                        case dict():
                            res.extend(ExodamContent._static_get_all_keys(value))
                        case list() | tuple():
                            for element in value:
                                res.extend(ExodamContent._static_get_all_keys(element))
                        case _:
                            pass
            case list() | tuple():
                for element in dict_content:
                    if isinstance(element, (list, dict, tuple)):
                        res.extend(ExodamContent._static_get_all_keys(element))
            case _:
                pass

        return res

    @staticmethod
    def _hash_exodam_content(raw_content: Any) -> str:  # noqa: ANN401
        """
        Hash an exodam content.

        If `raw_content` is a dict, return a string of the flatten dict,
        else return the string of raw_content.

        Args:
            raw_content: content to has.

        Returns:
            The hash of `raw_content`.
        """
        match raw_content:
            case dict():
                return str(
                    flatten(raw_content, reducer=make_reducer(DICT_FLATTEN_DELIMITER))
                )
            case _:
                return str(raw_content)

    @staticmethod
    def _sort_raw_exodam_content(raw_content: Any) -> Any:  # noqa: ANN401
        """
        Sort recursively a raw exodam content.

        We sort dictionary by keys, we sort list by hash of elements.

        Args:
            raw_content: Content to sort.

        Returns:
            The sorted content.
        """
        match raw_content:
            case dict():
                return {
                    key: ExodamContent._sort_raw_exodam_content(value)
                    for key, value in sorted(raw_content.items())
                }
            case list():
                return sorted(
                    [
                        ExodamContent._sort_raw_exodam_content(element)
                        for element in raw_content
                    ],
                    key=lambda x: ExodamContent._hash_exodam_content(x),
                )
            case _:
                return raw_content

    def __init__(self, raw_dict: dict[str, Any]) -> None:
        """
        Initialize an ExodamContent.

        Args:
            raw_dict: Content to sort and store in self.
        """
        super().__init__(ExodamContent._sort_raw_exodam_content(raw_dict))

    def __eq__(self, other: object) -> bool:
        """
        Evaluate the equality into self and other.

        Args:
            other: Other object to know if its equal.

        Returns:
            True if its equal, False otherwise.
        """
        if not isinstance(other, (ExodamContent, dict)):
            return NotImplemented
        return ExodamContent._hash_exodam_content(
            self
        ) == ExodamContent._hash_exodam_content(other)

    def __ne__(self, other: object) -> bool:
        """
        Evaluate the not equality into self and other.

        Args:
            other: Other object to know if its not equal.

        Returns:
            True if its not equal, False otherwise.
        """
        if not isinstance(other, (ExodamContent, dict)):
            return NotImplemented
        return ExodamContent._hash_exodam_content(
            self
        ) != ExodamContent._hash_exodam_content(other)

    def get_grav_sys(self) -> dict[str, Any]:
        """
        Get gravitational system part of self.

        Returns:
            The gravitational system sub dict of self.
        """
        if not self["relationship"]:
            return {}

        grav_sys = self["relationship"].get("gravitational_system", None)

        return cast(dict, grav_sys) if grav_sys else {}

    def get_objects(self) -> list[dict[str, Any]]:
        """
        Get objects part of self.

        Returns:
            The objects sub dict of self.
        """
        return cast(list, self["objects"])

    def get_names_in_grav_sys(self) -> list[str]:
        """
        Get all name in gravitational systems.

        Args:
            grav_sys: All gravitational systems.

        Returns:
            All name in all gravitational systems.

        Examples:
            >>> toto = ExodamContent(
            ...     {
            ...         "relationship":
            ...              {"gravitational_system":
            ...                 {"gs1": ["toto"], "gs2": ["gs1", "titi", "tutu"]}
            ...             }
            ...     }
            ... )
            >>> toto.get_names_in_grav_sys()
            ['toto', 'gs1', 'titi', 'tutu']

            >>> toto = ExodamContent({"relationship": None})
            >>> toto.get_names_in_grav_sys()
            []

            >>> toto = ExodamContent({"relationship": {}})
            >>> toto.get_names_in_grav_sys()
            []
        """
        if not self["relationship"]:
            return []

        grav_sys = self["relationship"].get("gravitational_system", None)

        if not grav_sys:
            return []

        result = []
        for obj_names in grav_sys.values():
            result.extend(obj_names)

        return result

    def get_all_keys(self) -> list[str]:
        """
        Get all key of a nested dictionary.

        Returns:
            All keys in the dict self and subdict.
        """
        return ExodamContent._static_get_all_keys(self)

    def get_object_names(self) -> list[str]:
        """
        Get a list with all objects name in objs.

        Args :
            - objs : list contain data
            - results : list of dict contains  future results

        Returns :
            results, the list in args with data append if we find a exotype

        Examples :
            >>> ExodamContent({"objects":
            ...      [
            ...         {"identity": {"name" : "toto"}},
            ...         {"identity": {"name" : "tata"}}
            ...     ]
            ... }).get_object_names()
            ['tata', 'toto']
        """
        return [obj["identity"]["name"].lower() for obj in self.get_objects()]
