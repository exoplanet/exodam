"""Load exodam yaml file."""
# Standard imports
from typing import Any

# Third party imports
from dotmap import DotMap

DELIMITER = "__"

SubGravitationalSystem = dict[str, list[str]]

GravitationalSystem = list[SubGravitationalSystem]

ExodictFileDict = dict[str, Any]

# This must have the mandatory keys:
# doi: str # noqa: ERA001
# objects: List[ExodamObject] # noqa: ERA001
# gs: List[List[str]] # noqa: ERA001
ExodamFileDict = dict[str, Any]

# This must have the mandatory keys:
#   name : str # noqa: ERA001
#   exo_type : str # noqa: ERA001
#   general : Dict[str, str] # noqa: ERA001
ExodamObject = dict[str, Any]


def get_colors(want_color: bool) -> DotMap:
    """
    Get colors for rich print.

    To us it in f-string:
        colors: DotMap = get_colors(with_colors)
        print(f"{colors.red.on}DANGER{colors.red.off}")

    Args:
        want_colors: True for colors False for no colors.

    Returns:
        Colors.

    Examples:
        >>> with_colors = get_colors(True)
        >>> with_colors.green
        DotMap(on='[bold green]', off='[/bold green]')

        >>> without_colors = get_colors(False)
        >>> without_colors.green
        DotMap(on='', off='')
    """
    if want_color:
        return DotMap(
            {
                "green": {"on": "[bold green]", "off": "[/bold green]"},
                "red": {"on": "[bold red]", "off": "[/bold red]"},
                "white": {"on": "[bold white]", "off": "[/bold white]"},
                "white_on_purple": {
                    "on": "[bold white on #c074c0]",
                    "off": "[/bold white on #c074c0]",
                },
                "white_on_yellow": {
                    "on": "[bold white on yellow]",
                    "off": "[/bold white on yellow]",
                },
                "white_on_orange": {
                    "on": "[bold white on orange3]",
                    "off": "[/bold white on orange3]",
                },
                "underline": {
                    "on": "[bold underline]",
                    "off": "[/bold underline]",
                },
            }
        )

    no_colors = {"on": "", "off": ""}
    return DotMap(
        {
            "green": no_colors,
            "red": no_colors,
            "white": no_colors,
            "white_on_purple": no_colors,
            "white_on_yellow": no_colors,
            "white_on_orange": no_colors,
            "underline": no_colors,
        }
    )
