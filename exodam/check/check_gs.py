"""Check the gravitational system part of an exodam file."""
# Third party imports
from dotmap import DotMap

# Local imports
from ..exodam_content import ExodamContent
from ..utils import get_colors


def check_grav_sys(
    exodam_content: ExodamContent,
    warning_messages: list[str],
    with_colors: bool,
) -> bool:
    """
    Check gravitational systems in an exodam file.

    We check if all name in all gravitational systems are object's name or an other
    gravitational system.

    Args:
        exodam_content: Content of an exodam file to check.
        warning_message: All warning message we display in the execution.
        with_colors: To allow the color in warning_messages. Default: `True`.

    Returns:
        True if all name in gravitational systems are object's names or an other
        gravitational system, False otherwise

    Examples:
        >>> toto = ExodamContent(
        ...     {
        ...         "objects": [
        ...             {"identity": {"name": "toto"}},
        ...             {"identity": {"name": "titi"}}
        ...         ],
        ...         "relationship": {
        ...             "gravitational_system":
        ...                 {"gs1": ["toto"], "gs2": ["gs1", "titi"]}
        ...         }
        ...     }
        ... )
        >>> check_grav_sys(toto, [], True)
        True

        >>> toto = ExodamContent(
        ...     {
        ...         "objects": [
        ...             {"identity": {"name": "toto"}},
        ...             {"identity": {"name": "titi"}}
        ...         ],
        ...         "relationship": {
        ...             "gravitational_system":
        ...                 {"gs1": ["toto"], "gs2": ["gs1", "titi", "tutu"]}
        ...         }
        ...     }
        ... )
        >>> check_grav_sys(toto, [], True)
        False
    """
    colors: DotMap = get_colors(with_colors)

    invalid_names = []
    valid_names = [
        *list(exodam_content.get_grav_sys().keys()),
        *exodam_content.get_object_names(),
    ]
    names_in_grav_sys = exodam_content.get_names_in_grav_sys()

    for name in names_in_grav_sys:
        if name not in valid_names:
            invalid_names.append(name)

    if invalid_names:
        warning_messages.append(
            f"Invalid name(s) in gravitational systems: {colors.white.on}"
            f"{invalid_names}{colors.white.off}",
        )
        return False

    return True
