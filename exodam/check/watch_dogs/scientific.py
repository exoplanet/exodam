"""Verify an scientific Exodam file with cerberus."""

# Standard imports
from collections import Counter
from typing import Any

# Third party imports
from dotmap import DotMap

# First party imports
from exodam_resources import ScientificCerberusType
from exodict import ExodictType

# Local imports
from ...exodam_content import ExodamContent
from ...utils import ExodamObject, get_colors
from ..check_gs import check_grav_sys
from .utils import check_it


def _check_publication(
    publication: dict[str, Any],
    warning_messages: list[str],
    with_colors: bool,
) -> bool:
    """
    Check if publication contains at least one URL or DOI.

    Args :
        dict_to_check : Dict to check.
        warning_message: All warning message we display in the execution.
        with_colors: To allow the color in warning_messages. Default: `True`.

    Returns :
        True if the publication contains a doi or an url, False otherwise.

    Examples:
        >>> pub = {"title": "toto", "doi": "123toto", "url": "www.toto.fr"}
        >>> _check_publication(pub, [], True)
        True

        >>> pub = {"title": "toto", "doi": "123toto", "url": None}
        >>> _check_publication(pub, [], True)
        True

        >>> pub = {"title": "toto", "doi": None, "url": "www.toto.fr"}
        >>> _check_publication(pub, [], True)
        True

        >>> pub = {"title": "toto", "doi": None, "url": None}
        >>> _check_publication(pub, [], True)
        False
    """
    colors: DotMap = get_colors(with_colors)

    match publication["doi"], publication["url"]:
        case (None, None):
            warning_messages.append(
                "One of the parameters `url` and `doi` "
                f"must not be None {colors.red.on}⨯{colors.red.off}",
            )
            return False
        case _:
            return True


def _are_duplicate_names(
    objects: list[ExodamObject],
    warning_messages: list[str],
    with_colors: bool,
) -> bool:
    """
    Check names in ExodamFile to know if some objects have the same name.

    We check in name and alternate names.

    Args :
        objects : Objects in a ExodamFile.
        warning_message: All warning message we display in the execution.
        with_colors: To allow the color in warning_messages. Default: `True`.

    Returns :
        True if we found duplicate names, False otherwise.

    Examples:
        >>> objects = [
        ...     {"identity": {"name": "toto", "alternate_names": ["tutu", "tata"]}},
        ...     {"identity": {"name": "titi", "alternate_names": ["tntn", "tyty"]}},
        ... ]
        >>> _are_duplicate_names(objects, [], True)
        False

        >>> objects = [
        ...     {"identity": {"name": "toto", "alternate_names": ["tutu", "tata"]}},
        ...     {"identity": {"name": "toto", "alternate_names": ["tntn", "tyty"]}},
        ... ]
        >>> _are_duplicate_names(objects, [], True)
        True

        >>> objects = [
        ...     {"identity": {"name": "toto", "alternate_names": ["tutu", "tata"]}},
        ...     {"identity": {"name": "titi", "alternate_names": ["toto", "tyty"]}},
        ... ]
        >>> _are_duplicate_names(objects, [], True)
        True

    """
    colors: DotMap = get_colors(with_colors)

    names = [obj["identity"]["name"] for obj in objects]
    for obj in objects:
        names.extend(obj["identity"]["alternate_names"])

    similar = {name for name in names if names.count(name) > 1}

    if similar:
        warning_messages.append(
            f"Duplicate objects names: {colors.white.on}'{similar}'{colors.white.off}"
        )
        return True

    return False


# TODO: Inetegrer detect and rename unknown apres une validation permissive
def scientific_good_cop(
    exodam_content: ExodamContent,
    permissive: bool,
    warning_messages: list[str],
    with_doi: bool,
    with_colors: bool,
) -> bool:
    """
    Check with permissive option if content of exdoam yaml file.

    Args:
        exodam_content: Content of an exodam file to check.
        permissive: True or False for permissive option or not.
        warning_message: All warning message we display in the execution.
        with_doi: True or False to validate the file with a doi or not.
        with_colors: To allow the color in warning_messages. Default: `True`.

    Returns:
        True if syntax is valid, False otherwise.
    """
    colors: DotMap = get_colors(with_colors)

    if not check_it(
        exodam_content,
        ExodictType.SCIENTIFIC,
        ScientificCerberusType.TYPE__PERMISSIVE,
        permissive,
        warning_messages,
        with_colors,
        with_doi,
    ):
        warning_messages.append(
            f"Permissive: Exodam syntax isn't good {colors.red.on}⨯{colors.red.off}"
        )
        return False

    warning_messages.append(
        f"Permissive: Exodam syntax is good{colors.green.on}✓{colors.green.off}"
    )
    return True


def scientific_bad_cop(
    exodam_content: ExodamContent,
    permissive: bool,
    warning_messages: list[str],
    with_colors: bool,
) -> bool:
    """
    Check strictly if content of scientific exdoam yaml file.

    Firstly we check the structure and general informnations of the file,
    Then we check each objects in the file, syntax, names and publication.

    Args :
        exodam_content: Content of an exodam file to check.
        permissive: True or False for permissive option or not.
        warning_message: All warning message we display in the execution.
        with_colors: To allow the color in warning_messages. Default: `True`.

    Returns:
        True if syntax is valid, no duplicate names and all publications contains a doi
        or an url, False otherwise.
    """
    exodam_types_count: Counter = Counter()  # EXODAM_TYPES_COUNT
    colors: DotMap = get_colors(with_colors)

    warning_messages.append(
        f"{colors.white_on_purple.on}Struct and global information check : "
        f"{colors.white_on_purple.off}",
    )

    if not check_it(
        exodam_content,
        ExodictType.SCIENTIFIC,
        ScientificCerberusType.TYPE__BASE,
        permissive,
        warning_messages,
        with_colors,
    ):
        warning_messages.append(
            "Exodam struct and global information syntax isn't good "
            f"{colors.red.on}⨯{colors.red.off}",
        )
        return False

    warning_messages.append(
        "Exodam struct and global information syntax is good "
        f"{colors.green.on}✓{colors.green.off}",
    )

    warning_messages.append(
        f"{colors.white_on_yellow.on}Objects check : {colors.white_on_yellow.off}",
    )

    for obj in exodam_content["objects"]:
        if not check_it(
            obj,
            ExodictType.SCIENTIFIC,
            ScientificCerberusType.TYPE__OBJECTS,
            permissive,
            warning_messages,
            with_colors,
        ):
            warning_messages.append(
                f"Exodam objects syntax isn't good {colors.red.on}⨯{colors.red.off}",
            )
            return False

        if not _check_publication(
            obj["references"]["publication"], warning_messages, with_colors
        ):
            warning_messages.append(
                "Exodam publication syntax isn't good "
                f"{colors.red.on}⨯{colors.red.off}",
            )
            return False

        exodam_types_count[obj["identity"]["exo_type"]] += 1

    warning_messages.append(
        f"Exodam objects syntax is good {colors.green.on}✓{colors.green.off}"
    )

    if _are_duplicate_names(exodam_content["objects"], warning_messages, with_colors):
        warning_messages.append(
            f"Exodam syntax isn't good {colors.red.on}⨯{colors.red.off}"
        )
        return False

    if exodam_content["relationship"] and not check_grav_sys(
        ExodamContent(exodam_content),
        warning_messages,
        with_colors,
    ):
        warning_messages.append(
            f"Exodam syntax isn't good {colors.red.on}⨯{colors.red.off}"
        )
        return False

    warning_messages.append(str(dict(exodam_types_count)))
    warning_messages.append(
        f"Exodam syntax is good {colors.green.on}✓{colors.green.off}"
    )
    return True
