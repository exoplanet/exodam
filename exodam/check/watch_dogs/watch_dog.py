"""Verify an Exodam file with cerberus."""

# First party imports
from exodict import ExodictType

# Local imports
from ...exodam_content import ExodamContent
from .editorial import editorial_bad_cop
from .scientific import scientific_bad_cop, scientific_good_cop


def _scientific_dog_house(
    exodam_content: ExodamContent,
    permissive: bool,
    warning_messages: list[str],
    with_doi: bool,
    color: bool,
) -> bool:
    """
    Check if a valid yaml file had the scientific exodam syntax.

    Args:
        exodam_content: Content of an exodam file to check.
        permissive: True or False for permissive validation.
        warning_message: All warning message we display in the execution.
        with_doi: True or False to validate the file with a doi or not.
        color: To allow the color in warning_messages. Default: `True`.

    Returns:
        True if dict is valid, False otherwise.
    """
    if permissive:
        return scientific_good_cop(
            exodam_content,
            permissive,
            warning_messages,
            with_doi,
            color,
        )

    return scientific_bad_cop(exodam_content, permissive, warning_messages, color)


def _editorial_dog_house(
    exodam_content: ExodamContent,
    warning_messages: list[str],
    color: bool,
) -> bool:
    """
    Check if a valid yaml file had the editorial exodam syntax.

    Args:
        exodam_content: Content of an exodam file to check.
        warning_message: All warning message we display in the execution.
        color: To allow the color in warning_messages. Default: `True`.

    Returns:
        True if dict is valid, False otherwise.
    """
    return editorial_bad_cop(exodam_content, warning_messages, color)


# PLR0913 = Too many arguments to function call
def dog_house(  # noqa: PLR0913
    exodam_content: ExodamContent,
    exodict_type: ExodictType,
    permissive: bool,
    warning_messages: list[str],
    with_doi: bool = True,
    color: bool = True,
) -> bool:
    """
    Check if a valid yaml file had the exodam syntax.

    Args:
        content: Content of an exodam file to check.
        exodict_type: Exodict type of data we want to validate.
        permissive: True or False for permissive validation.
        warning_message: All warning message we display in the execution.
        with_doi: True or False to validate the file with a doi or not.
        color: To allow the color in warning_messages. Default: `True`.

    Returns:
        True if dict is valid, False otherwise.
    """
    match exodict_type:
        case ExodictType.SCIENTIFIC:
            return _scientific_dog_house(
                exodam_content,
                permissive,
                warning_messages,
                with_doi,
                color,
            )
        case ExodictType.EDITORIAL:
            # TODO: traitement de `permissive` et `with_doi`
            return _editorial_dog_house(exodam_content, warning_messages, color)
        # No cover because it's just a security
        case _:  # pragma: no cover
            raise NotImplementedError()
