"""Verify an editorial Exodam file with cerberus."""

# Third party imports
from dotmap import DotMap

# First party imports
from exodam_resources import EditorialCerberusType
from exodict import ExodictType

# Local imports
from ...exodam_content import ExodamContent
from ...utils import get_colors
from .utils import check_it


def editorial_bad_cop(
    exodam_content: ExodamContent,
    warning_messages: list[str],
    with_colors: bool,
) -> bool:
    """
    Check strictly if content of editorial exdoam yaml file.

    Args :
        exodam_content: Content of an exodam file to check.
        warning_message: All warning message we display in the execution.
        with_colors: To allow the color in warning_messages. Default: `True`.

    Returns:
        True if syntax is valid, False otherwise.
    """
    colors: DotMap = get_colors(with_colors)

    warning_messages.append(
        f"{colors.white_on_purple.on}News check : " f"{colors.white_on_purple.off}",
    )

    # news
    if not check_it(
        exodam_content,
        ExodictType.EDITORIAL,
        EditorialCerberusType.TYPE__BASE,
        a_unknown=False,
        warning_messages=warning_messages,
        with_colors=with_colors,
    ):
        warning_messages.append(
            "Exodam news syntax isn't good " f"{colors.red.on}⨯{colors.red.off}",
        )
        return False

    warning_messages.append(
        "Exodam news syntax is good " f"{colors.green.on}✓{colors.green.off}",
    )

    return True
