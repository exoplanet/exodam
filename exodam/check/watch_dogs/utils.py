"""Useful watch dog functions."""

# Standard imports
from typing import Any, cast

# Third party imports
from cerberus import Validator

# First party imports
from exodam_resources import CerberusName, CerberusType, get_cerberus_content
from exodict import ExodictType

# Local imports
from ...exceptions import get_cerberus_errors


def _get_schema(
    cerberus_name_or_type: CerberusName | CerberusType,
    exodict_type: ExodictType,
    a_unknown: bool,
) -> Validator:
    """
    Get schema of cerberus validation.

    Args:
        cerberus_name_or_type: Cerberus resources name or type.
        a_unknown: True for a permissive validation, False for not permissive.

    Returns:
        A Validator based on yaml cerberus files in the folder `path`.
    """
    checker = {}
    cerberus_content = get_cerberus_content(
        exodict_type=exodict_type,
        name_or_type=cerberus_name_or_type,
    )

    for value in cerberus_content.values():
        checker.update(value)

    return Validator(checker, allow_unknown=a_unknown)


# PLR0913 = Too many arguments to function call
def check_it(  # noqa: PLR0913
    exodam_content: dict[str, Any],
    exodict_type: ExodictType,
    cerberus_name_or_type: CerberusName | CerberusType,
    a_unknown: bool,
    warning_messages: list[str],
    with_colors: bool,
    with_doi: bool = True,
) -> bool:
    """
    Check a dict with a cerberus validator.

    Args :
        exodam_content: Content of an exodam file to check.
        cerberus_name_or_type: Cerberus resources name or type.
        a_unknown: True for a permissive validation, False for not permissive.
        warning_message: All warning message we display in the execution.
        with_colors: To allow the color in warning_messages. Default: `True`.

    Returns :
        True if the dictionary is valid, False otherwise.
    """
    validator = _get_schema(cerberus_name_or_type, exodict_type, a_unknown)

    if not with_doi:
        validator.schema["objects"]["schema"]["schema"]["identity"]["schema"]["doi"][
            "required"
        ] = False

    valid = validator.validate(exodam_content)

    if not valid:
        # TODO: Mieux géréer la liste de warning message et la gestion des erreurs
        warning_messages.append(
            get_cerberus_errors(validator.errors).my_str(color=with_colors)
        )

    return cast(bool, valid)
