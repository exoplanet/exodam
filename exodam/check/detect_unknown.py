"""Detect and prefix all unknown key in an ExodamFileDict."""
# Standard imports
import re
from typing import Any

# First party imports
from exodam_resources import get_known_keys
from exodam_utils import REGEX_GS, REGEX_UNIT
from exodict import ExodictType

# Local imports
from ..exodam_content import ExodamContent

# TODO: get it by exodam config file
# Prefix used to say "we don't known this parameter but we keep it.
# UKNVAL_ -> Unknown Values"
# The data was saved as UKNVAL_data in database
UNKNOWN_VALUES_PREFIX = "UKNVAL_"


def _prefix_in_list(list_to_prefix: list[Any], unknown_keys: set[str]) -> None:
    """
    Prefix with `UKNVAL_` all unknown keys in list in an exodam yaml file.

    we prefix the unknown keys to keep them in the database but for the moment
        as they are not in the exodam dictionaries they will not be displayed
        - If we found a list in list_to_prefix recall prefix_in_list on this list
        - If we found a dictionary call prefix_unknown on this dict
        - If we found a unknown key prefix her with `UknVal_`

    Args :
        list_to_prefix : List from the dictionary taken from the yaml exodam file.
        unknown_keys : Unknown keys found in this file.
    """
    for element in list_to_prefix:
        match element:
            case list():
                _prefix_in_list(element, unknown_keys)
            case dict():
                _prefix_unknown(element, unknown_keys)
            case _:
                if element in unknown_keys:
                    list_to_prefix[
                        list_to_prefix.index(element)
                    ] = f"{UNKNOWN_VALUES_PREFIX}{element}"


def _prefix_unknown(
    dict_to_prefix: dict[str, Any],
    unknown_keys: set[str],
) -> dict[str, Any]:
    """
    Prefix with `UKNVAL_` all unknown keys in an exodam yaml file.

    We prefix the unknown keys to keep them in the database but for the moment
        as they are not in the exodam dictionaries they will not be displayed
        - If we found a list in dict_to_prefix call prefix_in_list on this list
        - If we found a dictionary recall prefix_unknown on this dict
        - If we found a unknown key prefix her with `UknVal_`

    Args :
        dict_to_prefix : Dictionary from the yaml exodam file.
        unknown_keys : Unknown keys found in this file.

    Returns :
        `dict_to_prefix` with the unknown keys prefixed by `UKNVAL_`.

    Examples:
        >>> d = {'a': 1, 'b': 2}
        >>> _prefix_unknown(d, ['toto', 'titi'])
        {'a': 1, 'b': 2}

        >>> d = {'a': [{'toto': 1, 'b': 2}, {'c': 3, 'd': 4}], 'titi': 5}
        >>> _prefix_unknown(d, ['toto', 'titi'])
        {'a': [{'b': 2, 'UKNVAL_toto': 1}, {'c': 3, 'd': 4}], 'UKNVAL_titi': 5}
    """
    for key in dict_to_prefix.copy().keys():
        match dict_to_prefix[key]:
            case list():
                _prefix_in_list(dict_to_prefix[key], unknown_keys)
            case dict():
                _prefix_unknown(dict_to_prefix[key], unknown_keys)

        if key in unknown_keys:
            dict_to_prefix[f"{UNKNOWN_VALUES_PREFIX}{key}"] = dict_to_prefix.pop(key)

    return dict_to_prefix


def _get_unknown_keys(
    exodam_content: ExodamContent,
    exodict_type: ExodictType,
) -> set[str]:
    """
    Get unknown keys for an exodam yaml file.

    Get all keys in the exodam content, remove object names, unit names and
    gravitational system names then compare these keys with knowns keys.

    Args:
        exodam_content: Content of the exdoam yaml file to analyze.
        exodict_type: Exodict type of exodam we want to check.

    Returns:
        All unknown keys.
    """
    exodam_keys = exodam_content.get_all_keys()
    objects_name = (
        exodam_content.get_object_names()
        if exodict_type == ExodictType.SCIENTIFIC
        else []
    )
    result = []

    for key in exodam_keys:
        if (
            not re.match(REGEX_GS, key)
            and not re.match(REGEX_UNIT, key)
            and key not in objects_name
        ):
            result.append(key)

    return set(result) - set(get_known_keys(exodict_type))


def check_and_rename_unknowns(
    exodam_content: ExodamContent,
    exodict_type: ExodictType,
    warning_messages: list[str],
) -> ExodamContent:
    """
    Check in a content form yaml exodam file if there are any unknown keys.

    If we have unknown keys renames their vith the prefix `UKNVAL_`.

    Args :
        exodam_content : Exodam content from an exodam yaml file.
        exodict_type: Exodict type of exodam we want to check.
        warning_message: All warning message we display in the execution.

    Returns :
        Exodam content with unknown keys prefixed.
    """
    unknown_keys = _get_unknown_keys(exodam_content, exodict_type)

    if len(unknown_keys) == 0:
        return exodam_content

    warning_messages.append(
        f"\n{len(unknown_keys)} " f"unknown keys detected\n{unknown_keys}\n",
    )

    return ExodamContent(_prefix_unknown(exodam_content, unknown_keys))
