"""Give yaml and csv examples of exodam syntax."""
# Standard imports
from pathlib import Path
from shutil import SameFileError

# Third party imports
from psycopg import Connection
from yaml import dump

# First party imports
from exodam_resources import get_yml_skel
from exodict import ExodictType


def generate_default_yaml_file(
    path: Path = Path.cwd() / "yaml_exemple.yaml", conn: Connection | None = None
) -> None:
    """
    Create an sample yaml exodam file.

    That can be used as a starting point for a contributor.

    Works by copying a hidden basc yaml file as yaml_exemple.yaml

    Args:
        path: Path of the result file.
        conn: Connection to a db. Use in test.

    Raise :
        - IOError if we get an os error
        - SameFileError if `path` already exist
    """
    try:
        with open(path, "w") as res_file:
            dump(get_yml_skel(ExodictType.SCIENTIFIC, conn), res_file)
    # No cover because it's just a security for copyfile
    except (OSError, SameFileError) as err:  # pragma: no cover
        raise err


def generate_default_csv_file() -> None:
    """
    Create an sample csv exodam file.

    That can be used as a starting point for a contributor.

    Works by copying a hidden basc csv file as yaml_exemple.yaml

    Raise :
        - OSError if we get an os error like "disk full"
        - FileExistError if a folder named "csv_exemple" already exist
        - IOError if we get an os error (alias of OsError)
        - SameFileError if a file named that we try to create already exist
    """
    raise NotImplementedError("This feature is not yet implemented")
