"""Create a yaml exodam file from a csv."""
# Standard imports
import csv
import re
from typing import Any

# Third party imports
import yaml
from flatten_dict import unflatten
from flatten_dict.splitters import make_splitter

# Local imports
from ..exceptions import GeneralNotFoundError, NoObjectsError
from ..utils import DELIMITER, ExodamFileDict, ExodamObject, GravitationalSystem

# Regex who match the objects name in a gs, use the get the objects name in a list of
# name and special character.
OBJECT_NAME_REGEX = re.compile(r".*?[a-z1-9].*")

# Separator use to get the gs in a list from a string with all the gs list.
GS_LIST_SEPARATOR = '","'

# Separator to get list of objects name and special character from a string with the
# objects name.
GS_OBJECT_SEPARATOR = "'"

# Types can by an exodam type
# TODO: reprendre ca plus tard
# EXODAM_TYPES = get_exodam_types() # noqa: ERA001
EXODAM_TYPES = ["star", "small_body", "pulsar", "ring", "disk", "planet", "satellite"]


def read_csv(file_name: str) -> None | list[Any]:
    """
    Read a csv file.

    Args :
        - file_name : name of the file to be read

    Returns :
        A list of the data read in the csv file if everything is fine.
        None otherwise
    """
    result = []

    try:
        with open(file_name, newline="", encoding="utf-8") as csv_file:
            reader = csv.reader(csv_file, quotechar="|")

            for row in reader:
                result.append(row)

    except FileNotFoundError:
        return None

    return result


def is_float(data: str) -> bool:
    """
    Determite if a string represented a float.

    Args :
        - data : string which can represent a float

    Returns :
        True if data represented a float ("19.0" or "2")
        False otherwise

    Examples :
        >>> is_float("19")
        True

        >>> is_float("0.2")
        True

        >>> is_float("toto")
        False
    """
    try:
        float(data)
        return True
    except ValueError:
        return False


def get_general_and_objects(path_file: str) -> None | list[ExodamObject]:
    """
    Get all the objects descipe in the file : 'path_file'.csv.

    The function read the file and if the file exist put the key,
    value in a dict. Keys (headers) are in the first row, all other rows are objecst.
    If we find data that has been flattered for export, unflatten it.

    Args :
        - path_file : path of the file

    Returns :
        A list of dictionary with the objects read in the csv file if the file exist.
            1 element of the list is a dict who describe one objects.
        None otherwise
    """
    results = []

    list_from_csv = read_csv(path_file)

    if not list_from_csv:
        return None

    headers = list_from_csv[0]
    data = list_from_csv[1:]

    for datas_list in data:
        result = {}

        for header, sub_data in zip(headers, datas_list, strict=True):
            if sub_data:
                new_sub_data = float(sub_data) if is_float(sub_data) else sub_data
                result[header] = new_sub_data

        result = unflatten(result, make_splitter(delimiter=DELIMITER))
        results.append(result)

    return results


def clean_gs_data(data: list[list[str]]) -> list[list[str]]:
    """
    Clean the data get by get_gs from the csv file.

    read_csv give a list of of row in string,
    this function convert this string of list of dub gs.

    Args :
        - data : list of sub gs from gs.csv file

    Returns :
        list of sub gs with each elements are a list of objects in one sub gs

    Examples :
        >>> clean_gs_data([[
        ...     ''' '"['toto', 'tata']'","'['titi', 'tutu']"' '''
        ... ]])
        [['toto', 'tata'], ['titi', 'tutu']]
    """
    results = []

    joined_string = ",".join(data[0])
    clean_datas = joined_string.split(GS_LIST_SEPARATOR)

    for sub_data in clean_datas:
        res = sub_data.split(GS_OBJECT_SEPARATOR)
        result = list(filter(OBJECT_NAME_REGEX.match, res))
        results.append(result)

    return results


def get_gs(path_file: str) -> None | GravitationalSystem:
    """
    Get all th gs descipe in the file : gs.csv.

    The function read the file and clean the data. 1 element in the list is 1 sub gs.

    Args :
        - path_file : path of the file gs.csv

    Returns :
        If we found gs : a list of dictionary with the gs read in the csv
            file and clean by clean_gs_data. 1 element of the list is a dict with
            key: name of sub gs, value : objects in sub gs
        None otherwise
    """
    results = []

    list_from_csv = read_csv(path_file)

    if not list_from_csv:
        return None

    headers = list_from_csv[0]
    data = list_from_csv[1:]

    clean_datas = clean_gs_data(data)

    for header, data in zip(headers, clean_datas, strict=True):
        results.append({header: data})

    return results


def get_system_in_dict(dir_name: str) -> ExodamFileDict:
    """
    Get a all system and put it in a dict.

        - get the general information from the file general.csv and add them to the dict
        - get the objects information from the files 'objects'.csv and if there
            are data add them to the dict.
                objects file are :
                    star.csv
                    planet.csv
                    satellite.csv
                    small_body.csv
                    pulsar.csv
                    ring.csv
                    disk.csv
        - get the gs information from the file gs.csv and add them to the dict

    Args :
        - dir_name : name of the directory contains all the file to describe a system
            (general.csv, objects.csv, gs.csv)

    Returns :
        A dictionary with all the general parameters, objects and gs describe in files
            in directory 'dir_name'

    Raises :
        - GeneralNotFound, NoObjects
    """
    result = {}
    list_of_objects = []

    general_data = get_general_and_objects(f"{dir_name}/general.csv")
    if not general_data:
        raise GeneralNotFoundError("The mandatory file : 'general.csv' was not found.")

    for data in general_data:
        for key, value in data.items():
            result[key] = value

    for e_type in EXODAM_TYPES:
        data_object = get_general_and_objects(f"{dir_name}/{e_type}.csv")
        if data_object is not None:
            list_of_objects.extend(data_object)

    if not list_of_objects:
        raise NoObjectsError("0 objects find, at least one object required")
    result["objects"] = list_of_objects

    list_of_gs = get_gs(f"{dir_name}/gs.csv")
    if list_of_gs:
        result["gs"] = list_of_gs

    return result


def dict_to_yaml(
    dict_to_write: ExodamFileDict,
    file_name: str,
) -> None:
    """
    Write a valid dict in a yaml file named: 'file_name'.yaml in directory : 'dir_name'.

    Args :
        - dict_to_write : dictionary to write in the yaml file
        - dir_name : name of the dict contains the files to import
        - file_name : name of the yaml file will be created

    Raises :
        - ValueError if `dict_to_yaml` could not be dump
        - FileNotFoundError if `file_name.yaml` was not found
    """
    try:
        with open(f"{file_name}.yaml", "w+", encoding="utf-8") as yaml_file:
            yaml.safe_dump(dict_to_write, yaml_file, sort_keys=False)
    except (ValueError, FileNotFoundError) as err:
        raise err


def csv_to_yaml(dir_name: str, file_name: str) -> str:
    """
    Get the data form all files in 'dir_name' and write it in yaml file: 'file_name'.

    Args :
        - dir_name :name of the dict contains the files to import
        - file_name : name of the yaml file will be created

    Returns :
        Message to display to the user to indicate that the file has been created
    """
    data = get_system_in_dict(dir_name)
    dict_to_yaml(data, file_name)
    return f'yaml file has been created : "{file_name}"'
