# Notre façon de travailler avec git sur exodam

Une façcon de travailler avec git (git workflow dans la suite du document) est comment
nous fonctionnons avec notre git, comment nous créons une nouvelle branche et comment
nous les soumettons à la relecture avant de l'intégrer. Nous verrons comment nous
fonctionnons avec git pour permettre à tous le monde de travailler sur le projet sans
être bloqué en attendant que les brances précédente soit validées.

## Table des matières

- [Notre façon de travailler avec git sur exodam](#notre-façon-de-travailler-avec-git-sur-exodam)
  - [Table des matières](#table-des-matières)
  - [La branche main](#la-branche-main)
  - [Nouvelles branches](#nouvelles-branches)
    - [Nommer vos branches](#nommer-vos-branches)
    - [Branches indépendantes](#branches-indépendantes)
    - [Branches dépendante d'une autre branche pas encore intégrée sur main](#branches-dépendante-dune-autre-branche-pas-encore-intégrée-sur-main)
  - [Faire une demande d'intégration](#faire-une-demande-dintégration)
    - [Rebasage](#rebasage)
    - [Demande d'intgration sur Gitlab](#demande-dintgration-sur-gitlab)

## La branche main

Ce projet fonctionne avec une seule branche protégée, la branche `main`. La branche main
est protégée, ce qui veux dire que seul le propriétaires et les mainteneurs peuvent
envoyer et intégrer dessus.

***C'est notre branche de production.***

![Fig 1](../../contribute/img/main_branch.svg)

## Nouvelles branches

Une branche doit être créer pour chaque modification. Elles seront envoyée sur le dépot
gitlab avant d'être validées par un mainteneur. Après validation elle seront intégrée à
la branch `main`.

### Nommer vos branches

Pour nommer vos branches, préfixer les avec `FEATURE_`, `DOC_`, `FIX_`, ... puis
utilisez un nom en snake case (tout en minuscule separé par des `_`).

Exemples:

- `FEATURE_addition_for_float`
- `DOC_workflow`
- `FIX_bug_0723_addition`

### Branches indépendantes

Si votre nouvelles branche ne dépend d'aucune fonctionnalité qui n'a pas encore été
intégrée sur `main`, vous devez crée votre branche directement depuis la branche `main`.

```bash
git checkout main
git branch FEATURE_nom_de_branche
git checkout FEATURE_nom de branche
```

Vous pouvez aussi faire

```bash
git checkout main
git checkout -b FEATURE_nom_de_branche
```

Pour plus d'informations sur `git branch`, vous pouvez lire
la [documentation sur git-branch](https://git-scm.com/docs/git-branch)

N'oubliez pas de remplir le `CHANGELOG.md` et augmenter le numéro version  dans
`pyprojects.toml`. Modifiez le `README.md` si cela est nécessaire.

![Fig 2](../../contribute/img/inde_branch.svg)

### Branches dépendante d'une autre branche pas encore intégrée sur main

Si votre branche dépende d'une branche pas encore intégrée sur la branche `main`, you
devez crée votre branche depuis cette autre branche.

Si votre branche `FEATURE_2` dépend de `FEATURE_1`

```bash
git checkout FEATURE_1
git branch FEATURE_2
git checkout FEATURE_2
```

Vous pouvez aussi faire

```bash
git checkout FEATURE_1
git checkout -b FEATURE_2
```

Pour plus d'informations sur `git branch`, vous pouvez lire
la [documentation sur git-branch](https://git-scm.com/docs/git-branch)

N'oubliez pas de remplir le `CHANGELOG.md` et augmenter le numéro version  dans
`pyprojects.toml`. Modifiez le `README.md` si cela est nécessaire.

![Fig 3](../../contribute/img/depends_branch.svg)

## Faire une demande d'intégration

### Rebasage

Quand vous avez fini le code de vote branche, n'oubliez pas les tests et de remplire le
`CHANGELOG.md` ainsi que d'augmenté le numéro de version dans `pyprojects.toml`.
Modifiez le `README.md` si cela est nécessaire.

La première étape avant de faire la demande d'intégration (merge request) est d'auto
rebaser votre branche pour nettoyer l'historique.
Pour cela vous devez faire une rebasage intéractif avec

```bash
git rebase -i $(git merge-base $(git branch --show-current) main)
```

Pour plus d'informations sur la commande vous pouvez lire les documentaions de:

- [git-rebase -i](https://git-scm.com/docs/git-rebase#Documentation/git-rebase.txt--i)
- [git merge-base](https://git-scm.com/docs/git-merge-base)
- [git branch --show-current](https://git-scm.com/docs/git-branch#Documentation/git-branch.txt---show-current)

Quand vous avez bien compris à quoi correspond cette commande, vous pouvez utiliser
l'alias just

```bash
just autorebase
```

![Fig_4.1](../../contribute/img/rebase_int_1.png)

Ceci est le fichier de rebasage qui s'ouvre dans votre terminal. Nous allons résumer les
3 mots clefs les plus utilent mais lisez attentivement le commentaire à la fin du
fichier.
Les mots clefs les plus utilent sont:

- reword: Comme sont nom l'indique ce mot-clef vous permet de renommer cotre commit.
- pick: 'Pick' dit au rebasage de garder ce commit comme il est.
- squash: 'Squash' vous permet de reunir ce commit avec le commmit précedent.

Vous devez 'squash' tout les commit qui ne viennent pas de votre branch en un seul
commit que vous appelerez 'All my past'.

![Fig_4.2](../../contribute/img/rebase_int_2.png)

Ensuite vous garder tout les commits important et 'squasher' ceux qui vous ont servi à
sauvegarder votre travail (souvent préfixés avec `WIP`).

![Fig_4.3](../../contribute/img/rebase_int_3.png)

Vous pouvez avoir des conflits à gérer, soyez prudent de séléctionner la bonne partie du
code et vérifiez que tout fonctionne avec precommit avant chaque `git rebase --continue`
A la fin lancez `git push --force-with-lease`.

Si vous relancez `just autorebase` vous devez avoir quelque chose comme ça.

![Fig_4.4](../../contribute/img/rebase_int_4.png)

Pour plus d'informations, regardez ces documentations:

- [git rebase](https://git-scm.com/docs/git-rebase)
- [git push --force-with-lease](https://git-scm.com/docs/git-push#Documentation/git-push.txt---no-force-with-lease)

Une dernière étape avant de pouvoir faire une demande d'intégration (merge request):
vous devez rebaser votre branche sur la branche '`main`.
Pour cela faites To do that use

```bash
git rebase main
```

Resolvez touts les conflits et n'oubliez pas de lancer precommit avant tout
`git rebase --continue`.
A la fin lancez `git push --force-with-lease`.

Quand vous rebasez sur la branche `main`, les commit de cette branche commencerons à la
fin de la branche `main`.

| ![Fig 7.1](../../contribute/img/rebase_main_1.svg) |
|:--:|
| *Avant le rebasage sur main* |

| ![Fig 7.2](../../contribute/img/rebase_main_2.svg) |
|:--:|
| *Après le rebasage sur main* |

### Demande d'intgration sur Gitlab

Quand tout est bon, vous devez faire une demande d'intégration (merge request) sur le
gitlab. Dans le champ: 'Select source branch', selectionnez votre branch et cliquez sur
'Compare branches and continue'.

![Fig 8](../../contribute/img/mr_1.png)

Remplisez le champ: 'Title *' avec le nom de votre branche et le champ: 'Desciption'
avec la partie du changelog qui correspond à votre branche.

![Fig 9](../../contribute/img/mr_2.png)

N'oubliez pas de cochez la case 'Squash commits when merge request is accepted' et
cliquez sur 'Create merge request'.

Quand un mainteneur aura regarder votre code:

- Si tout est bon, il l'intégrera dans la branche `main`.
- Si quelque chose est à corriger, il fera une critique et vous devrez corriger votre
  code avant que le mainteneur relise votre code et l'intégrer quand tout sera corriger.

Une fois que votre MR (merge request) aura été validé, cette branche sera intégrée dans
la branche `main`, ce qui vous donnera une structure comme ci-dessous.

| ![Fig 10.1](../../contribute/img/merge_branch_1.svg) |
|:--:|
| *Avec des branches indépendantes* |

| ![Fig 10.2](../../contribute/img/merge_branch_2.svg) |
|:--:|
| *Avec des branches dépendantes* |
