# Changelog

<!-- markdownlint-disable-file MD024 -->

Ce fichier est une traduction du [CHANGELOG](../../CHANGELOG.md) du projet. Certains
points ont été simplifié. Pour plus de précisions lisez le
[CHANGELOG](../../CHANGELOG.md) en anglais.

Tout les changements notable du projet sont documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère à [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.34.0-alpha] - 2022-01-31

### Ajout

- Ajout d'un verbe `generate_exodict` pour générer un dictionnaire. On peurt avoir celui
  par défaut ou spécifier un fichier pour chaque partie qui diffèrent.
- Ajout d'une fonctionnalité pour avoir un fichier json .exodict avec toute les parties
  du dictionnaire pour un fichier exodam.

## [0.33.1-alpha] - 2022-01-19

### Correction

- Correction de la vérification sur un fichier pour verifié que tout les objets ont un nom
  différent.

## [0.33.0-alpha] - 2022-01-19

### Ajout

- Ajout d'une traduction francaise pour le README.md
- Ajout d'un README.md plus étoffé

## [0.32.0-alpha] - 2022-01-17

### Ajout

- Ajout d'une vérification pour les publications pour savoir si elles ont au moins un
  URL ou un DOI.

## [0.31.0-alpha] - 2022-01-14

### Changement

- `imp_par` devient `impact_param` dans le code.
- `comet` devient `small_body` dans le code.

## [0.30.4-alpha] - 2022-01-13

### Correction

- Correction d'une faute, `magnetic_filed` en `magnetic_field`.
- Correction, `comet` devient `small_body`
- Correction, `specific_planet_parametre` devient `specific_planet_parameter`

## [0.30.0-alpha] - 2022-01-13

### Ajout

- Ajout de [typer](https://typer.tiangolo.com/) que l'on utilise à la place de CLI.

## [0.29.0-alpha] - 2022-01-12

### Ajout

- Ajout de pydocstyle.

## [0.28.5-alpha] - 2022-01-12

### Correction

- Maintenant `finalize` verifie` si `create_exodam_file` fonctionne correctement et
  affiche les bon messages.

## [0.28.4-alpha] - 2022-01-12

### Correction

- Correction de '1 required argument missing' finalize.py dans (doctests).
- Correction de '1 required argument missing' transformvupnu.py dans (doctests).
- Correction de '1 required argument missing' convertvupnu.py dans (pytest).
- Correction du nom d'un exodict dans generatecerberus.py.

## [0.28.0-alpha] - 2022-01-11

### Ajout

- Ajout de warning structurés quand on utilise les unitées par défaut pour les
  paramètre.

Exemples :

```shell
Warning : for the following parameters we used the default units :
beta pic
        - ra
        - dec
        - age
beta pic ter
        - distance
beta Pic toto
        - distance
        - ra
        - dec
        - age

```

## [0.27.0-alpha] - 2022-01-10

### Ajout

- Ajout d'une option à `check` dans CLI : --no_doi. Si no doi est utilisé exodam vérifie
  tout les paramètres mais le champ DOI n'est plus obligatoire.

## [0.26.0-alpha] - 2022-01-10

### Ajout

- Ajout de rich traceback pour un meilleur affichage des Traceback.
- Ajout de [rich](https://github.com/Textualize/rich) pour un meilleur afficher
  utilisateur.

### Changement

- Changement de tout les `print` avec `rich.console.print` à la place de colorama.
- Changement des nom de `RAunit` et `DECunit` en `hms` et `dms`

## [0.25.0-alpha] - 2022-01-07

### Ajout

- Ajout d'un exo_dictionnaire pour les publication.
- Ajout de [just](https://github.com/casey/just) pour simplifié les commandes lié au
  projet.
  - Ajout de [poetry](https://python-poetry.org/) pour organiser le prjet.

### Changement

- Changement des spécification des planets dans les exodict.

## [0.24.0-alpha] - 2022-01-05

### Ajout

- Ajout de la documentation et des test pour cette fonction.
- Ajout de `convert vupu` dans `finalize`.
- Ajout d'un fonction de convertion pour toute les valeurs "simple" (sans unité) avec
  récupération des unitées par defaut si elle ne sont pas spécifiées.

## [0.230-alpha] - 2021-12-20

### Ajout

- Ajout d'un petit dictionnaire pour les systèmes gravitationels

### Changement

- Les satellites ont maintenant les même paramètres que les planetes.
- Les paremètres orbitaux ne sont plus exclusifs au planètes.
- Le choxi pas défaut quand plus unité sont disponible est le premier de la liste dans
  le dictionnaire.
- La métallicité na maintenant plus d'unitée.

## [0.22.1-alpha] - 2021-12-14

### Ajout

- Ajout d'un dossier temporaire pour les tests de generation du cerberus.
- Ajout d'une fonction pour un meilleurs affiche d'un fichier .exodict.
- Ajout d'une exception: `EmptyYaml`.

### Changement

- Mise à jour du fichier CONTRIBUTING.md et de la documentation.
- Tout les `print` qui n'etait pas dans CLI on été supprimés et ajoutés dans CLI.
- Maintenant toutes les Exception créées dans Exodam on besoin d'un message.
- Maintenant `export_exodam` crée un dossier exodam_files si il n'existe pas et écrit le
  ficher .exodam dans ce dossier par défaut. On peut précisé un autre chemin à la
  commande.

### Correction

- Correction d'un bug dans les tests, maintenant on utilise un vrai dossier temporaire
  grâce à la `fxiture tmp_path` cf. [pytestdoc](https://docs.pytest.org/en/6.2.x/tmpdir.html)

### Suppréssion

- Suppression de check.py maintenant on utilise la focntion`dog_house`.

## [0.21.0-alpha] - 2021-12-06

### Ajout

- Ajout de plusieurs fonction pour générer la validation cerberus depuis un
  exo_dictionnaire.

## [0.20.0-alpha] - 2021-11-30

### Ajout

- Ajout d'une fonction pour améliorer l'affichage des erreur de vérification via
  cerberus.

## [0.20.0-alpha] - 2021-11-16

### Changement

- Utilisation des `match case` à la place de `if elif else` quand c'est possible.
- Changement de tout les `Optional[Any]` en `None | Any`. Ceci est la nouvelle syntax
    des union dans le typage de python3.10.

## [0.18.3-alpha] - 2021-11-10

### Ajout

- Ajout de la documentation avec sphinx.

### Changement

- Changement des docstrings dans srcs/ pour le génération de la documentation.

### Correction

- Correction des doctest dans `srcs.detectunknwon.get_unknwon_keys_in_list()`. Avec un
  `set` parfois le test ne fonctionnait pas car le set n'était pas trié.
- Ajout de spectral_type dans le fichier squelette yaml.
- Maintenant `mass_meas_methods` peut être RV (Radial Velocity).

## [0.17.0-alpha] - 2021-11-02

### Ajout

- Ajout d'un verbe pour finaliser un export. Il prend 2 path et demande un nom.
- Ajout d'une fonction pour finaliser un export.

## [0.16.5-alpha] - 2021-10-21

### Ajout

- Ajout 4 `types alias`, GravitationalSystem, SubGravitationalSystem, ExodamDictFile,
  ExodamObject.
- Ajout de doctest dans plusieurs fonctions.
- Ajout de test pour l'import, export. Le test verifie que si on export puis reinmport
  un fichier. Le fichier final est le même que celui donné au début.
- Ajout d'un fichier avec les ressources utile pour la validation des données.
- Ajout d'un squelette csv sous foprme de dossier.

### Changement

- Amélioration des docstring dans plusieurs fonctions.
- Changement des fonction pour avoir les dictionnaires, listes de trie, etc... depuis le
  nouveau ficher de ressource : `.ressources_files/tuils.yaml`
- Changement de `vupnu`, nous n'utilisons plus les tags YAMl mais les Key-Merge de yaml.

```yaml
Vupnu : &vupnu
    type : [float, dict]
    nullable : True
    schema :
      value :
        type : float
      uncert :
        type : float
      pos_uncert :
        type : float
      neg_uncert :
        type : float
      unit :
        type : string
```

Le fonction `replace_vupnu` à été supprimé, elle n'est plus utile.
[merge yaml doc](https://yaml.org/type/merge.html).

- Chnagement des test d'import sans gs, maintenant si le fichier gs.csv n'éxiste pas on
  renvoie None.
- Changement du chemin pour m'import csv. Maintenat nous utilisons `./file_name.yaml` et
  plus `./dir_name/file_name.yaml`.
- Changement du verbe inti, maintenant il crée iun ficher yaml et un dossier contenant
  les csv dans le dossier .init/
- Changement de où est placé le fichier squelette yaml.

### Correction

- Correction d'un bug d'import, exodam n'est plus dans `.srcs/`, il n'y à plus besoin
  d'import exdoam et .exodam.
- Correction de la regex pour le nom des objects dans l'import ; ".*?[a-z].*" ->
  ".*?[a-z1-9].*". Maintenant nous acceptons aussi les noms composé exclusivement de
  chiffres.
- Correct de l'export: SI l'export n'est pas forcé et que le fichier taml n'est pas
  valide nous ne faisons pas l'export. SI un des deux est Vrai nous faisons l'export.
- Correction du nom de certains paramètres.
- Correction des fonctions d'export avec le nouveau délimiteur: '__'.

## [0.15.0-alpha] - 2021-10-19

### Ajout

- Ajout de: `export`, `import` et `diff` a CLI.

## [0.14.1-alpha] - 2021-10-18

### Correction

- Maintenant `canonize` trie les paramètres dans les objets.

## [0.14.0-alpha] - 2021-10-15

### Ajout

- Ajout d'un fonctions d'import pour le csv.

## [0.13.0-alpha] - 2021-10-08

### Ajout

- Ajout d'une fonction `diff` pour faire la différence entre 2 fichiers yaml exodam.

## [0.12.1-alpha] - 2021-10-07

### Ajout

- Ajout d'une fonction et de son verbe  CLI pour exporter en json.
- Ajout du verbe `canonize` pour trié un yaml exodam avec ce cchéma de trie. exo_type
    (star -> planet -> satellite -> comet -> pulsar -> ring -> disk) -> alphabetic by
    name (a -> ... -> z)

### Changement

- Changement de comment on utilise m'export (exodam.py export \[OPTIONS\] PATH), with
  option force and format (csv or json, csv by default).

### Correction

- Correction d'un bug dans l'export json  qui ecrivait le path et non le dictionnaire
  dans le fichier.

## [0.11.1-alpha] - 2021-10-05

### Ajout

- Amélioration des tests avec `pytest.mark.parametrize` et `assertpy`.
- Ajout d'une fonction pour extrait le nom de tout les objets dans un fichier yaml
  exodam.
- Ajout d'une fonction pour supprimer de la listes des inconnues les sg corrects grâce
  au regex.

```yaml
^sg[1-9]+$
```

- Ajout de vérification pour les gs.

### Changement

- Modification de la structure des tests pour plus de clarté.
- Changement de la syntax yaml pour la nouvelle version (cf Issue #26)

### Correction

- Chargement du fichier de vérification cerberus des inconnues dans la fonction
  principale pour optimiser la fonction récurssive `detect_unknwon_recurssive`.
- La fonction `detect_unknown` verifie que la valeurs n'est pas déja dans la liste des
  inconnues.
- Ajout du type `satellite` dans les exo_type.
- La fonction `out_of_sg` est remplacer par `get_objects` et `get_gs`

## [0.10.0-alpha] - 2021-10-01

### Ajout

- Ajout de `canonize` pour trié les dictionnaire yaml exodam avec ce schéma de trie:
  exo_type (star -> planet -> satellite -> comet -> pulsar -> ring -> disk) ->
  alphabetic by name (a -> ... -> z)

## [0.9.0-alpha] - 2021-09-29

### Ajout

- Ajout d'une license et modification de `exodam.py` pour ajouter les auteurs,
  contributeurs, ...

## [0.8.0-alpha] - 2021-09-24

### Ajout

- Ajout d'une fonction pour exporter les fichier yaml exodam en json.

## [0.7.0-alpha] - 2021-09-24

### Ajout

- Ajout d'un verbe `init` pour créer des fichiers d'exemple yaml et csv.

## [0.6.1-alpha] - 2021-09-22

### Correction

- Correction d'un bug quand une liste ne contient pas de liste ou de dictionnaire dans
  la fonction: `we_have_a_list` car les `str` non pas de nom d'attribut.

## [0.6.0-alpha] - 2021-09-22

### Ajout

- Ajout d'une fonction d'export d'exodam

## [0.5.2-alpha] - 2021-09-21

### Correction

- Correction des fonctions `prefix_in_list` et `check_in_list` pour accepter d'autre
  types que des listes et des dictionnaires.

## [O.5.1-alpha] - 2021-09-20

### Correction

- Correction de plusierus nom de molécules. Maintenant `molecule_name` prend une liste
  avec tout les noms de molécules connu et les v"érifie avec une regex.

## [0.5.0-alpha] - 2021-09-20

### Ajout

- Ajout d'une fonctions pour prefixer les clef inconnues.

### Changement

- Changement des test pour la detection des valeurs inconnues.

## [0.4.5-alpha] - 2021-09-20

### Correction

- Correction de tests mal écrit.
    `if len(0) == 0 and not l == ["chat"]` devient `if not len(l) == 0`.

## [0.4.4-alpha] - 2021-09-20

### Correction

- Correction de certains paramlètres.
    Example : (molecule : {name : ....} -> molecule : {molecule_name :....]})

## [O.4.3-alpha] - 2021-09-17

### Correction

- Correction du type de magnetic_field dans unknown_check.yaml (float or dict -> bool).

### Ajout

- Ajout de tests pour des champs vide mais obligatoires en mode permissif et strict?
- Ajout de tests pour des champs vide mais optionels en mode permissif et strict?
- Ajout de la règle `nullable = True` dans les fichiers de vérification cerberus, pour
  les champs optionels.
- Ajout des `unknown_values` dans la verification permissive.

### Changement

- Changement de tout les test avec `assert(check_file(file_name)) == True` (or False)
    to `assert(check_file(file_name))` or `assert(not check_file(file_name))`.

## [O.3.2-alpha] - 2021-09-17

### Correction

- Correction d'une faute dans le nom d'une fonction et d'un fichier:
    'detect_unkown' -> 'detect_unknown'.

## [O.3.1-alpha] - 2021-09-17

### Correction

- Correction d'un nom dans les fichier cerberus: lanbda -> lambda.

### Ajout

- Ajout d'une fonction pour detecter les valeurs inconnue dans la vérification
  permissive.

## [0.2.1-alpha] - 2021-09-16

- Correction des test en accord avec la nouvelle fonction principale de vérification.

### Ajout

- Ajout d'une fonction principale de vérification.

## [0.1.1-alpha] - 2021-09-16

### Correction

- Ajout d'un dossier temporaire pour pytest.
- Corretion de plusieurs petits bugs lié à la vérification cerberus.
- Correction du nom de certains paramètre: `distance_distance` in `distance_value`)
- Correction pour le supooirt de liste dans un fichier yaml exodam.

### Changement

- Changement de al gestion d'erreurs.
- Mise à jour des fichier de validation.
- Export est maintenant un commande.

### Ajout

- Ajout de pyetst pour les objet planètes.
- Ajout de test pour ma validation cerberus.
- Ajout de couleurs et de message pour les utilisateurs dans le module de  vérification.
- Ajout de vérifiation pour les planètes.
- Ajout d'une fonction qui remplace le `tag: vupnu` par `int` ou `dict` pour chaques
  valeurs qui peut être précisées.
- Ajout de couleurs dans les message pour les utilisateurs.
- Ajout d'une vérification permissive.
- Ajout de test sur les vérification et l'export.
- Ajout  de fichier yaml pour vérifier la syntax des fichier yaml exodam.
- Ajout d'un fichier `obj.yaml` pre-documentation pour la syntax yaml que l'on utilise.
- Création d'une première version de l'export csv.
- Utilisation de click pour gérer les commandes d'exodam.
- Ajout de vérifiction sur l'extension du fichier à valider (`.yaml` ou `.yml`).$
- Ajout d'un fichier `exodam.py` pour lancer toutes les commandes de vérification.
- AJout des premières dépendances.
