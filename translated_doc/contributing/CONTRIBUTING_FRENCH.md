# Contribuer à EXODAM (Exoplanetary Data Model)

**Premièrement merci de contribuer.**

La suite explique comment contribuer au projet.

## Table des matières

- [Contribuer à EXODAM (Exoplanetary Data Model)](#contribuer-à-exodam-exoplanetary-data-model)
  - [Table des matières](#table-des-matières)
  - [De quoi avez vous besoin pour installer exodam sur votre ordinateur](#de-quoi-avez-vous-besoin-pour-installer-exodam-sur-votre-ordinateur)
    - [Quelle version de python](#quelle-version-de-python)
    - [Dépendances](#dépendances)
      - [Procédure d'installation pour les dépendances](#procédure-dinstallation-pour-les-dépendances)
      - [Procédure d'installation pour les dépendances de développement](#procédure-dinstallation-pour-les-dépendances-de-développement)
  - [Structure du projet](#structure-du-projet)
    - [Structure d'Exdoam](#structure-dexdoam)
  - [Commment](#commment)
    - [Comment utiliser les tests](#comment-utiliser-les-tests)
    - [Comment reporter des bug](#comment-reporter-des-bug)
    - [Comment soumettre des modifications ou des améliorations](#comment-soumettre-des-modifications-ou-des-améliorations)
    - [Comment faire une demande d'intégration](#comment-faire-une-demande-dintégration)
  - [Pre-commit](#pre-commit)
  - [Guide de style](#guide-de-style)
    - [Formateur](#formateur)
      - [Black](#black)
      - [Isort](#isort)
    - [Linter](#linter)
      - [Pydocstyle](#pydocstyle)
      - [Mypy](#mypy)
      - [Pylint](#pylint)
      - [Flake8](#flake8)
      - [Flakehell](#flakehell)
  - [Contacts](#contacts)

## De quoi avez vous besoin pour installer exodam sur votre ordinateur

### Quelle version de python

Exodam utilise la version 3.10 de python. Pour installer cette version faites:

```bash
sudo apt install python3.10
```

Verifiez la version.

```bash
$ python3 --version
Python 3.10.0
```

### Dépendances

Exodam utilise quelques dépendances:

- [pyyaml](https://pyyaml.org/wiki/PyYAMLDocumentation)
- [types-PyYaml](https://pyyaml.org/wiki/PyYAMLDocumentation)
- [cerberus](https://docs.python-cerberus.org/en/stable/index.html#)
- [click](https://click.palletsprojects.com/en/8.0.x/)
- [arrow](https://arrow.readthedocs.io/en/latest/#)
- [flatten-dict](https://github.com/ianlini/flatten-dict)
- [rich](https://rich.readthedocs.io/en/stable/index.html)
- [typer](https://github.com/tiangolo/typer)
- [dotmap](https://github.com/drgrib/dotmap)

et quelques dépendance pour le développement

- [pytest](https://docs.pytest.org/en/6.2.x/contents.html)
- [pytest-sugar](https://github.com/Teemu/pytest-sugar/)
- [pytest-pudb](https://github.com/wronglink/pytest-pudb)
- [pytest-bdd](https://github.com/pytest-dev/pytest-bdd)
- [pytest-cov](https://github.com/pytest-dev/pytest-cov)
- [xdoctest](https://xdoctest.readthedocs.io/en/latest/autoapi/xdoctest/index.html)
- [assertpy](https://github.com/assertpy/assertpy)
- [pre-commit](https://pre-commit.com)
- [black](https://github.com/psf/black)
- [isort](https://pycqa.github.io/isort/)
- [pydocstyle](http://www.pydocstyle.org/en/stable/)
- [mypy](https://mypy.readthedocs.io/en/stable/)
- [pylint](https://pylint.pycqa.org/en/latest/)
- [plerr](https://github.com/vald-phoenix/pylint-errors)
- [flake8]([flake-9](https://github.com/pycqa/flake8))
- [flakehell](https://flakehell.readthedocs.io/)
- [flake8-bandit](https://github.com/tylerwince/flake8-bandit)
- [flake8-commas](https://github.com/PyCQA/flake8-commas/)
- [flake8-eradicate](https://github.com/wemake-services/flake8-eradicate)
- [dlint](https://github.com/dlint-py/dlint)
- [codespell](https://github.com/codespell-project/codespell/)
- [sphinx](https://www.sphinx-doc.org/en/master/)
- [sphinx-rtd-theme](https://sphinx-themes.org/sample-sites/sphinx-rtd-theme/)
- [myst_parser](https://myst-parser.readthedocs.io/en/latest/)

#### Procédure d'installation pour les dépendances

Vous avez seulement besoin des dépendances classique pour utiliser exodam, elle sont
toutes disponible sur Pypi.

*Pour installer les dépendances vous aurez besoin de*
*[poetry](https://python-poetry.org/) et [just](https://github.com/casey/just)*

```bash
just install
```

Ou utilisez directement poetry:

```bash
poetry install --no-dev --remove-untracked
```

#### Procédure d'installation pour les dépendances de développement

Vous devez installer toutes ces paquets pour contribuer au projet, elle sont toutes
disponible sur Pypi.

*Pour installer les dépendances vous aurez besoin de*
*[poetry](https://python-poetry.org/) et [just](https://github.com/casey/just)*

```bash
just install-all
```

Ou utilisez dorectement poetry (et npm pour markdownlint)

```bash
poetry install --remove-untracked
sudo npm install
sudo npm install markdownlint-cli2 --global
```

## Structure du projet

Exodam est structuré avec 2 dossiers principal et 1 fichier;

- exodam.py
- srcs
- tests

*exodam* contient la code de 'l'interface' terminal pour lancer le programme avec
différentes commandes.

*srcs* contient tout le code du programme structuré en différent dossier pour vous
retrouvez dans le code.

*tests* contient tout les tests pour le programme (plus de 200).

### Structure d'Exdoam

![Struct](../../contribute/img/struct.svg)

## Commment

### Comment utiliser les tests

Pour lancer les test vous pouvez utilisez l'alias just:

```bash
just pytest
```

Si tout est bon, vous devez avoir quelque chose comme ceci:
![Tests](../../contribute/img/tests.png)

### Comment reporter des bug

Cette section va vous guidez pour reporter un bug d'Exdoam.
Avant de reporter un bug, vérifiez les issues [issues](https://gitlab.obspm.fr/exoplanet/exodam/-/issues)
et le [CHANGELOG.md](../changelog/CHANGELOG_FRENCH.md) car vous pourriez découvrir que
vous n'avez pas besoin d'en créer un.
Si il n'y a pas de trace de ce bug créez an issue [ici](https://gitlab.obspm.fr/exoplanet/exodam/-/issues)
en suivant le [template](../../contribute/templates/issues/bug_report.md),
**s'il vous plait incluez autant de détails que possible.**

### Comment soumettre des modifications ou des améliorations

Cette section va vous guidez pour soumettre une modification ou une améliorations pour Exdoam.
Avant de soumettre une modification ou une amélioration, vérifiez les issues [issues](https://gitlab.obspm.fr/exoplanet/exodam/-/issues)
et le [CHANGELOG.md](../changelog/CHANGELOG_FRENCH.md) car vous pourriez découvrir que
vous n'avez pas besoin d'en créer un.
Si la modification ou l'amélioration n'a jamais été proposée, créez une issue [ici](https://gitlab.obspm.fr/exoplanet/exodam/-/issues)
en suivant le [template](../../contribute/templates/issues/feature.md),
**s'il vous plait incluez autant de détails que possible.**

### Comment faire une demande d'intégration

Cette section va vous guidez pour faire une demande d'intégration pour Exodam.
Avant de faire une delande d'intégration assurez-vous de répondre à une [issue](https://gitlab.obspm.fr/exoplanet/exodam/-/issues)
ou d'en avoir ouverte une.

S'il vous plait suivez ces étapes pour que votre contribution soit prise en compte par
un mainteneur.

1. Suivez toutes les instructions dans le [template](../../contribute/templates/merge_request/merge_request.md)
2. Suivez le [guide de style](#guide-de-style)

## Pre-commit

Exodam utilise pre-commit avant chaque commit et avant chaque intégration. Pre-commit
est un script de 'git hook' utile pour identifier de simple problèmes avant de soumettre
son code à une critique. Vous devez installez pre-commit avant de commit sur le git.

```bash
just preinstall
```

## Guide de style

Exodam utilise quelques formateur et quelques "linters" pour améliorer la compréhension
du code par les autres développeurs.

### Formateur

Vous pouvez lancer tout les formateur sur tout les fichiers code et de tests avec:

```bash
just onmy31
```

#### Black

Black est un formateur pour économiser du temps et de l'énergie mentale pour des
questions plus importantes.
Black est lancé par pre-commit mais vous pouvez le lancer manuellement.

```bash
black #filename
```

[black documentation](https://github.com/psf/black)

#### Isort

Isort est un formateur pour trier les importations par ordre alphabétique,
et automatiquement séparés en sections et par type.
Isort est lancé par pre-commit mais vous pouvez le lancer manuellement.

```bash
isort #filename
```

[isort documentation](https://pycqa.github.io/isort/)

### Linter

Vous pouvez lancez tout les "linters" sur le code avec:

```bash
just lint
```

#### Pydocstyle

Pydocstyle est un linter pour vérifier la conformité aux conventions Python docstring.
Pydocstyle est lancé par pre-commit mais vous pouvez le lancer manuellement.

```bash
just pydocstyle #path
```

ou avec just sur tout le code avec:

```bash
just pydocstyle
```

[Pydocstyle documentation](http://www.pydocstyle.org/en/stable/)

#### Mypy

Mypy est un vérificateur de type statique pour Python.
Mypy est lancé par pre-commit mais vous pouvez le lancer manuellement.

```bash
just mypy
```

[Mypy documentation](https://mypy.readthedocs.io/en/stable/)

#### Pylint

Pylint est une analyse de code statique pour Python.
Pylint est lancé par pre-commit mais vous pouvez le lancer manuellement.

```bash
just pylint #path
```

ou avec just sur tout le code avec:

```bash
just pylint
```

[Pylint documentation](https://pylint.pycqa.org/en/latest/index.html)

Pour plus d'information sur une erreur pylint, vous pouvez utilisez [plerr](https://github.com/vald-phoenix/pylint-errors)
avec le code d'erreur

```bash
plerr W013
```

#### Flake8

Flake8 est un wrapper de PyFlakes, pycodestule et 'Ned Batchelder's McCabe script'.
Flake8 est lancé par pre-commit.

[Flake8 documentation](https://flake8.pycqa.org/en/latest/index.html#)

#### Flakehell

Flakehell est une version évoluée de flake8.
Flakehell est lancé par pre-commit mais vous pouvez le lancer manuellement.

```bash
just flakehell #path
```

ou avec just sur tout le code avec:

```bash
just flakhell
```

[Flakehell documentation](https://flakehell.readthedocs.io/)

## Contacts

- Auteur : Ulysse CHOSSON (LESIA)
- Mainteneur : Ulysse CHOSSON (LESIA)
- Email : <ulysse.chosson@obspm.fr>
- Contributeurs :
  - Quentin KRAL (LESIA)
  - Pierre-Yves MARTIN (LESIA)
