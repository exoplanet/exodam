<!-- markdownlint-disable-file MD041 -->

![maintenance](https://img.shields.io/maintenance/yes/2022)

# Exodam (Exoplanetary Data Model)

![exodam logo](contribute/img/exodam-logo-v0.png)

Exodam est une librairie Python pour aider à la création et à la manipulation des
fichiers de description d'exoplanètes et d'autre exo objects (exo-lunes, disques,
comètes, ...).

L'API Exodam facilite la vérification de vos fichiers exodam et les finalise avec un
exodcit (ou pas). Vous pouvez retrouver le projet exodict
[ici](https://gitlab.obspm.fr/exoplanet/exodict).

## Table des matières

- [Exodam (Exoplanetary Data Model)](#exodam-exoplanetary-data-model)
  - [Table des matières](#table-des-matières)
  - [Contact](#contact)
  - [Installation](#installation)
  - [Comment utiliser Exodam](#comment-utiliser-exodam)
  - [Fonctionnalités actuelles](#fonctionnalités-actuelles)
    - [Init](#init)
    - [Check](#check)
    - [Canonize](#canonize)
    - [Diff](#diff)
    - [Finalize](#finalize)
    - [Import csv](#import-csv)
    - [Export](#export)
  - [Contribution et informations pour les dévellopeurs](#contribution-et-informations-pour-les-dévellopeurs)
  - [Full documentation](#full-documentation)

## Contact

- Auteur : Ulysse CHOSSON (LESIA)
- Mainteneur : Ulysse CHOSSON (LESIA)
- Email : <ulysse.chosson@obspm.fr>
- Contributeurs :
  - Quentin KRAL (LESIA)
  - Françoise Roques (LESIA)
  - Pierre-Yves MARTIN (LESIA)

## Installation

Pour toutes les commandes spécifiques au projet, on utilise [just](https://github.com/casey/just).
**Vous devez l'installer**

Après vous pouvez installer les dépendances:

```bash
$ just install
pwd
/home/uchosson/Documents/exodam
poetry install --no-dev --remove-untracked
Installing dependencies from lock file

No dependencies to install or update

Installing the current project: exodam (0.35.0)
```

Et si vous devez contribuer au projet, installez les dépendance de dévellopement:

```bash
$ just install-all
pwd
/home/uchosson/Documents/exodam
poetry install --remove-untracked
Installing dependencies from lock file

No dependencies to install or update

Installing the current project: exodam (0.35.0)
npm install
up to date, audited 35 packages in 721ms

7 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
sudo npm install markdownlint-cli2@0.4.0 --global

changed 36 packages, and audited 37 packages in 3s

8 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

ainsi que les dépendances de pre-commit:

```bash
$ just preinstall
pwd
/home/uchosson/Documents/py-linq-sql
pre-commit clean
Cleaned /home/uchosson/.cache/pre-commit.
pre-commit autoupdate
Updating https://github.com/pre-commit/pre-commit-hooks ...
[INFO] Initializing environment for https://github.com/pre-commit/pre-commit-hooks.
already up to date.
Updating https://github.com/pre-commit/pre-commit-hooks ... already up to date.
Updating https://github.com/pycqa/isort ...
[INFO] Initializing environment for https://github.com/pycqa/isort.
already up to date.
Updating https://github.com/ambv/black ...
[INFO] Initializing environment for https://github.com/ambv/black.
already up to date.
Updating https://github.com/codespell-project/codespell ...
[INFO] Initializing environment for https://github.com/codespell-project/codespell.
already up to date.
Updating https://github.com/pycqa/flake8 ...
[INFO] Initializing environment for https://github.com/pycqa/flake8.
already up to date.
Updating https://github.com/DavidAnson/markdownlint-cli2 ...
[INFO] Initializing environment for https://github.com/DavidAnson/markdownlint-cli2.
already up to date.
pre-commit install --hook-type pre-merge-commit
pre-commit installed at .git/hooks/pre-merge-commit
pre-commit install --hook-type pre-push
pre-commit installed at .git/hooks/pre-push
pre-commit install --hook-type post-rewrite
pre-commit installed at .git/hooks/post-rewrite
pre-commit install-hooks
[INFO] Installing environment for https://github.com/pre-commit/pre-commit-hooks.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
[INFO] Installing environment for https://github.com/pycqa/isort.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
[INFO] Installing environment for https://github.com/ambv/black.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
[INFO] Installing environment for https://github.com/codespell-project/codespell.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
[INFO] Installing environment for https://github.com/pycqa/flake8.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
[INFO] Installing environment for https://github.com/DavidAnson/markdownlint-cli2.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
pre-commit install
pre-commit installed at .git/hooks/pre-commit
```

## Comment utiliser Exodam

Pour utiliser exodam vous pouvez le lancer avec [poetry](https://python-poetry.org/).

```shell
poetry run python3 exodam.py --help
```

Si vous n'avez pas installé just et poetry.

```shell
python3.10 exodam.py --help
```

Exodam utiliser Typer pour lancer les différents verbes (actions) avec leurs options
respectives. Vous pouvez voir les différents verbes avec l'option `--help` et dans les
[Fonctionnalités actuelles](#fonctionnalités-actuelles)

## Fonctionnalités actuelles

### Init

Init crée un fichier basique que vous pouvez remplir pour créer votre fichier exodam.
Vous pouvez choisir le format du basique, yaml ou csv.

### Check

Check vérifie votre fichier pour savoir si les syntax yaml et exodam de celui-ci sont
correct. Cette commande peux prendre différentes options :

- **display**, affiche votre fichier avant la validation.
- **permissive**, peut valider seulement la structure de votre fichier, DOI, objects,
  gs, mais ne regarde pas ce qu'il se passe à l'intérieur. ***Attention** un fichier
  vérifié avec cette option peut ne pas être correct quand vous finalisez l'exodam avec
  finalize.*
- **no-doi**, peut valider un fichier sans DOI

### Canonize

Canonize prend un fichier yaml exodam et trie les objects ainsi que les paramètres avec
un ordre spécifique.

### Diff

Diff vérifie les différences entre 2 fichiers. Diff vérifie les fichier avant d'éxecuter
la différentiation, si vous voulez faire un diff entre 2 fichiers sans validation vous
pouvez utilisez l'option permissive avec `--permissive`

### Finalize

Finalize crée un fichier exodam avec votre fichier ainsi qu'un dictionnaire donné.

### Import csv

Import csv prend un ***dossier*** et l'import pour le convertir en fichier yaml exodam.
Faite attention `exodam.py import-csv` attend un ***dossier*** et non un fichier. Vous
pouvez faire `exodam.py init csv` pour avoir un example.

### Export

Export prend un fichier yaml exodam et l'exporte en json ou en csv. Export verifie le
fichier avant de l'exporter, si vous voulez l'exporter sans validation vous pouvez
utiliser l'option force avec `--force`

## Contribution et informations pour les dévellopeurs

- [Changelog](CHANGELOG.md)
- [Contributing](CONTRIBUTING.md)
- [Notre façon de travailler avec git (git workflow)](workflow_FRENCH.md)

## Full documentation

- Documentation d'Exodam: WIP
- Syntaxe yaml des fichier exodam: WIP
- Documentation d'Exodict: WIP
- [Le projet Exodict](https://gitlab.obspm.fr/exoplanet/exodict)
