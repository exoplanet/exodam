"""All useful load file functions for all module."""

# Standard imports
from json import load as json_load
from pathlib import Path
from typing import Any, cast

# Third party imports
import yaml

# Local imports
from .exception import EmptyYamlError, InvalidYamlError


def is_yaml_file(path: str | Path) -> bool:
    """
    Check if path ended with ".yaml" or ".yml".

    Args :
        - path : path of the file to verify

    Returns :
        True if is .yaml or .yml extension False otherwise

    Examples :
        >>> is_yaml_file("toto.yaml")
        True

        >>> is_yaml_file("toto.yml")
        True

        >>> is_yaml_file("toto.txt")
        False
    """
    return str(path).endswith(".yaml") or str(path).endswith(".yml")


def load_yaml(path: str | Path) -> dict[str, Any]:
    """
    Load path in dict.

    If isn't yaml syntax or .yaml/.yml file return False else return True.

    Args:
        - path : path of the file to verify

    Returns:
        dictionary with the contents of the yaml file

    Raises:
        - InvalidYamlError: if anything is wrong with the YAML file

    """
    if not is_yaml_file(path):
        raise InvalidYamlError("The given file do not end up by .yaml or .yml")

    with open(path, encoding="utf-8") as file:
        try:
            loaded_dict = yaml.safe_load(file)
        except yaml.scanner.ScannerError as err:
            raise InvalidYamlError(f"Syntax error : {err}") from err

    # if pars == None , safe_load dont find yaml valid syntax else it's yaml
    # valid syntax file
    if loaded_dict is None:
        raise EmptyYamlError("The given yaml file are empty")

    return cast(dict[str, Any], loaded_dict)


def load_json(path: Path) -> dict:
    """
    Load a json file and return the content.

    Args:
        path: Path of the file to load.

    Returns:
        Content of the loadded file.
    """
    with open(path, encoding="utf-8") as json_file:
        return cast(dict, json_load(json_file))
