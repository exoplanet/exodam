"""All useful exceptions for all module."""


class ExodamError(Exception):
    """Master exception class for the exodam project."""


class InvalidYamlError(ExodamError):
    """Exception class for an Invalid Yaml file."""


class EmptyYamlError(ExodamError):
    """Exception class for an Empty Yaml file."""


class MissingEnvironmentVariableError(ExodamError):
    """Exception class for an empty environment variable."""
