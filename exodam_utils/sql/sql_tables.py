"""SQL tables struct."""

# Standard imports
from dataclasses import dataclass, field
from functools import lru_cache


@dataclass
class Table:
    """
    Representing table information.

    Attributes:
        name: Name of the table.
        insert_columns: Columns where we need to make insert.
    """

    name: str
    insert_columns: list[str] = field(default_factory=lambda: ["refereed", "data"])


@lru_cache(maxsize=1)
class ExodamSqlTables:
    """
    Exodam SQL tables.

    Attributes:
        exodam_skel: Exodam skel table.
    """

    exodam_skel = Table("exodam.exodam_skel")
    exodam_types = Table("exodam.exodam_types")
    exodam_known_keys = Table("exodam.exodam_known_keys")
    exodam_vupnu_keys = Table("exodam.exodam_vupnu_keys")
    exodam_cerberus = Table("exodam.exodam_cerberus", ["refereed"])
    exodam_cerberus2data = Table(
        "exodam.exodam_cerberus2data",
        ["id_cerberus", "name", "cerberus_type", "id_cerberus_data"],
    )
    exodam_cerberus_data = Table("exodam.exodam_cerberus_data", ["data"])
    exodict = Table("exodam.exodict", ["refereed"])
    exodict2data = Table("exodam.exodict2data", ["id_exodict", "name", "id_data"])
    exodict_data = Table("exodam.exodict_data", ["data"])
    exodict_editorial = Table("exodam.exodict_editorial", ["refereed"])
    exodict_editorial2data = Table(
        "exodam.exodict_editorial2data",
        ["id_exodict", "name", "id_data"],
    )
    exodict_editorial_data = Table("exodam.exodict_editorial_data", ["data"])
    exodam_editorial_skel = Table("exodam.exodam_editorial_skel")
    exodam_editorial_known_keys = Table("exodam.exodam_editorial_known_keys")
    exodam_editorial_cerberus = Table("exodam.exodam_editorial_cerberus", ["refereed"])
    exodam_editorial_cerberus2data = Table(
        "exodam.exodam_editorial_cerberus2data",
        ["id_cerberus", "name", "cerberus_type", "id_cerberus_data"],
    )
    exodam_editorial_cerberus_data = Table(
        "exodam.exodam_editorial_cerberus_data",
        ["data"],
    )
