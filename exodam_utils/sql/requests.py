"""SQL requests useful functions."""

# Standard imports
from typing import cast

# Third party imports
from psycopg import Connection
from py_linq import Enumerable
from py_linq_sql import SQLEnumerable

# Local imports
from .sql_tables import Table


def get_refereed_id(conn: Connection, table: Table) -> int:
    """
    Get id of a reference element.

    Args:
        conn: Connection to a database.
        table: Table of the database for the request.

    Returns:
        Id of the reference element of the given table.
    """
    record_ref_cerberus: Enumerable = (
        SQLEnumerable(conn, table.name)
        .select(lambda x: x.id)
        .where(lambda x: x.refereed == True)  # noqa: E712
        .execute()
    )
    return cast(int, record_ref_cerberus.to_list()[0].id)
