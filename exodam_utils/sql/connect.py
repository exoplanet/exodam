"""Connect function."""

# Standard imports
import contextlib
from collections.abc import Generator
from typing import cast

# Third party imports
from psycopg import Connection
from py_linq_sql import connect as py_linq_sql_connect

# Local imports
from ..config import ExodamDbConnectionInfo


def db_connect(conn_info: ExodamDbConnectionInfo) -> Connection:
    """
    Connect to a database.

    Args:
        conn_info: Information for the connection.

    Returns:
        Connection to the database.
    """
    return py_linq_sql_connect(
        conn_info.user,
        conn_info.password,
        conn_info.host,
        conn_info.port,
        conn_info.db_name,
    )


@contextlib.contextmanager
def conn_manager(conn: Connection | None) -> Generator[Connection, None, None]:
    """
    Get a Connection manager.

    If given connection is None we create a connection with envirenment information
    and yield the connection.
    When we finish with the connection we commit and we close it.

    Else just yield the given connection.
    """
    we_have_a_given_connection = False if conn is None else True

    # No cover because it's use when we are not in tests.
    if not we_have_a_given_connection:  # pragma: no cover
        conn_info = ExodamDbConnectionInfo()
        connection = db_connect(conn_info)

        yield cast(Connection, connection)

        connection.commit()
        connection.close()

    else:
        yield cast(Connection, conn)
