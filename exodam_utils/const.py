"""All useful constants for all module."""

# Standard imports
from pathlib import Path
from typing import Any, TypeAlias

# TODO: A recuperer depuis l'exodict
REGEX_GS = r"^gs[0-9]+$"

# TODO: A recuperer depuis l'exodict
REGEX_UNIT = r"^[a-zA-Z]+[a-zA-Z_]+_(unit|UNIT)$"

EXODICT_TYPE: TypeAlias = dict[str, dict[str, Any] | str]
BASE_DIR = Path(__file__).parent.parent
