"""Manage ENV variables."""

# Standard imports
from functools import lru_cache
from os import getenv

# Third party imports
from dotenv import load_dotenv

# Local imports
from ..exception import MissingEnvironmentVariableError


class DbConnectionInfo:
    """
    Db connection information.

    Attributes:
        user: User use to connect to the database.
        password: Password of the user use to connect to the database.
        host: Host of the database
        port: Port of the database.
        db_name: Database name.
        _raise_msg: Raise messages for the deferred raise method.
    """

    user: str
    password: str
    host: str
    port: str
    db_name: str
    _raise_msg: list[str]

    def _getenv_defer_fail(self, key: str) -> str:
        """
        Get env variable of defer fail.

        Try to get given environment variable if is empty of doesn't exist, stock name
        of the variable to raise an error after.

        Args:
            key: Name of the env var.

        Returns:
            The env var or None if is empty or doesn't exist.
        """
        env_var = getenv(key)

        if env_var is None or not env_var:
            self._raise_msg.append(key)
            return ""

        return env_var

    def _raise_defered_getenv_failure(self) -> None:
        """Raise deferred missing environment variable failure."""
        msg = f"Missing environment variable(s): {', '.join(self._raise_msg)}"
        raise MissingEnvironmentVariableError(msg)

    # PLR0913 = Too many arguments to function call
    def __init__(  # noqa: PLR0913
        self,
        user_env_var_name: str,
        password_env_var_name: str,
        host_env_var_name: str,
        port_env_var_name: str,
        database_env_var_name: str,
    ) -> None:
        """Initialize a DbConnectionInfo instance."""
        load_dotenv()
        self._raise_msg = []

        self.user = self._getenv_defer_fail(user_env_var_name)
        self.password = self._getenv_defer_fail(password_env_var_name)
        self.host = self._getenv_defer_fail(host_env_var_name)
        self.port = self._getenv_defer_fail(port_env_var_name)
        self.db_name = self._getenv_defer_fail(database_env_var_name)

        if self._raise_msg:
            self._raise_defered_getenv_failure()


@lru_cache(maxsize=1)
class ExodamDbConnectionInfo(DbConnectionInfo):
    """Singleton for exodam database connection information."""

    def __init__(self) -> None:
        """Initialize an ExodamDbConnectionInfo instance."""
        super().__init__(
            "LOCAL_DB_USER",
            "LOCAL_DB_PASSWORD",
            "LOCAL_DB_HOST",
            "LOCAL_DB_PORT",
            "LOCAL_DB_DATABASE_NAME",
        )
